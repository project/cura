<?php

/**
 * @file
 * Session functions for the Cura Childcare Suite.
 */

/**
 * Wrapper for session plan form.
 *
 * @param object $child
 *   The child object for which to obtain the session plans.
 * @param string $op
 *   The operation being performed.
 */
function cura_session_plan_form_new_wrapper($child, $op = NULL) {
  return drupal_get_form('cura_session_plan_form', $child, $op);
}

/**
 * Form for editing a session plan.
 */
function cura_session_plan_form_new($form, &$form_state, $child, $op = 'edit') {
//  drupal_set_message('<pre>child:<br>'.print_r($child, TRUE).'<br>'); // ** TESTING **
  $session_plans = cura_get_session_plans('latest', $child->nid);
  $session_types = cura_get_session_types(); // All active session types
  $form['overview'] = array(
    '#markup' => t('To be decided!'),
  );
  foreach ($session_types as $key => $session_type) {

  }
  $form['child_id'] = array(
    '#title' => t('Child'),
    '#type' => 'select',
    '#options' => cura_get_children('select', 'All'),
    '#default_value' => isset($invoice->child_id) ? $invoice->child_id : NULL,
    '#required' => TRUE,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save session plans'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Submission handler for the session plans form.
 */
function cura_session_plan_form_new_submit($form, &$form_state) {
  $account = $GLOBALS['user'];
}

/**
 * Saves the session plans.
 *
 * @param array $session_plans
 *   The session plans to be saved.
 */
function cura_session_plan_save(&$session_plans) {
  foreach ($session_plans as $session_plan) {
    $spw = entity_metadata_wrapper('cura_session_plan', (object) $session_plan);
    $spw->save();
  }
}

/**
 * Returns session plans for a specified child.
 */
function cura_get_session_plans($child_id) {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'cura_session_plan')
    ->propertyCondition('child_id', $child_id)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (isset($result['cura_session_plan'])) {
    $session_plan_ids = array_keys($result['cura_session_plan']);
//    drupal_set_message('<pre>session_plan_ids:<br>'.print_r($session_plan_ids, TRUE).'<br>'); // ** TESTING **
    return entity_load('cura_session_plan', $session_plan_ids);
  }
  else {
    return FALSE;
  }
}

/**
 * Returns render array to display the session plans for a child.
 *
 * @param object $child
 *   The child object owning the session plans.
 * @param int $date
 *   The point in time for which to obtain the session plans.
 *
 * @return array
 */
function cura_display_session_plan($child, $date = REQUEST_TIME) {
  $days = array(0 => 'Mon', 1 => 'Tue', 2 => 'Wed', 3 => 'Thu', 4 => 'Fri', 5 => 'Sat', 6 => 'Sun');
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'cura_session_plan')
    ->propertyCondition('child_id', $child->nid)
    ->propertyOrderBy('start')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (isset($result['cura_session_plan'])) {
    $session_plan_ids = array_keys($result['cura_session_plan']);
    $session_plans = entity_load('cura_session_plan', $session_plan_ids);
    foreach ($session_plans as $session_plan_id => $session_plan) {
      $spw = entity_metadata_wrapper('cura_session_plan', $session_plan);
      if (date('Y-m-d', $session_plan->start) <= date('Y-m-d', $date)) {
        $session_types[$session_plan->session_type_id] = $spw->session_type_id->label->value();
        $session_hours[$session_plan->session_type_id] = explode(',', $session_plan->days);
      }
    }
//    drupal_set_message('<pre>Session hours:<br>'.print_r($session_hours, TRUE).'<br>'); // ** TESTING **
    if (!empty($session_types)) {
      $header = array(
        array('data' => 'Session type', 'class' => 'text-left', 'style' => 'text-align: left; padding-right: 20px;'),
        array('data' => 'Mon', 'class' => 'text-right', 'style' => 'padding-right: 10px;'),
        array('data' => 'Tue', 'class' => 'text-right', 'style' => 'padding-right: 10px;'),
        array('data' => 'Wed', 'class' => 'text-right', 'style' => 'padding-right: 10px;'),
        array('data' => 'Thu', 'class' => 'text-right', 'style' => 'padding-right: 10px;'),
        array('data' => 'Fri', 'class' => 'text-right', 'style' => 'padding-right: 10px;'),
        array('data' => 'Sat', 'class' => 'text-right', 'style' => 'padding-right: 10px;'),
        array('data' => 'Sun', 'class' => 'text-right', 'style' => 'padding-right: 10px;'),
      );
      foreach ($session_types as $session_type_id => $session_type_label) {
        $row['data'] = array(
          array(
            'data' => $session_type_label,
            'class' => 'text-left',
            'style' => 'text-align: left; padding-right: 20px;',
          )
        );
        foreach ($days as $key => $day) {
          $hours = 0;
          if ($session_hours[$session_type_id][$key] == $day) {
            $hours = $spw->session_type_id->duration->value();
          }
          $row['data'][] = array(
            'data' => $hours,
            'class' => 'text-right',
            'style' => 'text-align: right; padding-right: 10px;',
          );
        }
        $row['class'] = array('text-right');
        $row['style'] = array('text-align: right;');
        $rows[] = $row;
      }
      return array(
        'session_plan_table' => array(
          '#theme' => 'table',
          '#attributes' => array('style' => array('text-align: right;')),
          '#header' => $header,
          '#rows' => $rows,
//          '#attributes' => array('style' => 'width: 95%')
        )
      );
    }
    else {
      return array(
        'session_plan_table' => array(
          '#markup' => '<p>There are no active session plans for this date.</p>',
        )
      );
    }
  }
  else {
    return array(
      'session_plan_table' => array(
        '#markup' => '<p>There are no session plans for this child.</p>',
      )
    );
  }
}

/**
 * Returns render array to display the sessions for a child.
 *
 * @param object $child
 * @param int $date
 *
 * @return array
 */
function cura_display_session($child, $date = REQUEST_TIME) {
  $monday_this = strtotime(date('Y-m-d', strtotime('last monday', strtotime('tomorrow', $date))));
  $monday_next = strtotime(date('Y-m-d', strtotime('next monday', $date)));
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'cura_session')
    ->propertyCondition('child_id', $child->nid)
    ->propertyCondition('date', $monday_this, '>=')
    ->propertyCondition('date', $monday_next, '<')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (isset($result['cura_session'])) {
    $session_ids = array_keys($result['cura_session']);
    $sessions = entity_load('cura_session', $session_ids);
    foreach ($sessions as $session_id => $session) {
      $session_types[$session->session_type_id] = $session->session_type_label;
      $session_hours[$session->session_type_id][date('D', $session->date)] = $session->hours;
    }
//    drupal_set_message('<pre>Session types:<br>'.print_r($session_types, TRUE).'<br>'); // ** TESTING **
//    drupal_set_message('<pre>Session hours:<br>'.print_r($session_hours, TRUE).'<br>'); // ** TESTING **
    $header = array('Session type', array('data' => 'Mon', 'class' => 'text-right'),
                    array('data' => 'Tue', 'class' => 'text-right'), array('data' => 'Wed', 'class' => 'text-right'),
                    array('data' => 'Thu', 'class' => 'text-right'), array('data' => 'Fri', 'class' => 'text-right'),
                    array('data' => 'Sat', 'class' => 'text-right'), array('data' => 'Sun', 'class' => 'text-right'),
    );
    foreach ($session_types as $session_type_id => $session_type_label) {
      $row['data'] = array(array('data' => $session_type_label, 'class' => 'text-left'));
      for ($days = 0; $days < 7; $days++) {
        $day = $monday_this + ($days * 86400);
        if (isset($session_hours[$session_type_id][date('D', $day)])) {
          $row['data'][] = $session_hours[$session_type_id][date('D', $day)];
        }
        else {
          $row['data'][] = 0;
        }
      }
      $row['class'] = array('text-right');
      $rows[] = $row;
    }
    return array(
      'session_plan_table' => array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
//        '#attributes' => array('style' => 'width: 95%')
      )
    );
  }
  else {
    return array(
      'session_plan_table' => array(
        '#markup' => '<p>No session set for this week for this child.</p>',
      )
    );
  }
}

/**
 * Returns session types for a specified child ID or all session types if no child is specified.
 *
 * @param int $child_id
 *   (optional) The ID of the child.
 *
 * @return object[]
 *   Returns an array of cura_session_type objects.
 */
function cura_get_session_types($child_id = NULL) {
  if (isset($child_id)) {
    $session_type_ids = db_query('SELECT DISTINCT session_type_id FROM {cura_session_plans}
                                  WHERE child_id = :child_id
                                  ORDER BY session_type_id',
                                  array(':child_id' => $child_id))->fetchCol();
  }
  else {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'cura_session_type')
      ->propertyCondition('active', TRUE)
      ->propertyOrderBy('start')
      ->addMetaData('account', user_load(1)); // Run the query as user 1.
    $result = $query->execute();
    if (isset($result['cura_session_type'])) {
      $session_type_ids = array_keys($result['cura_session_type']);
    }
  }
//drupal_set_message('<pre>session_plan_ids:<br>'.print_r($session_plan_ids, TRUE).'<br>'); // ** TESTING **

  return empty($session_type_ids) ? FALSE : entity_load('cura_session_type', $session_type_ids);
}

/**
 * Creates a new session record.
 *
 * @param object $session_type
 *   The cura_session_type object.
 * @param int $date
 *   The day of the session (UNIX Timestamp).
 * @param object $child
 *   The child node object.
 * @param array $session_data
 * @param bool $active
 *
 * @return object
 *   Returns a cura_session object.
 */
function cura_session_new($session_type, $date, $child, $session_data, $active) {
  $account = $GLOBALS['user'];
  $ip = ip_address();
  $session = FALSE;
  if (isset($session_type->session_type_id)) {
    $session = entity_create('cura_session', array(
      'label' => date('Ymd', $date).'-'.$session_type->name.'-'
                 .$child->cura_lastname[LANGUAGE_NONE][0]['value'].'('.$child->nid.')',
      'session_type_id' => $session_type->session_type_id,
      'date' => $date,
      'dow' => date('D', $date), // For easier reporting in Views
      'child_id' => $child->nid,
      'session_type_label' => $session_type->label,
      'start' => $session_type->start,
      'finish' => $session_type->finish,
      'duration' => $session_type->duration,
      'billable' => $session_type->billable,
      'hours' => $session_data['hours'],
      'hours_n' => $session_data['hours_n'],
      'hours_f' => $session_data['hours_f'],
      'amount' => $session_data['amount'],
      'amount_n' => $session_data['amount_n'],
      'amount_f' => $session_data['amount_f'],
      'created_by' => $account->uid,
      'created_on' => REQUEST_TIME,
      'created_ip' => $ip,
      'updated_by' => $account->uid,
      'updated_on' => REQUEST_TIME,
      'updated_ip' => $ip,
      'active' => $active,
    ));
    entity_save('cura_session', $session);
  }

  return $session;
}

/**
 * Updates a session record.
 *
 * @param object $session
 *   The cura_session object.
 * @param object $session_type
 *   The cura_session_type object.
 * @param array $session_data
 *   An array of session data.
 * @param bool $active
 *   Active flag.
 *
 * @return object
 *   Returns a cura_session object.
 */
function cura_session_update($session, $session_type, $session_data, $active) {
  $account = $GLOBALS['user'];
  $ip = ip_address();
  if (isset($session_type->session_type_id)) {
    $session->session_type_label = $session_type->label;
    $session->start = $session_type->start;
    $session->finish = $session_type->finish;
    $session->duration = $session_type->duration;
    $session->billable = $session_type->billable;
    $session->hours = $session_data['hours'];
    $session->hours_n = $session_data['hours_n'];
    $session->hours_f = $session_data['hours_f'];
    $session->amount = $session_data['amount'];
    $session->amount_n = $session_data['amount_n'];
    $session->amount_f = $session_data['amount_f'];
    $session->updated_by = $account->uid;
    $session->updated_on = REQUEST_TIME;
    $session->updated_ip = $ip;
    $session->active = $active;
    entity_save('cura_session', $session);
  }

  return $session;
}

/**
 * Updates the active indicator for a session.
 *
 * @param array $session_ids
 *   Array of session IDs to update.
 *
 * @return int
 *   The number of sessions updated.
 */
function cura_session_update_active($session_ids = array()) {
  $sql = 'UPDATE {cura_sessions} AS s';
  $sql .= ' LEFT JOIN {cura_attendance} AS a ON s.session_id = a.session_id';
  $sql .= ' LEFT JOIN {cura_billing} AS b ON s.session_id = b.session_id';
  $sql .= ' SET s.active = CASE';
  $sql .= '  WHEN a.session_id IS NULL AND b.session_id IS NULL THEN 0';
  $sql .= '  ELSE 1 END';
  if (!empty($session_ids)) $sql .= ' WHERE s.session_id IN (:session_ids)';
  $vars = array(':session_ids' => $session_ids);
  $count = db_query($sql, $vars)->rowCount();
  watchdog('cura', 'Updated active indicator for %c session records.', array('%c' => $count));
}

/**
 * Returns a session for a specified session ID. If an ID is not provided it will return a session
 * for a specific date, session type ID and child ID.
 *
 * @param int $session_type_id
 *   The ID of the session type.
 *
 * @return object
 *   Returns a cura_session object.
 */
function cura_get_session($session_type_id, $date, $child_id, $active = NULL) {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'cura_session')
    ->propertyCondition('session_type_id', $session_type_id)
    ->propertyCondition('date', $date)
    ->propertyCondition('child_id', $child_id)
    // Get the most recently updated session.
    ->propertyOrderBy('updated_on', 'DESC')
    ->range(0, 1)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  if (!is_null($active)) {
    $query->propertyCondition('active', $active);
  }
  $result = $query->execute();
  if (isset($result['cura_session'])) {
    $session = entity_load('cura_session', array_keys($result['cura_session']));
//    drupal_set_message('<pre>session_ids:<br>'.print_r($session_id, TRUE).'<br></pre>'); // ** TESTING **
    return array_shift($session);
  }
  else {
    return FALSE;
  }
}

/**
 * Deletes the inactive session records for the specified child. Active session records are those that have
 * linked attendance or billing data.
 *
 * @param int $child_id
 *   The ID of the child.
 * @param int $date
 *   The day to delete (UNIX timestamp).
 *
 * @return int
 *   Returns a count of the session records deleted.
 */
function cura_session_delete($child_id, $date = NULL) {
  if (empty($child_id) && empty($date)) {
    watchdog('cura', 'No sessions deleted as both child and date were not specified.');

    return 0;
  }

  $transaction = db_transaction();
  try {
    // Delete the sessions.
    $and = db_and();
    $and->condition('active', 0);
    if (!empty($child_id)) $and->condition('child_id', $child_id);
    if (!empty($date)) $and->condition('date', $date);
    $count = db_delete('cura_sessions')->condition($and)->execute();
    $label = ($count == 1) ? 'record' : 'records';
    watchdog('cura', 'Deleted @c session @l for @id on @d.',
             array(
               '@c' => $count,
               '@l' => $label,
               '@id' => empty($child_id) ? 'all children' : 'child ' . $child_id,
               '@d' => empty($date) ? 'all days' : date('l, j F Y', $date),
             )
    );
  }
  catch (Exception $e) {
    $transaction->rollback();
    watchdog_exception('cura', $e);
    throw $e;
  }

  return $count;
}

/**
 * Create or update session records.
 */
function cura_sessions_create($start = REQUEST_TIME, $finish = NULL, $child_nids = NULL) {
  if (!module_exists('cura_billing')) {
    watchdog('cura', 'The Cura Billing module is required to create session records.');
    return;
  }
  module_load_include('inc', 'cura_billing', 'cura_billing.ui');
  $sessions_created = $sessions_updated = 0;
  $session_types = cura_get_session_types();
//  drupal_set_message('<pre>session_types:<br>' . print_r($session_types, TRUE) . '<br>'); // ** TESTING **
  if (empty($session_types)) {
    watchdog('cura', 'No session types loaded in cura_sessions_create!');
    return;
  }
  // Default finish of 1 year from now.
  if (is_null($finish) || $finish <= $start) $finish = REQUEST_TIME + 31556926;
  if (is_null($child_nids)) {
    // Get the children not in Left or Cancelled status.
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'child')
      ->fieldCondition('field_child_status', 'value', array('L', 'C'), 'NOT IN')
      // Run the query as user 1.
      ->addMetaData('account', user_load(1));
    $result = $query->execute();
    if (isset($result['node'])) {
      $child_nids = array_keys($result['node']);
    }
  }
  if (!is_null($child_nids)) {
    if ($session_plans = cura_session_plan_load($start, $finish, $child_nids)) {
      $base_bill = cura_billing_base_bill_create($start, $finish);
      $children = node_load_multiple($child_nids);
      foreach ($children as $nid => $child) {
        // Reset the session plans and invoice lines.
        $base_bill['session_plans'] = $base_bill['lines'] = array();
        foreach ($session_plans as $session_plan) {
          $spw = entity_metadata_wrapper('cura_session_plan', $session_plan);
//          echo '<pre>Commerce product:<br>' . print_r($spw->session_type_id->product_id->value()) . '</pre>';
          if ($session_plan->child_id == $child->nid) {
            $base_bill['session_plans'][$session_plan->session_plan_id] = array(
              'label' => $session_plan->label,
              'session_type_id' => $spw->session_type_id->raw(),
              'session_type' => $spw->session_type_id->label->value(),
              'product_id' => $spw->session_type_id->product_id->raw(),
              'product' => $spw->session_type_id->product_id->title->value(),
              'sku' => $spw->session_type_id->product_id->sku->value(),
              'price' => $spw->session_type_id->product_id->commerce_price->amount->value(),
              'hours' => $spw->session_type_id->duration->value(),
              'sup_product_id' => $spw->session_type_id->sup_product_id->raw(),
              'sup_product' => $spw->session_type_id->sup_product_id->title->value(),
              'sup_sku' => $spw->session_type_id->sup_product_id->sku->value(),
              'sup_price' => $spw->session_type_id->sup_product_id->commerce_price->amount->value(),
              'days' => $session_plan->days,
              'funding' => $session_plan->funding,
              'start' => date('Y-m-d', $session_plan->start),
            );
            $base_bill['lines'][$spw->session_type_id->product_id->raw()] = array(
              'product_id' => $spw->session_type_id->product_id->raw(),
              'product' => $spw->session_type_id->product_id->title->value(),
              'sku' => $spw->session_type_id->product_id->sku->value(),
              'price' => $spw->session_type_id->product_id->commerce_price->amount->value(),
              'hours' => 0,
            );
            $base_bill['lines'][$spw->session_type_id->sup_product_id->raw()] = array(
              'product_id' => $spw->session_type_id->sup_product_id->raw(),
              'product' => $spw->session_type_id->sup_product_id->title->value(),
              'sku' => $spw->session_type_id->sup_product_id->sku->value(),
              'price' => $spw->session_type_id->sup_product_id->commerce_price->amount->value(),
              'hours' => 0,
            );
          }
        }
        $bill = cura_billing_bill_create($base_bill, $child);
//        drupal_set_message('<pre>bill:<br>' . print_r($bill, TRUE) . '<br>'); // ** TESTING **
        $sessions = array();
        // Process all the days on the bill.
        foreach ($bill['days'] as $date => $day) {
          // Only continue with processing if the child has sessions.
          if (!empty($day['sessions'])) {
            foreach ($day['sessions'] as $session_type_id => $bill_session) { // Process all the sessions in the day
              $session_type = $session_types[$session_type_id];
              $sessions[$child->nid][$date][$session_type_id] = $session_data = array(
                'hours' => $bill_session['hours'],
                'hours_n' => $bill_session['hours_n'],
                'hours_f' => $bill_session['hours_f'],
                'amount' => $bill_session['amount_n'] + $bill_session['amount_f'],
                'amount_n' => $bill_session['amount_n'],
                'amount_f' => $bill_session['amount_f'],
              );
              if ($session = cura_get_session($session_type_id, strtotime($date), $child->nid)) {
                // Check if session has attendance or billing recorded.
                if (!$session->active) {
                  cura_session_update($session, $session_type, $session_data, FALSE);
                  $sessions_updated++;
                }
              }
              else {
                // Create a new session.
                $session = cura_session_new($session_type, strtotime($date), $child, $session_data, FALSE);
                $sessions_created++;
              }
            }
          }
        }
//        drupal_set_message('<pre>sessions:<br>' . print_r($sessions, TRUE) . '<br>'); // ** TESTING **
      }
      watchdog('cura', 'Created %sc and updated %su session records for %c children.',
               array('%sc' => $sessions_created, '%su' => $sessions_updated, '%c' => count($children)));
    }
    else {
      watchdog('cura', 'No session plans retrieved. Created 0 session records.');
    }
  }
  else {
    watchdog('cura', 'No children records retrieved. Created 0 session records.');
  }
}

