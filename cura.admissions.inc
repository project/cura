<?php

/**
 * @file
 * Admissions functions for the Cura Childcare Suite.
 */

module_load_include('inc', 'cura', 'cura.sessions');

/**
 * Creates a new child record if one does not already exist.
 * 
 * @param $child array()
 */
function cura_child_save($child) {
  $child['firstname'] = trim($child['firstname']);
  $child['lastname'] = trim($child['lastname']);
  $month = date('n', strtotime($child['birthdate']));
  $day = date('j', strtotime($child['birthdate']));
  if ($month > 8) {
    $year = date('Y', strtotime($child['birthdate'])) + 5;
  }
  else {
    $year = date('Y', strtotime($child['birthdate'])) + 4;
  }
  // Set the finish date as the start of the school year.
  $finish_date = date("Y-m-d", mktime(0, 0, 0, 9, 1, $year));
  $child['nid'] = cura_child_get($child);
  // Check if no child exists.
  if ($child['nid'] === NULL) {

    $new_child = new stdClass();
//    $new_child->title = $child['lastname'].', '.$child['firstname'].' ('.$child['birthdate'].')'; // Handled by autonodetitle
    $new_child->type = "child";
    // Sets some defaults and invokes hooks.
    node_object_prepare($new_child);
    $new_child->language = LANGUAGE_NONE;
    // Admissions user.
    $new_child->uid = 165;
    $new_child->cura_firstname['und'][0]['value'] = $child['firstname'];
    $new_child->cura_lastname['und'][0]['value'] = $child['lastname'];
    $new_child->field_birth_date['und'][0]['value'] = $child['birthdate'];
    $new_child->field_birth_date['und'][0]['rrule'] = 'RRULE:FREQ=YEARLY;INTERVAL=1;BYMONTH='
                                                       . $month . ';BYMONTHDAY=' . $day . ';COUNT=5;WKST=MO';
    $new_child->field_child_gender['und'][0]['value'] = $child['gender'];
    $new_child->field_child_ethnicity['und'][0]['value'] = $child['ethnicity'];
    $new_child->field_child_first_lang['und'][0]['tid'] = $child['first_language'];
    if (isset($child['account'])) {
      $new_child->field_account_id['und'][] = array(
          'target_id' => $child['account'],
          'target_type' => 'node',
      );
    }
    $new_child->field_child_reg_date['und'][0]['value'] = $child['reg_date'];
    $new_child->field_child_start_date['und'][0]['value'] = $child['start_date'];
    $new_child->field_child_finish_date['und'][0]['value'] = $child['finish_date'];
    foreach ($child['days'] as $key => $value) {
      $new_child->field_child_days['und'][$key]['value'] = $value;
    }
    // Prepare node for saving.
    $new_child = node_submit($new_child);
    node_save($new_child);

    $child['nid'] = $new_child->nid;

//    drupal_set_message('Added child: '.$child['firstname'].' '.$child['lastname'].' ('.$child['nid'].')');
    watchdog('cura', 'Added child @fn @ln (@nid).', array('@fn' => $child['firstname'],
        '@ln' => $child['lastname'], '@nid' => $child['nid']));

  }

  return $child['nid'];
}

/**
 * Finds a child record with the provided data.
 * 
 * @param $child array('firstname', 'lastname', 'birthdate', 'gender', 'ethnicity', 'first_language',
 *                     'account', 'reg_date', 'start_date', 'finish_date')
 */
function cura_child_get($child) {
  module_load_include('inc', 'cura', 'includes/cura.firstname');
  $firstnames = cura_get_firstnames($child['firstname']);
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'child')
    ->propertyOrderBy('created', 'DESC')
    ->fieldCondition('cura_firstname', 'value', $firstnames, 'IN')
    ->fieldCondition('cura_lastname', 'value', $child['lastname'], '=')
    ->fieldCondition('field_birth_date', 'value', $child['birthdate'], '=')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  // Check if a child was found.
  if (isset($result['node'])) {
    $child['nids'] = array_keys($result['node']);
//    drupal_set_message('Child: ' . $child['firstname'] . ' ' . $child['lastname'] . ' (' . $child['nids'][0] . ') found!');
    return $child['nids'][0];
  }
  else {
//    drupal_set_message('Child: ' . $child['firstname'] . ' ' . $child['lastname'] . ' not found!');
    return NULL;
  }
}

/**
 * Gets the attendance schedule from the sessions table.
 *
 * @param int $start
 *   The start of the attendance period.
 * @param array $child_ids
 *   The IDs of the children.
 * @param string $format
 *   The format in which to return the data.
 *
 * @return string|false
 */
function cura_get_attendance_schedule($start = REQUEST_TIME, $period = '7 days', $child_ids, $format = 'objects') {
  $schedule = false;
  $start = date('Y-m-d', $start);
  $finish = date('Y-m-d', strtotime($start . ' +' . $period));
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'cura_session')
    ->propertyCondition('date', array($start, $finish), 'between')
    ->propertyOrderBy('date')
    ->propertyOrderBy('start')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  // Check if sessions were found.
  if (isset($result['node'])) {
    switch ($format) {
      case 'ids':
        $schedule = array_keys($result['node']);
        break;
      case 'objects':
        $schedule = node_load_multiple(array_keys($result['node']));
        break;
      case 'text':
        $starters = '';
        $schedule = node_load_multiple(array_keys($result['node']));
        foreach ($children as $child) {
          $cw = entity_metadata_wrapper('node', $child);
          $date = date('Y-m-d D', $cw->field_child_start_date->value());
          $name = $cw->cura_firstname->value()." ".$cw->cura_lastname->value();
          $age_at_start = date_diff(date_create($child->field_birth_date[LANGUAGE_NONE][0]['value']),
                                    date_create($child->field_child_start_date[LANGUAGE_NONE][0]['value']))->format('%yy %mm');
          if (is_object($cw->field_child_key_person->value())) {
            $kp = $cw->field_child_key_person->field_staff_person->field_firstname->value();
          }
          else {
            $kp = 'No key person';
          }
          $starters .= $date . " " . $name . " (" . $age_at_start . ") - " . $kp . "\r\n";
        }
        break;
      case 'html':
        $starters = "<table><thead><tr><th>Date<th>Child<th>Age<th>Key person</thead><tbody>";
        $children = node_load_multiple(array_keys($result['node']));
        foreach ($children as $child) {
          $cw = entity_metadata_wrapper('node', $child);
          $date = date('Y-m-d D', $cw->field_child_start_date->value());
          $name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
          $age_at_start = date_diff(date_create($child->field_birth_date[LANGUAGE_NONE][0]['value']),
                                    date_create($child->field_child_start_date[LANGUAGE_NONE][0]['value']))->format('%yy %mm');
          if (is_object($cw->field_child_key_person->value())) {
            $kp = $cw->field_child_key_person->field_staff_person->field_firstname->value();
            $kp .= ' ' . $cw->field_child_key_person->field_staff_person->field_lastname->value();
          }
          else {
            $kp = 'No key person';
          }
          $starters .= "<tr><td>" . $date . "<td>" . $name . "<td>" . $age_at_start . "<td>" . $kp;
        }
        $starters .= '</table>';
        break;
    }
  }
  else {
    switch ($format) {
      case 'text':
        $starters = 'No starters in period ' . $start . ' to ' . $finish . '.';
        break;
      case 'html':
        $starters = '<p>No starters in period ' . $start . ' to ' . $finish . '.</p>';
        break;
    }
  }

  return $starters;
}

/**
 * Gets the starters for the specified period.
 *
 * @param int $start
 *   The start of the period.
 * @param string $period
 *   The period expressed as a string.
 * @param string $format
 *   The format in which to return the data.
 *
 * @return string
 */
function cura_get_starters($start = REQUEST_TIME, $period = '12 weeks', $format = 'objects') {
  $starters = false;
  $start = date('Y-m-d', $start);
  $finish = date('Y-m-d', strtotime($start . ' +' . $period));
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'child')
    ->fieldCondition('field_child_status', 'value', array('C'), 'NOT IN')
    ->fieldCondition('field_child_start_date', 'value', array($start, $finish), 'between')
    ->fieldOrderBy('field_child_start_date', 'value')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  // Check if a child was found.
  if (isset($result['node'])) {
    switch ($format) {
      case 'ids':
        $starters = array_keys($result['node']);
        break;
      case 'objects':
        $starters = node_load_multiple(array_keys($result['node']));
        break;
      case 'text':
        $starters = '';
        $children = node_load_multiple(array_keys($result['node']));
        foreach ($children as $child) {
          $cw = entity_metadata_wrapper('node', $child);
          $date = date('Y-m-d D', $cw->field_child_start_date->value());
          $name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
          $age_at_start = date_diff(date_create($child->field_birth_date[LANGUAGE_NONE][0]['value']),
                                    date_create($child->field_child_start_date[LANGUAGE_NONE][0]['value']))->format('%yy %mm');
          if (is_object($cw->field_child_key_person->value())) {
            $kp = $cw->field_child_key_person->field_staff_person->field_firstname->value();
          }
          else {
            $kp = '-';
          }
          $starters .= $date . ' ' . $name . ' (' . $age_at_start . ') - ' . $kp . "\r\n";
        }
        break;
      case 'html':
        $starters = '<h2>' . t('Future starters') . '</h2>';
        $starters .= '<table style="margin-left:10px;"><thead>';
        $starters .= '<tr style="background-color:#f5f5f5;">';
        $starters .= '<th style="text-align:left;padding-right:10px;min-width:100px;">Date';
        $starters .= '<th style="text-align:left;padding-right:10px;">Child';
        $starters .= '<th style="text-align:right;padding-right:10px;">Age';
        $starters .= '<th style="text-align:left;padding-right:10px;">Key person';
        $starters .= '</thead><tbody>';
        $children = node_load_multiple(array_keys($result['node']));
        foreach (array_values($children) as $row => $child) {
          $cw = entity_metadata_wrapper('node', $child);
          $date = date('Y-m-d D', $cw->field_child_start_date->value());
          $name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
          $age_at_start = date_diff(date_create($child->field_birth_date[LANGUAGE_NONE][0]['value']),
                                    date_create($child->field_child_start_date[LANGUAGE_NONE][0]['value']))->format('%yy %mm');
          if (is_object($cw->field_child_key_person->value())) {
            $kp = $cw->field_child_key_person->field_staff_person->field_firstname->value()
                  ." ".$cw->field_child_key_person->field_staff_person->field_lastname->value();
          }
          else {
            $kp = '-';
          }
          $colour = (($row + 1) % 2 == 1) ? '#fff' : '#f5f5f5';
          $starters .= '<tr style="background-color:' . $colour . '">';
          $starters .= '<td style="text-align:left;padding-right:10px;">' . $date;
          $starters .= '<td style="text-align:left;padding-right:10px;">' . $name;
          $starters .= '<td style="text-align:right;padding-right:10px;">' . $age_at_start;
          $starters .= '<td style="text-align:left;padding-right:10px;">' . $kp;
        }
        $starters .= '</tbody></table>';
        break;
    }
  }
  else {
    switch ($format) {
      case 'text':
        $starters = "No starters in the period ".$start." to ".$finish.".\n";
        break;
      case 'html':
        $starters = '<h2>' . t('Future starters') . '</h2>';
        $starters .= '<p style="margin-left:10px;">No starters in the period ' . $start . ' to ' . $finish . ".</p>\n";
        break;
    }
  }

  return $starters;
}

/**
 * Gets the leavers for the specified period.
 *
 * @param int $start
 *   The start of the period.
 * @param string $period
 *   The period expressed as a string.
 * @param string $format
 *   The format in which to return the data.
 *
 * @return string
 */
function cura_get_leavers($start = REQUEST_TIME, $period = '12 weeks', $format = 'objects') {
  $leavers = false;
  $start = date('Y-m-d', $start);
  $finish = date('Y-m-d', strtotime($start . ' +' . $period));
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'child')
    ->fieldCondition('field_child_status', 'value', array('C'), 'NOT IN')
    ->fieldCondition('field_child_finish_date', 'value', array($start, $finish), 'between')
    ->fieldOrderBy('field_child_finish_date', 'value')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  // Check if a child was found.
  if (isset($result['node'])) {
    switch ($format) {
      case 'ids':
        $leavers = array_keys($result['node']);
        break;
      case 'objects':
        $leavers = node_load_multiple(array_keys($result['node']));
        break;
      case 'text':
        $leavers = '';
        $children = node_load_multiple(array_keys($result['node']));
        foreach ($children as $child) {
          $cw = entity_metadata_wrapper('node', $child);
          $date = date('Y-m-d D', $cw->field_child_finish_date->value());
          $name = $cw->cura_firstname->value()." ".$cw->cura_lastname->value();
          $age_at_finish = date_diff(date_create($child->field_birth_date[LANGUAGE_NONE][0]['value']), 
                                    date_create($child->field_child_finish_date[LANGUAGE_NONE][0]['value']))->format('%yy %mm');
          // Check if the child has a key person.
          if (is_object($cw->field_child_key_person->value())) {
            $kp = $cw->field_child_key_person->field_staff_person->field_firstname->value();
          }
          else {
            $kp = '-';
          }
          $leavers .= $date . ' ' . $name . ' (' . $age_at_start . ') - ' . $kp . "\r\n";
        }
        break;
      case 'html':
        $leavers = '<h2>' . t('Future leavers') . '</h2>';
        $leavers .= '<table style="margin-left:10px;"><thead>';
        $leavers .= '<tr style="background-color:#f5f5f5;">';
        $leavers .= '<th style="text-align:left;padding-right:10px;min-width:100px;">Date';
        $leavers .= '<th style="text-align:left;padding-right:10px;">Child';
        $leavers .= '<th style="text-align:right;padding-right:10px;">Age';
        $leavers .= '<th style="text-align:left;padding-right:10px;">Key person';
        $leavers .= '</thead><tbody>';
        $children = node_load_multiple(array_keys($result['node']));
        foreach (array_values($children) as $row => $child) {
          $cw = entity_metadata_wrapper('node', $child);
          $date = date('Y-m-d D', $cw->field_child_finish_date->value());
          $name = $cw->cura_firstname->value()." ".$cw->cura_lastname->value();
          $age_at_finish = date_diff(date_create($child->field_birth_date[LANGUAGE_NONE][0]['value']),
                                    date_create($child->field_child_finish_date[LANGUAGE_NONE][0]['value']))->format('%yy %mm');
          // Check if the child has a key person.
          if (is_object($cw->field_child_key_person->value())) {
            $kp = $cw->field_child_key_person->field_staff_person->field_firstname->value()
                  . ' ' . $cw->field_child_key_person->field_staff_person->field_lastname->value();
          }
          else {
            $kp = '-';
          }
          $colour = (($row + 1) % 2 == 1) ? '#fff' : '#f5f5f5';
          $leavers .= '<tr style="background-color:' . $colour . '">';
          $leavers .= '<td style="text-align:left;padding-right:10px;">' . $date;
          $leavers .= '<td style="text-align:left;padding-right:10px;">' . $name;
          $leavers .= '<td style="text-align:right;padding-right:10px;">' . $age_at_finish;
          $leavers .= '<td style="text-align:left;padding-right:10px;">' . $kp;
        }
        $leavers .= "</tbody></table>";
        break;
    }
  }
  else {
    switch ($format) {
      case 'text':
        $leavers = 'No leavers in the period ' . $start . ' to ' . $finish . ".\n";
        break;
      case 'html':
        $leavers = '<h2>' . t('Future leavers') . '</h2>';
        $leavers .= '<p style="margin-left:10px;">No leavers in the period ' . $start . ' to ' . $finish . ".</p>\n";
        break;
    }
  }

  return $leavers;
}

/**
 * Gets the most recent starters.
 */
function cura_get_past_starters($count = 30) {
  $starters = false;
  $tomorrow = strtotime(date('Y-m-d') . ' +1 day');
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'child')
    ->fieldCondition('field_child_status', 'value', array('C'), 'NOT IN')
    ->fieldCondition('field_child_start_date', 'value', date('Y-m-d', $tomorrow), '<')
    ->fieldOrderBy('field_child_start_date', 'value', 'DESC')
    ->range(0, $count)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  // Check if a child was found.
  if (isset($result['node'])) {
    $starters = node_load_multiple(array_keys($result['node']));
  }

  return $starters;
}

/**
 * Gets the most recent leavers.
 */
function cura_get_past_leavers($count = 30) {
  $leavers = false;
  $tomorrow = strtotime(date('Y-m-d') . ' +1 day');
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'child')
    ->fieldCondition('field_child_status', 'value', array('C'), 'NOT IN')
    ->fieldCondition('field_child_finish_date', 'value', date('Y-m-d', $tomorrow), '<')
    ->fieldOrderBy('field_child_finish_date', 'value', 'DESC')
    ->range(0, $count)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  // Check if a child was found.
  if (isset($result['node'])) {
    $leavers = node_load_multiple(array_keys($result['node']));
  }

  return $leavers;
}

/**
 * Gets the most recent starters and leavers.
 */
function cura_get_past_starters_leavers($count = 30) {
  $records = array();
  if ($starters = cura_get_past_starters($count)) {
    foreach ($starters as $starter) {
      $sw = entity_metadata_wrapper('node', $starter);
      $records[] = array(
        'date' => $sw->field_child_start_date->value(),
        'lastname' => $sw->cura_lastname->value(),
        'firstname' => $sw->cura_firstname->value(),
        'child_id' => $sw->getIdentifier(),
        'age' => date_diff(date_create($starter->field_birth_date[LANGUAGE_NONE][0]['value']),
                           date_create($starter->field_child_start_date[LANGUAGE_NONE][0]['value']))->format('%yy %mm'),
        'text' => 'Started ' . date('l, j F Y', $sw->field_child_start_date->value()),
        'color' => 'Green',
      );
    }
  }
  if ($leavers = cura_get_past_leavers($count)) {
    foreach ($leavers as $leaver) {
      $lw = entity_metadata_wrapper('node', $leaver);
      $records[] = array(
        'date' => $lw->field_child_finish_date->value(),
        'lastname' => $lw->cura_lastname->value(),
        'firstname' => $lw->cura_firstname->value(),
        'child_id' => $lw->getIdentifier(),
        'age' => date_diff(date_create($leaver->field_birth_date[LANGUAGE_NONE][0]['value']),
                           date_create($leaver->field_child_finish_date[LANGUAGE_NONE][0]['value']))->format('%yy %mm'),
        'text' => 'Left ' . date('l, j F Y', $lw->field_child_finish_date->value()),
      );
    }
  }

  // Sort the records on date, lastname, firstname, child ID.
  usort($records, function($a, $b) {
    if ($a['date'] - $b['date'] === 0) {
      if (strcmp($a['lastname'], $b['lastname']) === 0) {
        if (strcmp($a['firstname'], $b['firstname']) === 0) {
          return $a['child_id'] - $b['child_id'];
        }
        else {
          return strcmp($a['firstname'], $b['firstname']);
        }
      }
      else {
        return strcmp($a['lastname'], $b['lastname']);
      }
    }
    else {
      return $b['date'] - $a['date'];
    }
  });
  $records = array_slice($records, 0, $count);
//  drupal_set_message('After sorting:<br><pre>' . print_r($records, TRUE) . '</pre>'); // *** TESTING ***

  // Convert the records to html for email.
  $html = '<h2>' . t('Past starters and leavers') . '</h2>';
  if ($records) {
    $html .= '<p style="margin-left:10px;">' . t('This notification has been configured to display the last @count starters and leavers.', array('@count' => $count)) . '</p>';
    $html .= '<table style="margin-left:10px;"><thead>';
    $html .= '<tr style="background-color:#f5f5f5;">';
    $html .= '<th style="text-align:left;padding-right:10px;min-width:100px;">Date';
    $html .= '<th style="text-align:left;padding-right:10px;">Child';
    $html .= '<th style="text-align:right;padding-right:10px;">Age';
    $html .= '<th style="text-align:left;padding-right:10px;">';
    $html .= '</thead><tbody>';
    foreach ($records as $row => $record) {
      $colour = (($row + 1) % 2 == 1) ? '#fff' : '#f5f5f5';
      $html .= '<tr style="background-color:' . $colour;
      if (isset($record['color'])) $html .= ';color:' . $record['color'];
      $html .= '">';
      $html .= '<td style="text-align:left;padding-right:10px;">' . date('Y-m-d', $record['date']);
      $html .= '<td style="text-align:left;padding-right:10px;">' . $record['firstname'] . ' ' . $record['lastname'];
      $html .= '<td style="text-align:right;padding-right:10px;">' . $record['age'];
      $html .= '<td style="text-align:left;padding-right:10px;">' . $record['text'];
    }
    $html .= '</tbody></table>';
  }
  else {
    // No recent starters or leavers.
    $html .= '<p style="margin-left:10px;">' . t('No recent starters or leavers.') . '</p>';
  }

  return $html;
}

