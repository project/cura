<?php

/**
 * Views for the Attendance module of the Cura Childcare Suite.
 */

/**
 * Implements hook_views_default_views().
 */
function cura_attendance_views_default_views() {
  $views = array();

  // Define the attendance view.
  $view = new view();
  $view->name = 'cura_attendance';
  $view->description = 'Weekly attendance summary';
  $view->tag = 'cura_attendance';
  $view->base_table = 'cura_sessions';
  $view->human_name = 'Attendance';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Attendance';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Session: Session ID */
  $handler->display->display_options['fields']['session_id']['id'] = 'session_id';
  $handler->display->display_options['fields']['session_id']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['session_id']['field'] = 'session_id';

  /* Display: Weekly Attendance Plan */
  $handler = $view->new_display('page', 'Weekly Attendance Plan', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Weekly Attendance Plan';
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    8 => '8',
    5 => '5',
    6 => '6',
    7 => '7',
    3 => '3',
  );
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'date_views_pager';
  $handler->display->display_options['pager']['options']['skip_empty_pages'] = 0;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'date',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
    1 => array(
      'field' => 'session_type_label',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'session_id' => 'session_id',
    'date' => 'date',
    'session_type_id' => 'session_type_id',
    'session_type_label' => 'session_type_label',
    'nid' => 'nid',
    'cura_firstname' => 'cura_firstname',
    'cura_lastname' => 'cura_lastname',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'session_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'session_type_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'session_type_label' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cura_firstname' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cura_lastname' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Session: Child_id */
  $handler->display->display_options['relationships']['child_id']['id'] = 'child_id';
  $handler->display->display_options['relationships']['child_id']['table'] = 'cura_sessions';
  $handler->display->display_options['relationships']['child_id']['field'] = 'child_id';
  $handler->display->display_options['relationships']['child_id']['label'] = 'Child ID referencing Child';
  $handler->display->display_options['relationships']['child_id']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Session: Session ID */
  $handler->display->display_options['fields']['session_id']['id'] = 'session_id';
  $handler->display->display_options['fields']['session_id']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['session_id']['field'] = 'session_id';
  $handler->display->display_options['fields']['session_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['session_id']['element_label_colon'] = FALSE;
  /* Field: Session: Date */
  $handler->display->display_options['fields']['date']['id'] = 'date';
  $handler->display->display_options['fields']['date']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['date']['field'] = 'date';
  $handler->display->display_options['fields']['date']['exclude'] = TRUE;
  $handler->display->display_options['fields']['date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['date']['element_wrapper_type'] = 'h2';
  $handler->display->display_options['fields']['date']['date_format'] = 'custom';
  $handler->display->display_options['fields']['date']['custom_date_format'] = 'l, j F Y';
  $handler->display->display_options['fields']['date']['second_date_format'] = 'long';
  /* Field: Session: Session type ID */
  $handler->display->display_options['fields']['session_type_id']['id'] = 'session_type_id';
  $handler->display->display_options['fields']['session_type_id']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['session_type_id']['field'] = 'session_type_id';
  $handler->display->display_options['fields']['session_type_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['session_type_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['session_type_id']['separator'] = '';
  /* Field: Session: Session_type_label */
  $handler->display->display_options['fields']['session_type_label']['id'] = 'session_type_label';
  $handler->display->display_options['fields']['session_type_label']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['session_type_label']['field'] = 'session_type_label';
  $handler->display->display_options['fields']['session_type_label']['label'] = 'Session Type';
  $handler->display->display_options['fields']['session_type_label']['exclude'] = TRUE;
  $handler->display->display_options['fields']['session_type_label']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['session_type_label']['element_wrapper_type'] = 'h3';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'child_id';
  $handler->display->display_options['fields']['nid']['label'] = 'Child ID';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Firstname */
  $handler->display->display_options['fields']['cura_firstname']['id'] = 'cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['table'] = 'field_data_cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['field'] = 'cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['relationship'] = 'child_id';
  $handler->display->display_options['fields']['cura_firstname']['exclude'] = TRUE;
  $handler->display->display_options['fields']['cura_firstname']['element_label_colon'] = FALSE;
  /* Field: Content: Lastname */
  $handler->display->display_options['fields']['cura_lastname']['id'] = 'cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['table'] = 'field_data_cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['field'] = 'cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['relationship'] = 'child_id';
  $handler->display->display_options['fields']['cura_lastname']['exclude'] = TRUE;
  $handler->display->display_options['fields']['cura_lastname']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Child';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[cura_firstname] [cura_lastname] ([nid])';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Field: Session: Hours */
  $handler->display->display_options['fields']['hours']['id'] = 'hours';
  $handler->display->display_options['fields']['hours']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['hours']['field'] = 'hours';
  $handler->display->display_options['fields']['hours']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['hours']['precision'] = '0';
  $handler->display->display_options['fields']['hours']['separator'] = ' ';
  /* Field: Session: Hours_n */
  $handler->display->display_options['fields']['hours_n']['id'] = 'hours_n';
  $handler->display->display_options['fields']['hours_n']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['hours_n']['field'] = 'hours_n';
  $handler->display->display_options['fields']['hours_n']['label'] = 'Hours (Non-funded)';
  $handler->display->display_options['fields']['hours_n']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['hours_n']['precision'] = '0';
  $handler->display->display_options['fields']['hours_n']['separator'] = ' ';
  /* Field: Session: Hours_f */
  $handler->display->display_options['fields']['hours_f']['id'] = 'hours_f';
  $handler->display->display_options['fields']['hours_f']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['hours_f']['field'] = 'hours_f';
  $handler->display->display_options['fields']['hours_f']['label'] = 'Hours (Funded)';
  $handler->display->display_options['fields']['hours_f']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['hours_f']['precision'] = '0';
  $handler->display->display_options['fields']['hours_f']['separator'] = ' ';
  /* Field: Session: Amount */
  $handler->display->display_options['fields']['amount']['id'] = 'amount';
  $handler->display->display_options['fields']['amount']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['amount']['field'] = 'amount';
  $handler->display->display_options['fields']['amount']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['amount']['precision'] = '0';
  $handler->display->display_options['fields']['amount']['prefix'] = '£';
  /* Field: Session: Amount_n */
  $handler->display->display_options['fields']['amount_n']['id'] = 'amount_n';
  $handler->display->display_options['fields']['amount_n']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['amount_n']['field'] = 'amount_n';
  $handler->display->display_options['fields']['amount_n']['label'] = 'Amount (Non-funded)';
  $handler->display->display_options['fields']['amount_n']['precision'] = '0';
  $handler->display->display_options['fields']['amount_n']['prefix'] = '£';
  /* Field: Session: Amount_f */
  $handler->display->display_options['fields']['amount_f']['id'] = 'amount_f';
  $handler->display->display_options['fields']['amount_f']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['amount_f']['field'] = 'amount_f';
  $handler->display->display_options['fields']['amount_f']['label'] = 'Amount (Funded)';
  $handler->display->display_options['fields']['amount_f']['precision'] = '0';
  $handler->display->display_options['fields']['amount_f']['prefix'] = '£';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Session: Date */
  $handler->display->display_options['sorts']['date']['id'] = 'date';
  $handler->display->display_options['sorts']['date']['table'] = 'cura_sessions';
  $handler->display->display_options['sorts']['date']['field'] = 'date';
  /* Sort criterion: Session: Start */
  $handler->display->display_options['sorts']['start']['id'] = 'start';
  $handler->display->display_options['sorts']['start']['table'] = 'cura_sessions';
  $handler->display->display_options['sorts']['start']['field'] = 'start';
  /* Sort criterion: Content: Lastname (cura_lastname) */
  $handler->display->display_options['sorts']['cura_lastname_value']['id'] = 'cura_lastname_value';
  $handler->display->display_options['sorts']['cura_lastname_value']['table'] = 'field_data_cura_lastname';
  $handler->display->display_options['sorts']['cura_lastname_value']['field'] = 'cura_lastname_value';
  $handler->display->display_options['sorts']['cura_lastname_value']['relationship'] = 'child_id';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Session: Date */
  $handler->display->display_options['filters']['date']['id'] = 'date';
  $handler->display->display_options['filters']['date']['table'] = 'cura_sessions';
  $handler->display->display_options['filters']['date']['field'] = 'date';
  $handler->display->display_options['filters']['date']['operator'] = '>=';
  $handler->display->display_options['filters']['date']['value']['value'] = 'mon';
  $handler->display->display_options['filters']['date']['value']['type'] = 'offset';
  /* Filter criterion: Session: Date */
  $handler->display->display_options['filters']['date_1']['id'] = 'date_1';
  $handler->display->display_options['filters']['date_1']['table'] = 'cura_sessions';
  $handler->display->display_options['filters']['date_1']['field'] = 'date';
  $handler->display->display_options['filters']['date_1']['operator'] = '<=';
  $handler->display->display_options['filters']['date_1']['value']['value'] = 'mon +6 days';
  $handler->display->display_options['filters']['date_1']['value']['type'] = 'offset';
  $handler->display->display_options['path'] = 'attendance';

  /* Display: Weekly Attendance Plan Summary */
  $handler = $view->new_display('attachment', 'Weekly Attendance Plan Summary', 'attachment_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Weekly Summary';
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    8 => '8',
    5 => '5',
    6 => '6',
    7 => '7',
    3 => '3',
  );
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '14';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'session_id' => 'session_id',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'session_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Summary';
  $handler->display->display_options['header']['area']['content'] = '<h2>Summary</h2>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Session: Date */
  $handler->display->display_options['fields']['date']['id'] = 'date';
  $handler->display->display_options['fields']['date']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['date']['field'] = 'date';
  $handler->display->display_options['fields']['date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['date']['date_format'] = 'custom';
  $handler->display->display_options['fields']['date']['custom_date_format'] = 'D, j M Y';
  $handler->display->display_options['fields']['date']['second_date_format'] = 'long';
  /* Field: Session: Child_id */
  $handler->display->display_options['fields']['child_id']['id'] = 'child_id';
  $handler->display->display_options['fields']['child_id']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['child_id']['field'] = 'child_id';
  $handler->display->display_options['fields']['child_id']['group_type'] = 'count_distinct';
  $handler->display->display_options['fields']['child_id']['label'] = 'Children';
  $handler->display->display_options['fields']['child_id']['element_type'] = 'span';
  $handler->display->display_options['fields']['child_id']['element_class'] = 'text-right';
  $handler->display->display_options['fields']['child_id']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['child_id']['element_label_class'] = 'text-right';
  $handler->display->display_options['fields']['child_id']['element_label_colon'] = FALSE;
  /* Field: Session: Hours */
  $handler->display->display_options['fields']['hours']['id'] = 'hours';
  $handler->display->display_options['fields']['hours']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['hours']['field'] = 'hours';
  $handler->display->display_options['fields']['hours']['group_type'] = 'sum';
  $handler->display->display_options['fields']['hours']['element_type'] = 'span';
  $handler->display->display_options['fields']['hours']['element_class'] = 'text-right';
  $handler->display->display_options['fields']['hours']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['hours']['element_label_class'] = 'text-right';
  $handler->display->display_options['fields']['hours']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['hours']['precision'] = '2';
  /* Field: Session: Hours_n */
  $handler->display->display_options['fields']['hours_n']['id'] = 'hours_n';
  $handler->display->display_options['fields']['hours_n']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['hours_n']['field'] = 'hours_n';
  $handler->display->display_options['fields']['hours_n']['group_type'] = 'sum';
  $handler->display->display_options['fields']['hours_n']['label'] = 'Hours (Non-funded)';
  $handler->display->display_options['fields']['hours_n']['element_type'] = 'span';
  $handler->display->display_options['fields']['hours_n']['element_class'] = 'text-right';
  $handler->display->display_options['fields']['hours_n']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['hours_n']['element_label_class'] = 'text-right';
  $handler->display->display_options['fields']['hours_n']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['hours_n']['precision'] = '2';
  /* Field: Session: Hours_f */
  $handler->display->display_options['fields']['hours_f']['id'] = 'hours_f';
  $handler->display->display_options['fields']['hours_f']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['hours_f']['field'] = 'hours_f';
  $handler->display->display_options['fields']['hours_f']['group_type'] = 'sum';
  $handler->display->display_options['fields']['hours_f']['label'] = 'Hours (Funded)';
  $handler->display->display_options['fields']['hours_f']['element_type'] = 'span';
  $handler->display->display_options['fields']['hours_f']['element_class'] = 'text-right';
  $handler->display->display_options['fields']['hours_f']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['hours_f']['element_label_class'] = 'text-right';
  $handler->display->display_options['fields']['hours_f']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['hours_f']['precision'] = '2';
  /* Field: Session: Amount */
  $handler->display->display_options['fields']['amount']['id'] = 'amount';
  $handler->display->display_options['fields']['amount']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['amount']['field'] = 'amount';
  $handler->display->display_options['fields']['amount']['group_type'] = 'sum';
  $handler->display->display_options['fields']['amount']['element_type'] = 'span';
  $handler->display->display_options['fields']['amount']['element_class'] = 'text-right';
  $handler->display->display_options['fields']['amount']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['amount']['element_label_class'] = 'text-right';
  $handler->display->display_options['fields']['amount']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['amount']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['amount']['precision'] = '2';
  $handler->display->display_options['fields']['amount']['prefix'] = '£';
  /* Field: Session: Amount_n */
  $handler->display->display_options['fields']['amount_n']['id'] = 'amount_n';
  $handler->display->display_options['fields']['amount_n']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['amount_n']['field'] = 'amount_n';
  $handler->display->display_options['fields']['amount_n']['group_type'] = 'sum';
  $handler->display->display_options['fields']['amount_n']['label'] = 'Amount (Non-funded)';
  $handler->display->display_options['fields']['amount_n']['element_type'] = 'span';
  $handler->display->display_options['fields']['amount_n']['element_class'] = 'text-right';
  $handler->display->display_options['fields']['amount_n']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['amount_n']['element_label_class'] = 'text-right';
  $handler->display->display_options['fields']['amount_n']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['amount_n']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['amount_n']['precision'] = '2';
  $handler->display->display_options['fields']['amount_n']['prefix'] = '£';
  /* Field: Session: Amount_f */
  $handler->display->display_options['fields']['amount_f']['id'] = 'amount_f';
  $handler->display->display_options['fields']['amount_f']['table'] = 'cura_sessions';
  $handler->display->display_options['fields']['amount_f']['field'] = 'amount_f';
  $handler->display->display_options['fields']['amount_f']['group_type'] = 'sum';
  $handler->display->display_options['fields']['amount_f']['label'] = 'Amount (Funded)';
  $handler->display->display_options['fields']['amount_f']['element_type'] = 'span';
  $handler->display->display_options['fields']['amount_f']['element_class'] = 'text-right';
  $handler->display->display_options['fields']['amount_f']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['amount_f']['element_label_class'] = 'text-right';
  $handler->display->display_options['fields']['amount_f']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['amount_f']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['amount_f']['precision'] = '2';
  $handler->display->display_options['fields']['amount_f']['prefix'] = '£';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Session: Date */
  $handler->display->display_options['sorts']['date']['id'] = 'date';
  $handler->display->display_options['sorts']['date']['table'] = 'cura_sessions';
  $handler->display->display_options['sorts']['date']['field'] = 'date';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Session: Date */
  $handler->display->display_options['filters']['date']['id'] = 'date';
  $handler->display->display_options['filters']['date']['table'] = 'cura_sessions';
  $handler->display->display_options['filters']['date']['field'] = 'date';
  $handler->display->display_options['filters']['date']['operator'] = '>=';
  $handler->display->display_options['filters']['date']['value']['value'] = 'mon';
  $handler->display->display_options['filters']['date']['value']['type'] = 'offset';
  /* Filter criterion: Session: Date */
  $handler->display->display_options['filters']['date_1']['id'] = 'date_1';
  $handler->display->display_options['filters']['date_1']['table'] = 'cura_sessions';
  $handler->display->display_options['filters']['date_1']['field'] = 'date';
  $handler->display->display_options['filters']['date_1']['operator'] = '<=';
  $handler->display->display_options['filters']['date_1']['value']['value'] = 'mon +6 days';
  $handler->display->display_options['filters']['date_1']['value']['type'] = 'offset';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );

  $views[$view->name] = $view;

  return $views;
}

