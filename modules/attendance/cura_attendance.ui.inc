<?php

/**
 * @file cura_attendance.ui.inc
 * User interface functions for the Attendance module of the Cura Childcare Suite.
 */

/**
 * Form for displaying attendance forecasts.
 */
function cura_attendance_forecast_form($form, &$form_state, $date = NULL) {
  if (empty($date)) $date = REQUEST_TIME;
  if (!isset($form_state['storage']['date'])) $form_state['storage']['date'] = date('Y-m-d', $date);
  $form['filter'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
    '#prefix' => '<table><tr>',
    '#suffix' => '</tr></table>',
  );
  $form['filter']['date'] = array(
    '#title' => t('Date'),
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#date_label_position' => 'within',
    '#default_value' => $form_state['storage']['date'],
    '#required' => TRUE,
    '#prefix' => '<td>',
    '#suffix' => '</td>',
  );
  $form['filter']['type'] = array(
    '#title' => t('Type'),
    '#type' => 'textfield',
    '#prefix' => '<td>',
    '#suffix' => '</td>',
  );
  $form['filter']['actions'] = array('#type' => 'actions');
  $form['filter']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#prefix' => '<td style="vertical-align:bottom;padding:10px;">',
    '#suffix' => '</td>',
  );

  // Attendance forecast
  $start = strtotime('last monday', strtotime('tomorrow', strtotime($form_state['storage']['date'])));
  $finish = strtotime(date('Y-m-d', strtotime('+6 days', $start)));
  $period = date('l, j F Y', $start) . ' to ' . date('l, j F Y', $finish);
  $forecast = '<h1>Weekly plan</h1>';
  $forecast .= '<p>For the period ' . $period . '.</p>';
  $title = t('Session summary');
  $records = cura_get_session_summary($start, $finish);
  $forecast .= theme('cura_hours_per_day', array('title' => $title, 'caption' => NULL, 'records' => $records));
  $title = t('Day of week summary');
  $sessions = cura_get_sessions($start, $finish);
  $data = array();
  $data['start'] = $start;
  $data['finish'] = $finish;
  foreach ($sessions as $session) {
    $child = $session->firstname . ' ' . $session->lastname . ' (' . $session->nid . ')';
    if (!isset($data['rows'][$child])) {
      $data['rows'][$child] = array('Mon' => 0, 'Tue' => 0, 'Wed' => 0, 'Thu' => 0, 'Fri' => 0, 'Sat' => 0, 'Sun' => 0);
      $data['rows'][$child]['child_id'] = $session->nid;
      $data['rows'][$child]['firstname'] = $session->firstname;
      $data['rows'][$child]['lastname'] = $session->lastname;
      $data['rows'][$child]['birthdate'] = $session->birthdate;
    }
    $data['rows'][$child][date('D', $session->date)] += $session->hours;
  }
  $forecast .= theme('cura_hours_per_child', array('title' => $title, 'data' => $data));

  $form['summary'] = array(
    '#markup' => $forecast,
  );

  return $form;
}

/**
 * Submission handler for the attendance forecast form.
 */
function cura_attendance_forecast_form_submit($form, &$form_state) {
  $form_state['storage']['date'] = $form_state['values']['date'];
  $form_state['rebuild'] = TRUE;
}

