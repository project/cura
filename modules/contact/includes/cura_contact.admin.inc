<?php

/**
 * @file
 * Administrative page callbacks for the Contact module of the Cura Childcare Suite.
 */

/**
 * Form constructor for the Contacts settings page.
 *
 * @see cura_contact_menu()
 * @see cura_settings_form()
 * @ingroup forms
 */
function cura_contact_config_form($form, &$form_state) {
  $settings = cura_get_settings();

  $form['settings']['general']['contact']['extract']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the scheduled extract of contacts.'),
    '#default_value' => $settings['general']['contact']['extract']['enabled'],
    '#access' => user_access('administer cura settings'),
  );

  return cura_settings_form($form);
}

