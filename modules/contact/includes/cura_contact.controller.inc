<?php

/**
 * @file
 * The controller for the Cura contact entity containing the CRUD operations.
 */

/**
 * The controller class for contacts contains methods for the contact CRUD operations.
 *
 * Mainly relies on the EntityAPIController class provided by the Entity
 * module, just overrides specific features.
 */
class CuraContactEntityController extends CuraEntityController {

  /**
   * Creates a Cura contact.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   * @return
   *   A cura_contact object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $account = $GLOBALS['user'];
    $values += array(
      'label' => 'Contact',
      'type' => 'Parent',
      'data' => array(),
      'uid' => $account->uid,
      'status' => 'N',
    );

    return parent::create($values);
  }

  /**
   * Saves a Cura contact.
   *
   * @param $contact
   *   The full contact object to save.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($contact, DatabaseTransaction $transaction = NULL) {
    if (empty($contact)) return FALSE;

    $ip = ip_address();

    // Check if this contact is new.
    if (empty($contact->{$this->idKey})) {
      $contact->created_on = REQUEST_TIME;
      $contact->created_ip = $ip;
    }

    $contact->updated_on = REQUEST_TIME;
    $contact->updated_ip = $ip;
    if (empty($contact->active)) $contact->active = 1;

    return parent::save($contact, $transaction);
  }

  /**
   * Unserializes the data property of loaded tasks.
   */
  public function attachLoad(&$queried_contacts, $revision_id = FALSE) {
//    foreach ($queried_contacts as $contact_id => &$contact) {
//      $contact->data = unserialize($contact->data);
//    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_commerce_product_load().
    parent::attachLoad($queried_contacts, $revision_id);
  }

  /**
   * Deletes multiple contacts by ID.
   *
   * @param $contact_ids
   *   An array of contact IDs to delete.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($contact_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($contact_ids)) {
      $contacts = $this->load($contact_ids, array());

      // Ensure the contacts can actually be deleted.
//      foreach ((array) $contacts as $contact_id => $contact) {
//        if (!cura_contact_can_delete($contact)) {
//          unset($contacts[$contact_id]);
//        }
//      }

      // If none of the specified contacts can be deleted, return FALSE.
      if (empty($contacts)) {
        return FALSE;
      }

      parent::delete(array_keys($contacts), $transaction);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Builds a structured array representing the entity's content.
   *
   * The content built for the entity will vary depending on the $view_mode
   * parameter.
   *
   * @param $entity
   *   An entity object.
   * @param $view_mode
   *   View mode, e.g. 'full', 'teaser'...
   * @param $langcode
   *   (optional) A language code to use for rendering. Defaults to the global
   *   content language of the current request.
   * @return
   *   The renderable array.
   */
  public function buildContent($contact, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Prepare a reusable array representing the CSS file to attach to the view.
    $attached = array(
      'css' => array(drupal_get_path('module', 'commerce_product') . '/theme/commerce_product.theme.css'),
    );

    // Add the default fields inherent to the entity.
    $content['contact_id'] = array(
      '#markup' => 'Contact ID: ' . $contact->contact_id,
      '#attached' => $attached,
    );
    $content['label'] = array(
      '#markup' => 'Label: ' . $contact->label,
      '#attached' => $attached,
    );
    $content['status'] = array(
      '#markup' => 'Status: ' . $contact->status,
      '#attached' => $attached,
    );

    return parent::buildContent($contact, $view_mode, $langcode, $content);
  }
}

