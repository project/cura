<?php

/**
 * @file cura_contact.ui.inc
 * User interface functions for the Contact module of the Cura Childcare Suite.
 */

/**
 * Form callback wrapper: create or edit a contact.
 *
 * @param object $contact
 *   The contact object to edit through the form.
 * @param object $account
 *   The user account object.
 */
function cura_contact_form_wrapper($child = NULL, $contact = NULL) {

  // Set the page title.
  if (empty($contact->contact_id)) {
    drupal_set_title(t('Create a new contact'));
  }
  else {
    drupal_set_title(t('Edit contact'));
  }

  return drupal_get_form('cura_contact_form', $child, $contact);
}

/**
 * Form for editing a contact.
 */
function cura_contact_form($form, &$form_state, $child, $contact) {
  $form_state['cura_child'] = $child;
  $form_state['cura_contact'] = $contact;
  if (isset($contact->person_id)) $person = node_load($contact->person_id);

  $options = array('Parent' => 'Parent');
  $options = cura_taxonomy_options('cura_contact_type', 'name', $options);
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => $options,
    '#default_value' => 'Parent',
    '#required' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'container',
    '#title' => t('Name'),
    '#tree' => TRUE,
  );
  $form['name']['firstname'] = array(
    '#type' => 'textfield',
    '#title' => t('Firstname'),
    '#size' => 20,
    '#maxlength' => 40,
    '#default_value' => isset($person) ? $person->field_firstname[LANGUAGE_NONE][0]['value'] : NULL,
    '#required' => TRUE,
    '#attributes' => array('placeholder' => t('First name')),
  );
  $form['name']['lastname'] = array(
    '#type' => 'textfield',
    '#title' => t('Lastname'),
    '#size' => 20,
    '#maxlength' => 40,
    '#default_value' => isset($person) ? $person->field_lastname[LANGUAGE_NONE][0]['value'] : NULL,
    '#required' => TRUE,
    '#attributes' => array('placeholder' => t('Last name')),
  );
  $form['contact'] = array(
    '#type' => 'container',
    '#title' => t('Contact'),
    '#tree' => TRUE,
  );
  $form['contact']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#maxlength' => 255,
    '#default_value' => isset($person) ? $person->field_email[LANGUAGE_NONE][0]['email'] : NULL,
    '#required' => TRUE,
    '#attributes' => array('placeholder' => t('Email address')),
  );
  $form['contact']['mobile'] = array(
    '#type' => 'textfield',
    '#title' => t('Mobile'),
    '#size' => 20,
    '#maxlength' => 40,
    '#default_value' => isset($person) ? $person->field_mobile[LANGUAGE_NONE][0]['value'] : NULL,
  );
  $form['contact']['home_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Home phone'), 
    '#size' => 20,
    '#maxlength' => 40,
    '#default_value' => isset($person) ? $person->field_home_phone[LANGUAGE_NONE][0]['value'] : NULL,
    '#required' => TRUE,
  );
  $form['contact']['work_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Work phone'),
    '#size' => 20,
    '#maxlength' => 40,
    '#default_value' => isset($person->field_work_phone[LANGUAGE_NONE][0]['value']) ? $person->field_work_phone[LANGUAGE_NONE][0]['value'] : NULL,
  );
  $form['address'] = array(
    '#type' => 'container',
    '#title' => t('Address'),
    '#tree' => TRUE,
  );
  $form['address']['line1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address 1'),
    '#size' => 40,
    '#maxlength' => 80,
    '#default_value' => isset($person) ? $person->field_physical_address[LANGUAGE_NONE][0]['thoroughfare'] : NULL,
    '#required' => TRUE,
  );
  $form['address']['line2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address 2'),
    '#size' => 40,
    '#maxlength' => 80,
    '#default_value' => isset($person) ? $person->field_physical_address[LANGUAGE_NONE][0]['premise'] : NULL,
  );
  $form['address']['city'] = array(
    '#type' => 'textfield',
    '#title' => t('Town/City'),
    '#size' => 30,
    '#maxlength' => 40,
    '#default_value' => isset($person) ? $person->field_physical_address[LANGUAGE_NONE][0]['locality'] : NULL,
    '#required' => TRUE,
  );
  $form['address']['county'] = array(
    '#type' => 'textfield',
    '#title' => t('County'),
    '#size' => 40,
    '#maxlength' => 80,
    '#default_value' => isset($person) ? $person->field_physical_address[LANGUAGE_NONE][0]['administrative_area'] : NULL,
  );
  $form['address']['postcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Postcode'),
    '#size' => 15,
    '#maxlength' => 35,
    '#default_value' => isset($person) ? $person->field_physical_address[LANGUAGE_NONE][0]['postal_code'] : NULL,
    '#required' => TRUE,
  );
  $form['address']['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => country_get_list(),
    '#default_value' => isset($person) ? $person->field_physical_address[LANGUAGE_NONE][0]['country'] : variable_get('site_default_country', 'GB'),
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );
  if (isset($person)) $form['actions']['submit']['#value'] = t('Update');

  // Add a cancel button at the bottom of the form.
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : '<front>',
  );

  return $form;
}

/**
 * Validation handler for the contact form.
 */
function cura_contact_form_validate($form, &$form_state) {
//  drupal_set_message('<pre>' . print_r($form_state['values'], TRUE) . '</pre>');
  if (!valid_email_address($form_state['values']['contact']['email'])) {
    form_set_error('contact][email', t('You must enter a valid e-mail address.'));
  }
}

/**
 * Submission handler for the contact form.
 */
function cura_contact_form_submit($form, &$form_state) {

  module_load_include('inc', 'cura', 'cura.admissions');

  // Get the child and contact from the form.
  $child = $form_state['cura_child'];
  $contact = $form_state['cura_contact'];

  switch ($form_state['values']['op']) {

    case 'Create':
      $person_id = cura_person_save(array(
        'firstname' => $form_state['values']['name']['firstname'],
        'lastname' => $form_state['values']['name']['lastname'],
        'birthdate' => NULL,
        'email' => $form_state['values']['contact']['email'],
        'mobile' => $form_state['values']['contact']['mobile'],
        'home_phone' => $form_state['values']['contact']['home_phone'],
        'work_phone' => $form_state['values']['contact']['work_phone'],
        'address_1' => $form_state['values']['address']['line1'],
        'city' => $form_state['values']['address']['city'],
        'county' => $form_state['values']['address']['county'],
        'postcode' => $form_state['values']['address']['postcode'],
        'country' => $form_state['values']['address']['country'],
      ));
      if (!empty($person_id)) {
        $person = node_load($person_id);
        $name = $person->field_firstname[LANGUAGE_NONE][0]['value'] . ' ' . $person->field_lastname[LANGUAGE_NONE][0]['value'];
        // drupal_set_message('<h3>Person<h3><pre>' . print_r($person, TRUE) . '</pre>');
        $sql = 'SELECT 1 FROM {cura_contact}';
        $sql .= ' WHERE child_id = :child_id AND person_id = :person_id';
        $vars = array(
          ':child_id' => $child->nid,
          ':person_id' => $person_id,
        );
        $contact_exists = db_query($sql, $vars)->fetchField();
        if (!$contact_exists) {
          if ($contact = cura_contact_new($child->nid, $person_id)) {
            if (cura_contact_save($contact)) {
              watchdog('cura_contact', 'Added contact %i (@n)', array('%i' => $contact->contact_id, '@n' => $name));
            }
          }
        }
      }
      break;

    case 'Update':
      break;

  }
  $form_state['redirect'] = 'child/' . $child->nid;
}

