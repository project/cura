<?php

/**
 * @file
 * Default theme implementation to display the contact form.
 *
 * Available variables:
 * - $title: the (sanitized) title of the summary.
 * - $records: An array of summary records to display.
 *
 * @see template_preprocess()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class="row">
  <?php print drupal_render($form['type']); ?>
</div>
<div class="row">
  <div style="display:inline-block">
    <?php print drupal_render($form['name']['firstname']); ?>
  </div>
  <div style="display:inline-block">
    <?php print drupal_render($form['name']['lastname']); ?>
  </div>
</div>
<div class="row">
  <div style="display:inline-block">
    <?php print drupal_render($form['contact']['email']); ?>
    <?php print drupal_render($form['contact']['mobile']); ?>
  </div>
  <div style="display:inline-block">
    <?php print drupal_render($form['contact']['home_phone']); ?>
    <?php print drupal_render($form['contact']['work_phone']); ?>
  </div>
</div>
<div class="row">
  <div>
    <?php print drupal_render($form['address']['line1']); ?>
    <?php print drupal_render($form['address']['line2']); ?>
    <?php print drupal_render($form['address']['city']); ?>
  </div>
  <div style="display:inline-block">
    <?php print drupal_render($form['address']['county']); ?>
  </div>
  <div style="display:inline-block">
    <?php print drupal_render($form['address']['postcode']); ?>
  </div>
  <div>
    <?php print drupal_render($form['address']['country']); ?>
  </div>
</div>

<?php print drupal_render_children($form); ?>

