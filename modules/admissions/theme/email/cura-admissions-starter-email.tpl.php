<?php

/**
 * @file
 * The template for the New Starter email.
 *
 * Available variables:
 * - $body: The body of the email.
 * - $footer: The user-configurable email footer.
 */
?>

<?php print $body; ?>

<?php print $footer; ?>

