<?php

/**
 * @file
 * The template for the Offer email body.
 *
 * Available variables:
 * - $body: The main body of the email.
 * - $footer: The user-configurable email footer.
 */
?>
<?php print $body; ?>
<?php print $footer; ?>

