<?php

/**
 * @file
 * Form template for responding to offers.
 *
 * Available variables:
 * - $form: The form element array.
 *
 * @see template_preprocess()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php $cw = entity_metadata_wrapper('node', $form['child_id']['#value']); ?>
<?php $child_image = $cw->field_child_image->value(); ?>
<?php $child_image_uri = $child_image ? $child_image['uri'] : '/sites/default/files/default_images/default-person-image_2.jpg'; ?>
<?php $child_plan = cura_display_session_plan($cw->value(), $cw->field_child_start_date->value()); ?>

<div class="row">
  <div style="float:right;">
    <img src="<?php print file_create_url($child_image_uri); ?>" width="200px" height="200px" alt="Picture" class="img-circle"><br>
    <?php //print $cw->field_child_image->file->url->value(); ?>
  </div>
  <div>
    <p><?php print $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value(); ?></p>
    <p><?php print 'Start date: ' . date('l, j F Y', $cw->field_child_start_date->value()); ?></p>
  </div>
</div>
<div>
  <?php print render($child_plan); ?>
</div>
<?php //print '--- form ---<br><pre>' . print_r($form, TRUE) . '</pre>--- end form ---<br>'; ?>

<?php print drupal_render_children($form); ?>

