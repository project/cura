<?php

/**
 * @file
 * Default theme implementation to display the registration form.
 *
 * Available variables:
 * - $title: the (sanitized) title of the summary.
 * - $records: An array of summary records to display.
 *
 * @see template_preprocess()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php //print '<pre>' . print_r($form, TRUE) . '</pre>'; ?>

<?php // Page 1. ?>

<?php if (isset($form['data']['child']['firstname'])) { ?>
  <fieldset class="collapsible panel panel-default form-wrapper collapse-processed" id="edit-child">
    <legend class="panel-heading">
      <a href="#" class="panel-title fieldset-legend" data-toggle="collapse" data-target="#edit-child > .collapse">
        <span class="fieldset-legend-prefix element-invisible">Hide</span>Child
      </a>
    </legend>
    <div style="padding:15px;">Enter the details for the child. For purposes of identification you will be required to produce a birth certificate or current passport for your child and the names and date of birth entered on this form must correspond with the details on the identification document.</div>
    <div class="panel-collapse collapse fade in">
      <div class="row">
        <div class="col-xs-12 col-sm-8">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['firstname']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['lastname']); ?>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4">
          <?php print drupal_render($form['data']['child']['birthdate']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['gender']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['ethnicity']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['language']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['child']['dietary']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['child']['sen']); ?>
        </div>
      </div>
    </div>
  </fieldset>
  <fieldset class="collapsible panel panel-default form-wrapper collapse-processed" id="edit-address">
    <legend class="panel-heading">
      <a href="#" class="panel-title fieldset-legend" data-toggle="collapse" data-target="#edit-address > .collapse">
        <span class="fieldset-legend-prefix element-invisible">Hide</span>Address
      </a>
    </legend>
    <div class="panel-collapse collapse fade in">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['address']['line1']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['address']['line2']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['address']['city']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['address']['county']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['address']['postcode']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['address']['country']); ?>
        </div>
      </div>
    </div>
  </fieldset>
<?php } ?>

<?php // Page 2. ?>

<?php if (isset($form['data']['parent1']['title'])) { ?>
  <fieldset class="collapsible panel panel-default form-wrapper collapse-processed" id="edit-parent1">
    <legend class="panel-heading">
      <a href="#" class="panel-title fieldset-legend" data-toggle="collapse" data-target="#edit-parent1 > .collapse">
        <span class="fieldset-legend-prefix element-invisible">Hide</span>Primary parent/carer details
      </a>
    </legend>
    <div style="padding:15px;">Please provide the details of the primary parent/carer that will be interacting with Polstead Preschool. The primary parent/carer is the person with parental responsibility and with which the child normally resides.</div>
    <div class="panel-collapse collapse fade in">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent1']['title']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent1']['firstname']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent1']['lastname']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['parent1']['email']); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['parent1']['mobile']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['parent1']['home_phone']); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['parent1']['work_phone']); ?>
        </div>
      </div>
    </div>
  </fieldset>
  <fieldset class="collapsible panel panel-default form-wrapper collapse-processed" id="edit-parent2">
    <legend class="panel-heading">
      <a href="#" class="panel-title fieldset-legend" data-toggle="collapse" data-target="#edit-parent2 > .collapse">
        <span class="fieldset-legend-prefix element-invisible">Hide</span>Second parent/carer details
      </a>
    </legend>
    <div style="padding:15px;">Please provide the details of an additional parent/carer and specify whether this person also has parental responsibility for the child. Provide the address for this person should it differ from the primary parent/carer.</div>
    <div class="panel-collapse collapse fade in">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['parent2']['parental_responsibility']); ?>
        </div>
      </div> 
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent2']['title']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent2']['firstname']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent2']['lastname']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['parent2']['email']); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['parent2']['mobile']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['parent2']['home_phone']); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['parent2']['work_phone']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['parent2']['address']['line1']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['parent2']['address']['line2']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent2']['address']['city']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent2']['address']['county']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['parent2']['address']['postcode']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['parent2']['address']['country']); ?>
        </div>
      </div>
    </div>
  </fieldset>
<?php } ?>

<?php // Page 3. ?>

<?php if (isset($form['data']['child']['doctor'])) { ?>
  <fieldset class="collapsible panel panel-default form-wrapper collapse-processed" id="edit-health">
    <legend class="panel-heading">
      <a href="#" class="panel-title fieldset-legend" data-toggle="collapse" data-target="#edit-health > .collapse">
        <span class="fieldset-legend-prefix element-invisible">Hide</span>Medical information
      </a>
    </legend>
    <div style="padding:15px;">The information in this area will help us effectively manage the health of your child.</div>
    <div class="panel-collapse collapse fade in">
      <fieldset>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['doctor']['title']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['doctor']['firstname']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['doctor']['lastname']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['child']['doctor']['email']); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['child']['doctor']['mobile']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['child']['doctor']['home_phone']); ?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?php print drupal_render($form['data']['child']['doctor']['work_phone']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['child']['doctor']['address']['line1']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['child']['doctor']['address']['line2']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['doctor']['address']['city']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['doctor']['address']['county']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['doctor']['address']['postcode']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['child']['doctor']['address']['country']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['health_visitor']['name']); ?>
          </div>
          <div style="display:inline-block">
            <?php print drupal_render($form['data']['child']['health_visitor']['phone']); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <?php print drupal_render($form['data']['child']['medical_conditions']); ?>
        </div>
      </div>
      </fieldset>
    </div>
  </fieldset>
<?php } ?>

<?php print drupal_render_children($form); ?>

