<?php

/**
 * @file
 * Form template for attendance plans.
 *
 * Available variables:
 * - $form: The form element array.
 *
 * @see template_preprocess()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php //print '--- form ---<br><pre>' . print_r($form, TRUE) . '</pre>--- end form ---<br>'; ?>

<table class="table">
 <thead>
    <tr><td>Session<td>Mon<td>Tue<td>Wed<td>Thu<td>Fri<td>Sat<td>Sun
  </thead>
  <tbody>
    <?php foreach ($form['attendance_plan']['session_types']['#value'] as $session_type_id => $session) { ?>
<?php //print 'session:<br><pre>' . print_r($session, TRUE) . '</pre>--- end session ---<br>'; ?>
      <tr><td><?php print $form['attendance_plan']['sessions'][$session->label]['label']['#value']; ?>
      <?php foreach (array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun') as $day) { ?>
        <td><?php print render($form['attendance_plan']['sessions'][$session->label]['days'][$day]); ?>
      <?php } ?>
    <?php } ?>
  </tbody>
</table>

<?php print drupal_render_children($form); ?>

