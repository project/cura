<?php

/**
 * @file
 * The controller for the Cura registration entity containing the CRUD operations.
 */

/**
 * The controller class for registrations contains methods for the registration CRUD operations.
 *
 * Mainly relies on the EntityAPIController class provided by the Entity
 * module, just overrides specific features.
 */
class CuraRegistrationEntityController extends CuraEntityController {

  /**
   * Creates a Cura registration.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   * @return
   *   A cura_registration object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $account = $GLOBALS['user'];
    $values += array(
      'label' => 'Registration',
      'data' => serialize(array()),
      'uid' => $account->uid,
      'status' => 'N',
    );

    return parent::create($values);
  }

  /**
   * Saves a Cura registration.
   *
   * @param $registration
   *   The full registration object to save.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($registration, DatabaseTransaction $transaction = NULL) {
    if (empty($registration)) return FALSE;

    $ip = ip_address();

    // Check if this registration is new.
    if (empty($registration->{$this->idKey})) {
      $registration->created_on = REQUEST_TIME;
      $registration->created_ip = $ip;
    }

    $registration->updated_on = REQUEST_TIME;
    $registration->updated_ip = $ip;
    if (empty($registration->active)) $registration->active = 1;

    return parent::save($registration, $transaction);
  }

  /**
   * Unserializes the data property of loaded tasks.
   */
  public function attachLoad(&$queried_registrations, $revision_id = FALSE) {
    foreach ($queried_registrations as $registration_id => &$registration) {
      $registration->data = unserialize($registration->data);
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_registration_load().
    parent::attachLoad($queried_registrations, $revision_id);
  }

  /**
   * Deletes multiple registrations by ID.
   *
   * @param $registration_ids
   *   An array of registration IDs to delete.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($registration_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($registration_ids)) {
      $registrations = $this->load($registration_ids, array());

      // Ensure the registrations can actually be deleted.
//      foreach ((array) $registrations as $registration_id => $registration) {
//        if (!cura_registration_can_delete($registration)) {
//          unset($registrations[$registration_id]);
//        }
//      }

      // If none of the specified registrations can be deleted, return FALSE.
      if (empty($registrations)) {
        return FALSE;
      }

      parent::delete(array_keys($registrations), $transaction);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Builds a structured array representing the entity's content.
   *
   * The content built for the entity will vary depending on the $view_mode
   * parameter.
   *
   * @param $entity
   *   An entity object.
   * @param $view_mode
   *   View mode, e.g. 'full', 'teaser'...
   * @param $langcode
   *   (optional) A language code to use for rendering. Defaults to the global
   *   content language of the current request.
   * @return
   *   The renderable array.
   */
  public function buildContent($registration, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Prepare a reusable array representing the CSS file to attach to the view.
    $attached = array(
      'css' => array(drupal_get_path('module', 'cura_admissions') . '/theme/cura_admissions.theme.css'),
    );

    // Add the default fields inherent to the task entity.
    $content['registration_id'] = array(
      '#markup' => 'Registration ID: ' . $registration->registration_id,
      '#attached' => $attached,
    );
    $content['label'] = array(
      '#markup' => 'Label: ' . $registration->label,
      '#attached' => $attached,
    );
    $content['status'] = array(
      '#markup' => 'Status: ' . $registration->status,
      '#attached' => $attached,
    );

    return parent::buildContent($registration, $view_mode, $langcode, $content);
  }
}

