<?php

/**
 * @file
 * Cron functions for the Admissions module of the Cura Childcare Suite.
 */

/**
 * Cron scheduler.
 */
function cura_admissions_cron_scheduler() {

  // Send starter emails.
  cura_admissions_send_starter_emails();

  // Run these on the first day of the month.
  if (date('j', time()) == 1) {
    cura_admissions_send_waiting_list_emails();
    watchdog('cura_admissions', 'Cron ran on the first day of the month.');
  }

  return TRUE;
}

/**
 * Sends starter emails.
 */
function cura_admissions_send_starter_emails() {

  // Get the relevant settings.
  $settings = cura_get_settings('general');
  $settings['email'] = cura_get_email_settings('starter');

  $future_start = date('Y-m-d', strtotime('+' . $settings['email']['days'] . ' days'));

  // Get starters for date.
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'child')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_child_status', 'value', array('L', 'C'), 'NOT IN')
    ->fieldCondition('field_child_start_date', 'value', $future_start)
    ->fieldOrderBy('cura_lastname', 'value', 'ASC')
    ->fieldOrderBy('cura_firstname', 'value', 'ASC')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (isset($result['node'])) {
    $children = node_load_multiple(array_keys($result['node']));
    foreach ($children as $child) {
      cura_admissions_send_starter_email($child, $settings);
    }
  }
  else {
    watchdog('cura_admissions', 'No starters on ' . date('D, j M Y', strtotime($future_start)) . '.');
  }
}

/**
 * Sends an email to the account owner of a starter.
 *
 * @param object $child
 *   The child starting.
 * @param array $settings
 *   General and specific email settings.
 */
function cura_admissions_send_starter_email($child, $settings = NULL) {

  $module = 'cura_admissions';
  $key = 'starter';
  $language = language_default();
  $send = TRUE;

  // Get the relevant settings.
  if (empty($settings)) {
    $settings = cura_get_settings('general');
    $settings['email'] = cura_get_email_settings($key);
  }

  // Check that email notifications have been enabled.
  if (!$settings['general']['notification']['email']['enabled']) {
    watchdog($module, 'Email notifications are not enabled!');
    return;
  }
  if (!$settings['email']['enabled']) {
    watchdog($module, 'Starter email notifications are not enabled!');
    return;
  }

  $cw = entity_metadata_wrapper('node', $child);
  $child_name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
  $aw = entity_metadata_wrapper('node', $cw->field_account_id->value());
  $aow = entity_metadata_wrapper('node', $aw->field_account_owner->value());
  $account_owner_name = $aow->field_firstname->value() . ' ' . $aow->field_lastname->value();
  $admin_email = 'Administrator <' . variable_get('site_mail', ini_get('sendmail_from')) . '>';
  $from = $settings['email']['from'];
  $to = $account_owner_name . ' <' . $aow->field_email->value() . '>';
//  $to = 'Administrator <' . variable_get('site_mail', ini_get('sendmail_from')) . '>'; // ** TESTING **
  $cc = cura_contact_get_contact_email_addresses($cw->getIdentifier(), 'Parent');
  unset($cc[$aow->field_email->value()]);
  if (!empty($settings['email']['cc'])) {
    $cc[] = $settings['email']['cc'];
  }
  if (!empty($cc)) $params['headers']['cc'] = implode(', ', $cc);
  $params['subject'] = filter_xss($child_name . ' starting soon!');
  $params['body'][] = token_replace($settings['email']['body'], array(
    'person' => $aow->value(), 'account' => $aw->value(), 'child' => $cw->value()), array());

  // Send the email.
  $result = cura_send_email($module, $key, $to, $language, $params, $from, $send);
  if ($result['result'] == TRUE) {
    watchdog($module, 'Sent starter email to @to for @cn.', array(
      '@to' => $to, '@cn' => $child_name));
  }
  else {
    watchdog($module, 'Could not send starter email to @to for @cn.', array(
      '@to' => $to, '@cn' => $child_name), WATCHDOG_ERROR);
  }
}

/**
 * Sends email notifications to people on the waiting list.
 */
function cura_admissions_send_waiting_list_emails() {

  // Get the relevant settings.
  $settings = cura_get_settings('general');
  $settings['email'] = cura_get_email_settings('waiting_list');

  // Get the children on the waiting list.
  $child_nids = cura_get_children('nids', array('W'));
  $children = entity_load('node', $child_nids);

  // Send waiting list email to each child.
  $count = 0;
  foreach ($children as $child) {
    if (cura_admissions_send_waiting_list_email($child, $settings)) {
      $count++;
    }
  }
  watchdog('cura_admissions', 'Sent %c email notifications to waiting list.', array('%c' => $count));
}

/**
 * Sends a waiting list email to the account owner.
 *
 * @param object $child
 *   The child on the waiting list.
 * @param array $settings
 *   General and specific email settings.
 */
function cura_admissions_send_waiting_list_email($child, $settings = NULL) {

  $module = 'cura_admissions';
  $key = 'waiting_list';
  $language = language_default();
  $send = TRUE;

  // Get the relevant settings.
  if (empty($settings)) {
    $settings = cura_get_settings('general');
    $settings['email'] = cura_get_email_settings($key);
  }

  // Check that email notifications have been enabled.
  if (!$settings['general']['notification']['email']['enabled']) {
    watchdog($module, 'Email notifications are not enabled!');
    return;
  }
  if (!$settings['email']['enabled']) {
    watchdog($module, 'Waiting list email notifications are not enabled!');
    return;
  }

  $cw = entity_metadata_wrapper('node', $child);
  $child_name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
  $aw = entity_metadata_wrapper('node', $cw->field_account_id->value());
  $aow = entity_metadata_wrapper('node', $aw->field_account_owner->value());
  $account_owner_name = $aow->field_firstname->value() . ' ' . $aow->field_lastname->value();
  $admin_email = 'Administrator <' . variable_get('site_mail', ini_get('sendmail_from')) . '>';
  $from = $settings['email']['from'];
  $to = $account_owner_name . ' <' . $aow->field_email->value() . '>';
//  $to = 'Administrator <' . variable_get('site_mail', ini_get('sendmail_from')) . '>'; // ** TESTING **
  if (!empty($settings['email']['cc'])) {
    $params['headers']['cc'] = $settings['email']['cc'];
  }
  $params['subject'] = filter_xss($settings['general']['org']['general']['org_name_short'] . ' waiting list - ' . $child_name);
  $params['body'][] = token_replace($settings['email']['body'] . $settings['general']['org']['general']['footer'], array(
    'person' => $aow->value(), 'account' => $aw->value(), 'child' => $cw->value()), array());

  // Send the email.
  $result = cura_send_email($module, $key, $to, $language, $params, $from, $send);
  if ($result['result'] == TRUE) {
    watchdog($module, 'Sent waiting list email to @to.', array('@to' => $to));
  }
  else {
    watchdog($module, 'Could not send waiting list email to @to.', array('@to' => $to), WATCHDOG_ERROR);
  }

  return $result['result'];
}

