<?php

/**
 * Views for the Admissions module of the Cura Childcare Suite.
 */

/**
 * Implements hook_views_default_views().
 */
function cura_admissions_views_default_views() {
  $views = array();

  // Define the offers view.
  $view = new view();
  $view->name = 'cura_admissions_offers';
  $view->description = 'Displays the offers made in the Cura Childcare Suite.';
  $view->tag = 'cura_admissions';
  $view->base_table = 'node';
  $view->human_name = 'Offers';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Offers';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'child' => 'child',
  );

  /* Display: Offers */
  $handler = $view->new_display('page', 'Offers', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Offers';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<h2>Pending offers</h2><a href="/admissions/offer/add" title="Add a new offer">Add a new offer</a>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No pending offers.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Firstname */
  $handler->display->display_options['fields']['cura_firstname']['id'] = 'cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['table'] = 'field_data_cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['field'] = 'cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['exclude'] = TRUE;
  $handler->display->display_options['fields']['cura_firstname']['element_label_colon'] = FALSE;
  /* Field: Content: Lastname */
  $handler->display->display_options['fields']['cura_lastname']['id'] = 'cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['table'] = 'field_data_cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['field'] = 'cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['label'] = 'Child';
  $handler->display->display_options['fields']['cura_lastname']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['cura_lastname']['alter']['text'] = '[cura_lastname], [cura_firstname]';
  $handler->display->display_options['fields']['cura_lastname']['element_label_colon'] = FALSE;
  /* Field: Content: Birthdate */
  $handler->display->display_options['fields']['field_birth_date']['id'] = 'field_birth_date';
  $handler->display->display_options['fields']['field_birth_date']['table'] = 'field_data_field_birth_date';
  $handler->display->display_options['fields']['field_birth_date']['field'] = 'field_birth_date';
  $handler->display->display_options['fields']['field_birth_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_birth_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '1',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'hide',
  );
  $handler->display->display_options['fields']['field_birth_date']['delta_offset'] = '0';
  /* Field: Content: Start date */
  $handler->display->display_options['fields']['field_child_start_date']['id'] = 'field_child_start_date';
  $handler->display->display_options['fields']['field_child_start_date']['table'] = 'field_data_field_child_start_date';
  $handler->display->display_options['fields']['field_child_start_date']['field'] = 'field_child_start_date';
  $handler->display->display_options['fields']['field_child_start_date']['label'] = 'Start';
  $handler->display->display_options['fields']['field_child_start_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_child_start_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Content: Offer date */
  $handler->display->display_options['fields']['field_child_offer_date']['id'] = 'field_child_offer_date';
  $handler->display->display_options['fields']['field_child_offer_date']['table'] = 'field_data_field_child_offer_date';
  $handler->display->display_options['fields']['field_child_offer_date']['field'] = 'field_child_offer_date';
  $handler->display->display_options['fields']['field_child_offer_date']['label'] = 'Offer';
  $handler->display->display_options['fields']['field_child_offer_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a class="btn btn-success" href="/admissions/offer/[nid]/accept">Accept</a> <a class="btn btn-danger" href="/admissions/offer/[nid]/delete">Delete</a>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Lastname (cura_lastname) */
  $handler->display->display_options['sorts']['cura_lastname_value']['id'] = 'cura_lastname_value';
  $handler->display->display_options['sorts']['cura_lastname_value']['table'] = 'field_data_cura_lastname';
  $handler->display->display_options['sorts']['cura_lastname_value']['field'] = 'cura_lastname_value';
  /* Sort criterion: Content: Firstname (cura_firstname) */
  $handler->display->display_options['sorts']['cura_firstname_value']['id'] = 'cura_firstname_value';
  $handler->display->display_options['sorts']['cura_firstname_value']['table'] = 'field_data_cura_firstname';
  $handler->display->display_options['sorts']['cura_firstname_value']['field'] = 'cura_firstname_value';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'child' => 'child',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Status (field_child_status) */
  $handler->display->display_options['filters']['field_child_status_value']['id'] = 'field_child_status_value';
  $handler->display->display_options['filters']['field_child_status_value']['table'] = 'field_data_field_child_status';
  $handler->display->display_options['filters']['field_child_status_value']['field'] = 'field_child_status_value';
  $handler->display->display_options['filters']['field_child_status_value']['operator'] = 'not';
  $handler->display->display_options['filters']['field_child_status_value']['value'] = array(
    'C' => 'C',
  );
  $handler->display->display_options['filters']['field_child_status_value']['group'] = 1;
  /* Filter criterion: Content: Offer date (field_child_offer_date) */
  $handler->display->display_options['filters']['field_child_offer_date_value']['id'] = 'field_child_offer_date_value';
  $handler->display->display_options['filters']['field_child_offer_date_value']['table'] = 'field_data_field_child_offer_date';
  $handler->display->display_options['filters']['field_child_offer_date_value']['field'] = 'field_child_offer_date_value';
  $handler->display->display_options['filters']['field_child_offer_date_value']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_child_offer_date_value']['group'] = 1;
  /* Filter criterion: Content: Accept date (field_child_accept_date) */
  $handler->display->display_options['filters']['field_child_accept_date_value']['id'] = 'field_child_accept_date_value';
  $handler->display->display_options['filters']['field_child_accept_date_value']['table'] = 'field_data_field_child_accept_date';
  $handler->display->display_options['filters']['field_child_accept_date_value']['field'] = 'field_child_accept_date_value';
  $handler->display->display_options['filters']['field_child_accept_date_value']['operator'] = 'empty';
  $handler->display->display_options['filters']['field_child_accept_date_value']['group'] = 1;
  $handler->display->display_options['path'] = 'admissions/offers';
  $handler->display->display_options['menu']['title'] = 'Offers';
  $handler->display->display_options['menu']['weight'] = '10';
  $handler->display->display_options['menu']['name'] = 'cura-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Offer history */
  $handler = $view->new_display('attachment', 'Offer history', 'attachment_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Accepted offers';
  $handler->display->display_options['defaults']['use_more'] = FALSE;
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['defaults']['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['defaults']['use_more_text'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'nid' => 'nid',
    'cura_firstname' => 'cura_firstname',
    'cura_lastname' => 'cura_lastname',
    'field_birth_date' => 'field_birth_date',
    'field_child_start_date' => 'field_child_start_date',
    'field_child_offer_date' => 'field_child_offer_date',
    'field_child_accept_date' => 'field_child_accept_date',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cura_firstname' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cura_lastname' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_birth_date' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_child_start_date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_child_offer_date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_child_accept_date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Accepted offers';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<h2>Accepted offers</h2>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = 'Child ID';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Firstname */
  $handler->display->display_options['fields']['cura_firstname']['id'] = 'cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['table'] = 'field_data_cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['field'] = 'cura_firstname';
  $handler->display->display_options['fields']['cura_firstname']['exclude'] = TRUE;
  $handler->display->display_options['fields']['cura_firstname']['element_label_colon'] = FALSE;
  /* Field: Content: Lastname */
  $handler->display->display_options['fields']['cura_lastname']['id'] = 'cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['table'] = 'field_data_cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['field'] = 'cura_lastname';
  $handler->display->display_options['fields']['cura_lastname']['label'] = 'Child';
  $handler->display->display_options['fields']['cura_lastname']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['cura_lastname']['alter']['text'] = '[cura_lastname], [cura_firstname]';
  $handler->display->display_options['fields']['cura_lastname']['element_label_colon'] = FALSE;
  /* Field: Content: Birthdate */
  $handler->display->display_options['fields']['field_birth_date']['id'] = 'field_birth_date';
  $handler->display->display_options['fields']['field_birth_date']['table'] = 'field_data_field_birth_date';
  $handler->display->display_options['fields']['field_birth_date']['field'] = 'field_birth_date';
  $handler->display->display_options['fields']['field_birth_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_birth_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '1',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'hide',
  );
  $handler->display->display_options['fields']['field_birth_date']['delta_offset'] = '0';
  /* Field: Content: Start date */
  $handler->display->display_options['fields']['field_child_start_date']['id'] = 'field_child_start_date';
  $handler->display->display_options['fields']['field_child_start_date']['table'] = 'field_data_field_child_start_date';
  $handler->display->display_options['fields']['field_child_start_date']['field'] = 'field_child_start_date';
  $handler->display->display_options['fields']['field_child_start_date']['label'] = 'Start';
  $handler->display->display_options['fields']['field_child_start_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_child_start_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Content: Offer date */
  $handler->display->display_options['fields']['field_child_offer_date']['id'] = 'field_child_offer_date';
  $handler->display->display_options['fields']['field_child_offer_date']['table'] = 'field_data_field_child_offer_date';
  $handler->display->display_options['fields']['field_child_offer_date']['field'] = 'field_child_offer_date';
  $handler->display->display_options['fields']['field_child_offer_date']['label'] = 'Offer';
  $handler->display->display_options['fields']['field_child_offer_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_child_offer_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Content: Accept date */
  $handler->display->display_options['fields']['field_child_accept_date']['id'] = 'field_child_accept_date';
  $handler->display->display_options['fields']['field_child_accept_date']['table'] = 'field_data_field_child_accept_date';
  $handler->display->display_options['fields']['field_child_accept_date']['field'] = 'field_child_accept_date';
  $handler->display->display_options['fields']['field_child_accept_date']['label'] = 'Accepted';
  $handler->display->display_options['fields']['field_child_accept_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_child_accept_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Accept date (field_child_accept_date) */
  $handler->display->display_options['sorts']['field_child_accept_date_value']['id'] = 'field_child_accept_date_value';
  $handler->display->display_options['sorts']['field_child_accept_date_value']['table'] = 'field_data_field_child_accept_date';
  $handler->display->display_options['sorts']['field_child_accept_date_value']['field'] = 'field_child_accept_date_value';
  $handler->display->display_options['sorts']['field_child_accept_date_value']['order'] = 'DESC';
  /* Sort criterion: Content: Offer date (field_child_offer_date) */
  $handler->display->display_options['sorts']['field_child_offer_date_value']['id'] = 'field_child_offer_date_value';
  $handler->display->display_options['sorts']['field_child_offer_date_value']['table'] = 'field_data_field_child_offer_date';
  $handler->display->display_options['sorts']['field_child_offer_date_value']['field'] = 'field_child_offer_date_value';
  $handler->display->display_options['sorts']['field_child_offer_date_value']['order'] = 'DESC';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'child' => 'child',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Offer date (field_child_offer_date) */
  $handler->display->display_options['filters']['field_child_offer_date_value']['id'] = 'field_child_offer_date_value';
  $handler->display->display_options['filters']['field_child_offer_date_value']['table'] = 'field_data_field_child_offer_date';
  $handler->display->display_options['filters']['field_child_offer_date_value']['field'] = 'field_child_offer_date_value';
  $handler->display->display_options['filters']['field_child_offer_date_value']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_child_offer_date_value']['group'] = 1;
  /* Filter criterion: Content: Accept date (field_child_accept_date) */
  $handler->display->display_options['filters']['field_child_accept_date_value']['id'] = 'field_child_accept_date_value';
  $handler->display->display_options['filters']['field_child_accept_date_value']['table'] = 'field_data_field_child_accept_date';
  $handler->display->display_options['filters']['field_child_accept_date_value']['field'] = 'field_child_accept_date_value';
  $handler->display->display_options['filters']['field_child_accept_date_value']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_child_accept_date_value']['group'] = 1;
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $handler->display->display_options['attachment_position'] = 'after';

  $views[$view->name] = $view;

  return $views;
}

