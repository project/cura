<?php

/**
 * @file
 * Forms for the Admissions module of the Cura Childcare Suite. For administration and settings forms
 * see the cura_admissions.admin.inc file.
 */

module_load_include('inc', 'cura', 'cura.sessions');

/******************************************************************************
 *                                                                            *
 * Admissions offers.                                                         *
 *                                                                            *
 ******************************************************************************/

/**
 * Form for creating and updating offers.
 *
 * @param object $child
 *   The child object for which the offer is being made.
 * @param string $op
 *   The operation being performed.
 */
function cura_admissions_offer_form($form, &$form_state, $child, $op) {

  // Get the relevant settings.
  $settings = cura_get_settings('general');
  $email_settings = cura_get_email_settings('offer');

  // Get the child object from a form refresh.
  if (isset($form_state['cura_child'])) {
    $child = $form_state['cura_child'];
  }

  // Check that a valid child object has been passed to this form.
  if ($child) {
    if (is_object($child) && isset($child->type) && ($child->type == 'child')) {
      $cw = entity_metadata_wrapper('node', $child);
      $child_name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
    }
    else {
      drupal_set_message(t('Invalid child object sent to form!'));
      drupal_goto('');
    }
  }

  switch ($op) {

    // New offer.
    case 'add':
      if (empty($child)) {
        // No child selected.
        $form['notice'] = array(
          '#markup' => '<p>' . t('The following actions will be taken when submitting this offer') . ':</p>'
            . '<ul><li>Mail - An offer will be sent to the parent/carer via email.'
            . '<li>Offer date - The date of the offer will be recorded against the child record.'
            . '<li>Notification - A system notification will be recorded for the parent/carer.</ul>'
            . '<h3>Notes</h3><ul><li>The deposit will only be raised when the offer is accepted.'
            . '<li>The offer will be based on the current session plan. If the start date is incorrect,'
            . 'or the session plan detail needs to be changed the session plan must be updated accordingly.</ul>'
        );
      }
      $options = cura_get_children('select', array('N', 'W'));
      $form['#tree'] = TRUE;
      $form['child'] = array(
        '#type' => 'fieldset',
        '#title' => t('Child'),
      );
      $form['child']['child_id'] = array(
        '#title' => t('Child'),
        '#type' => 'select',
        '#options' => $options,
        '#attributes' => array('onChange' => 'document.getElementById("cura-admissions-offer-form").submit();'),
        '#empty_value' => 0,
        '#default_value' => isset($form_state['values']['child']['child_id']) ? $form_state['values']['child']['child_id'] : NULL,
      );
      $form['actions'] = array('#type' => 'actions');
      $form['actions']['refresh'] = array(
        '#type' => 'submit',
        '#value' => t('Refresh'),
      );
      if ($child) {
        $child_start_date = $cw->field_child_start_date->value();
        $aw = entity_metadata_wrapper('node', $cw->field_account_id->value());
        $aow = entity_metadata_wrapper('node', $aw->field_account_owner->value());
        $account_owner_name = $aow->field_firstname->value() . ' ' . $aow->field_lastname->value();
        $user = $aow->field_user_id->value();
        $to = $account_owner_name . ' <' . $aow->field_email->value() . '>';
        $body = token_replace($email_settings['body'], array(
          'person' => $aow->value(), 'account' => $aw->value(), 'child' => $cw->value()), array());
        $markup = cura_display_session_plan($cw->value(), $child_start_date);
        $form['child']['attendance_plan'] = array(
          '#markup' => render($markup),
        );
        $form['email'] = array(
          '#type' => 'fieldset',
          '#title' => t('Offer email'),
        );
        $form['email']['from'] = array(
          '#type' => 'textfield',
          '#title' => t('From'),
          '#default_value' => $email_settings['from'],
          '#maxlength' => 320,
          '#disabled' => TRUE,
        );
        $form['email']['to'] = array(
          '#type' => 'textarea',
          '#title' => t('To'),
          '#rows' => 3,
          '#default_value' => $to,
          '#required' => TRUE,
        );
        $form['email']['headers']['cc'] = array(
          '#type' => 'textarea',
          '#title' => t('Cc'),
          '#rows' => 3,
          '#default_value' => !empty($email_settings['cc']) ? $email_settings['cc'] : NULL,
          '#disabled' => TRUE,
        );
        $form['email']['subject'] = array(
          '#type' => 'textfield',
          '#title' => t('Subject'),
          '#default_value' => 'Offer for ' . $child_name,
        );
        $form['email']['body'] = array(
          '#type' => 'text_format',
          '#format' => 'full_html',
          '#rows' => 15,
          '#default_value' => $body,
        );
        $form['date'] = array(
          '#type' => 'fieldset',
          '#title' => t('Offer date'),
        );
        $form['date']['offer_date'] = array(
          '#title' => t('Offer date'),
          '#type' => 'date_popup',
          '#description' => t('If a date is specified it is assumed that an offer has already been made and'
        		    . ' no email will be sent.'),
          '#date_format' => 'Y-m-d H:i:s',
          '#date_label_position' => 'within',
          '#prefix' => 'Only specify a date if the offer has already been made to the parent. If this field is'
               . ' blank the offer date will default to the current date and time and an offer email will'
               . ' be sent to the parent.',
        );
        $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Make offer'),
        );
      }
      break;

    // Accept pending offer.
    case 'accept':
      $form['#tree'] = TRUE;
      $form['child']['child_id'] = array(
        '#type' => 'value',
        '#value' => $cw->getIdentifier(),
      );
      $form['child']['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Child'),
        '#default_value' => $child_name,
        '#disabled' => TRUE,
      );
      $form['accept_date'] = array(
	'#title' => t('Accept date'),
	'#type' => 'date_popup',
	'#date_format' => 'Y-m-d H:i:s',
	'#date_label_position' => 'within',
	'#required' => TRUE,
	'#prefix' => 'This is the date when the parent confirmed acceptance of the offer of a place at the'
		     . ' setting. It is normally taken from the confirmation email sent by the parent.',
      );
      $form['actions'] = array('#type' => 'actions');
      $form['actions']['submit'] = array(
	'#type' => 'submit',
	'#value' => t('Accept offer'),
      );
      break;

    // Delete pending offer.
    case 'delete':
      $form['#tree'] = TRUE;
      $form['child']['child_id'] = array(
        '#type' => 'value',
        '#value' => $cw->getIdentifier(),
      );
      $form['nid'] = array(
	'#type' => 'value',
	'#value' => $cw->getIdentifier(),
      );
      $form = confirm_form($form, t('Are you sure you want to delete the offer made to %c?',
	      array('%c' => $cw->title->value())), 'admissions/offers', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
      break;
  }


  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Submission handler for the form to create/update offers.
 */
function cura_admissions_offer_form_submit($form, &$form_state) {
//  drupal_set_message('Values:op:<pre>' . print_r($form_state['values']['op'], TRUE) . '</pre>');
  if (!empty($form_state['values']['child']['child_id'])) {
    $child = node_load($form_state['values']['child']['child_id']);
    $cw = entity_metadata_wrapper('node', $child);
    $form_state['cura_child'] = $cw->value();
    $child_name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
    $birth_date = $cw->field_birth_date[0]->value();
    $pw = entity_metadata_wrapper('node', $cw->field_account_id->field_account_owner->raw());
    $user = $pw->field_user_id->value();
//    drupal_set_message('Account:<pre>' . print_r($user, TRUE) . '</pre>');
  }
  switch ($form_state['values']['op']) {
    case 'Refresh':
      unset($form_state['input']['email']);
      unset($form_state['input']['date']);
      $form_state['rebuild'] = TRUE;
      break;

    case 'Make offer':
      // An offer date has been specified.
      if (isset($form_state['values']['date']['offer_date'])) {
        $offer_date = strtotime($form_state['values']['date']['offer_date']);
      }
      else {
        $offer_date = REQUEST_TIME;
        $params = $form_state['values']['email'];
        $hmac = drupal_hmac_base64($birth_date . $offer_date . $user->uid, drupal_get_hash_salt() . $user->pass);
        $params['body'] = strtr($params['body']['value'], array(
          '[offer:url]' => $GLOBALS['base_url'] . '/admissions/offer/link/' . $child->nid . '/' . $hmac,
        ));
        cura_admissions_send_offer_email($params);
      }

// Commented for testing purposes.
      $child->field_child_offer_date['und'][0]['value'] = date('Y-m-d H:i:s', $offer_date);
      node_save($child);
//
//      // Offer notification.
//      $notification = new stdClass();
//      $notification->label = 'Offer for ' . $child_name;
//      $notification->type = 'Offer';
//      $notification->notes = 'Place for ' . $child_name . ' offered ' . date('H:i l, j F Y', $offer_date) . '.';
//      $notification->date = $offer_date;
//      $notification->child_id = $cw->getIdentifier();
//      $notification->uid = $user->uid;
//      cura_notification_save($notification);

      $form_state['redirect'] = 'admissions/offers';
      break;

    case 'Accept offer':
//      drupal_set_message('Values:op:<pre>' . print_r($form_state['values']['op'], TRUE) . '</pre>');
      $offer_date = $cw->field_child_offer_date->value();
      $accept_date = isset($form_state['values']['accept_date']) ? strtotime($form_state['values']['accept_date']) : null;
      // Check that the accept date is valid.
      if (empty($accept_date) || $accept_date <= $offer_date || $accept_date > REQUEST_TIME + 3600) {
        drupal_set_message('Invalid date!');
        $form_state['rebuild'] = TRUE;
        break;
      }

      $child->field_child_accept_date['und'][0]['value'] = date('Y-m-d H:i:s', $accept_date);
      node_save($child);

      // Create new deposit order if the billing module is implemented.
      if (module_exists('cura_billing')) {
        $products[variable_get('cura_dep_product_id', '0')] = 1;
        $term = cura_get_term_by_name('Deposit', 'order_type');
        $order_ref = $child_name;
        if ($order = cura_billing_commerce_order_new($pw->getIdentifier(), $products, 'checkout_review', 'commerce_order',
                                                         $term->tid, $order_ref)) {
          watchdog('cura', 'Created deposit order for @title @fn @ln (@nid), user @uid.',
            array('@title' => $pw->field_person_title->value(), '@fn' => $pw->field_firstname->value(),
                  '@ln' => $pw->field_lastname->value(), '@nid' => $pw->getIdentifier(), '@uid' => $user->uid));
        }
      }
      else {
        watchdog('cura', 'Enable the Cura Billing module to create deposit orders.');
      }

      // Accept notification.
      $notification = new stdClass();
      $notification->label = 'Offer accepted for ' . $child_name;
      $notification->type = 'Offer accepted';
      $notification->notes = 'Place for ' . $child_name . ' accepted ' . date('H:i l, j F Y', $accept_date) . '.';
      $notification->date = $accept_date;
      $notification->child_id = $cw->getIdentifier();
      $notification->uid = $user->uid;
      cura_notification_save($notification);

      $form_state['redirect'] = 'admissions/offers';
      break;

    case 'Delete':
      if (isset($child->field_child_offer_date['und'][0]['value']) &&
          empty($child->field_child_accept_date['und'][0]['value'])) {
        $child->field_child_offer_date['und'][0]['value'] = NULL;
        node_save($child);
        drupal_set_message(t('Deleted offer to @child.', array('@child' => $child->title)));
      }
      else {
        drupal_set_message(t('It is not possible to delete the offer to @child.', array('@child' => $child->title)));
      }
      $form_state['redirect'] = 'admissions/offers';
      break;

    case 'Cancel':
      $url = isset($_GET['destination']) ? $_GET['destination'] : '/admissions/offers';
      drupal_goto($url);
      break;
  }
}

/**
 * Form callback: accept offer.
 *
 * @param $child int
 *   The ID of the child for whom the offer was made.
 * @param $hmac string
 *   The base-64 encoded hash-based message authentication code (HMAC).
 */
function cura_admissions_offer_accept_form($form, &$form_state, $child, $hmac) {
  if ($child && ($cw = entity_metadata_wrapper('node', $child)) && ($cw->type->value() === 'child')) {
    $child_name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
    drupal_set_title(t('Offer for @c', array('@c' => $child_name)), PASS_THROUGH);
    $birth_date = $cw->field_birth_date[0]->value();
    $offer_date = $cw->field_child_offer_date->value();
    $aow = entity_metadata_wrapper('node', $cw->field_account_id->field_account_owner->raw());
    $user = $aow->field_user_id->value();
    $hmac2 = drupal_hmac_base64($birth_date . $offer_date . $user->uid, drupal_get_hash_salt() . $user->pass);
    if ($hmac === $hmac2) {
    drupal_set_message('HMAC confirmed!');
    $days = cura_date_diff($offer_date, REQUEST_TIME);
    drupal_set_message($days . ($days > 1 ? ' days' : ' day') . ' old.');
    $form['child_id'] = array(
      '#type' => 'hidden',
      '#value' => $cw->getIdentifier(),
    );
    $form['birthdate'] = array(
      '#type' => 'textfield',
      '#title' => t('Child'),
      '#maxlength' => 60,
      '#default_value' => format_date($cw->field_birth_date[0]->value(), 'custom', 'D, j M Y'),
    );
    $form['start_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Start date'),
      '#default_value' => format_date($cw->field_child_start_date->value(), 'custom', 'D, j M Y'),
    );
    }
    else {
      drupal_set_message('HMAC not authenticated.');
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Accept offer'));
//  drupal_set_message('<pre>' . print_r($cw->type, TRUE) . '</pre>');

  return $form;
}

/**
 * Submit callback for cura_admissions_offer_accept_form().
 */
function cura_admissions_offer_accept_form_submit($form, &$form_state) {
  drupal_set_message('<pre>' . print_r($form_state['values'], TRUE) . '</pre>');
}

/******************************************************************************
 *                                                                            *
 * Admissions attendance plans.                                               *
 *                                                                            *
 ******************************************************************************/

/**
 * Subform to handle session plans.
 *
 *
 */
function cura_admissions_attendance_plan_subform($attendance_plan = NULL) {
  $settings = cura_get_settings('general');
  module_load_include('inc', 'cura', 'cura.sessions');
  $session_types = cura_get_session_types();
  $subform = array(
    '#tree' => TRUE,
    'session_types' => array(
      '#value' => $session_types,
    ),
  );
  foreach ($session_types as $session_type) {
    $subform['sessions'][$session_type->label]['label'] = array(
      '#value' => $session_type->label,
    );
    foreach (array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun') as $day) {
      $subform['sessions'][$session_type->label]['days'][$day] = array(
        '#type' => 'checkbox',
        '#default_value' => isset($attendance_plan['sessions'][$session_type->session_type_id]['days'][$day]) ? $attendance_plan['sessions'][$session_type->session_type_id]['days'][$day] : 0,
        '#disabled' => !$settings['general']['calendar']['days'][$day]['enabled'],
      );
    }
  }

  return $subform;
}

/**
 * Form callback: create, edit or delete an attendance plan.
 *
 * @param $attendance_plan
 *   The attendance plan object to edit through the form.
 */
function cura_admissions_attendance_plan_form($form, &$form_state, $attendance_plan = NULL) {
  $form['child'] = array(
    '#type' => 'textfield',
    '#title' => t('Child'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'child/autocomplete',
  );
  $form['attendance_plan'] = cura_admissions_attendance_plan_subform();
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save plan', array(), array('context' => 'Child attendance plan')),
    '#weight' => 40,
  );

//  drupal_set_message('<pre>' . print_r($form, TRUE) . '</pre>');
  return $form;
}

/**
 * Validation callback for cura_admissions_attendance_plan_form().
 */
function cura_admissions_attendance_plan_form_validate($form, &$form_state) {

}

/**
 * Submit callback for cura_admissions_attendance_plan_form().
 */
function cura_admissions_attendance_plan_form_submit($form, &$form_state) {
  drupal_set_message('<pre>' . print_r($form_state['values'], TRUE) . '</pre>');
}

