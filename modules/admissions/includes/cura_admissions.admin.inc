<?php

/**
 * @file
 * Admin UI for the Admissions module of the Cura Childcare Suite.
 */

/**
 * Form for managing settings.
 *
 * @see cura_menu()
 * @see cura_settings_form()
 * @ingroup forms
 */
function cura_admissions_settings_form($form, &$form_state) {
  $settings = cura_get_settings();

  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['settings'] = array(
    '#tree' => TRUE,
  );

  // General settings.
  $form['settings']['general']['admissions'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#weight' => -50,
    '#access' => user_access('administer cura_registration entities'),
  );
  $form['settings']['general']['admissions']['general']['tbd'] = array(
    '#type' => 'textfield',
    '#title' => t('To be decided'),
    '#default_value' => $settings['general']['admissions']['general']['tbd'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['admissions']['general']['help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help on registering.'),
    '#default_value' => $settings['general']['admissions']['general']['help'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['admissions']['notification']['email']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable admissions email notifications'),
    '#default_value' => $settings['general']['admissions']['notification']['email']['enabled'],
    '#access' => user_access('administer cura settings'),
  );

  // TODO settings.
  $form['settings']['general']['admissions']['fieldset_two'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fieldset Two'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#weight' => -40,
    '#access' => user_access('administer cura settings'),
  );

  $form['settings']['general']['admissions']['fieldset_two']['settings_one'] = array(
    '#type' => 'checkbox',
    '#title' => t('To be done'),
    '#description' => t('This is a stub for future admissions settings.'),
    '#default_value' => 1,
    '#access' => user_access('administer cura settings'),
  );

  // Email notification settings.
  $form['settings']['notification']['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Emails'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#tree' => TRUE,
    '#weight' => -30,
    '#access' => user_access('administer cura settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[general][notification][email][enabled]"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Waiting list email.
  $form['settings']['notification']['email']['waiting_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Waiting list email notifications'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer cura settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[general][admissions][notification][email][enabled]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['notification']['email']['waiting_list']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable waiting list email notifications'),
    '#default_value' => $settings['notification']['email']['waiting_list']['enabled'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['waiting_list']['from'] = array(
    '#type' => 'textfield',
    '#title' => t('The email address sending the email notification'),
    '#default_value' => $settings['notification']['email']['waiting_list']['from'],
    '#maxlength' => 320,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['waiting_list']['cc'] = array(
    '#type' => 'textarea',
    '#title' => t('The email addresses copied in on the email notification'),
    '#default_value' => $settings['notification']['email']['waiting_list']['cc'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['waiting_list']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('The email body template'),
    '#default_value' => $settings['notification']['email']['waiting_list']['body'],
    '#access' => user_access('administer cura settings'),
  );

  // Starters and leavers email.
  $form['settings']['admissions']['notification']['starters_leavers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Starters and leavers email'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer cura settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[general][admissions][notification][email][enabled]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['admissions']['notification']['starters_leavers']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable starters and leavers emails'),
    '#default_value' => $settings['notification']['starters_leavers']['enabled'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['admissions']['notification']['starters_leavers']['from'] = array(
    '#type' => 'textfield',
    '#title' => t('The email address sending the starters and leavers email'),
    '#default_value' => $settings['notification']['starters_leavers']['from'],
    '#maxlength' => 320,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['admissions']['notification']['starters_leavers']['to'] = array(
    '#type' => 'textarea',
    '#title' => t('The email addresses to which the email is sent. Separate with commas.'),
    '#default_value' => $settings['notification']['starters_leavers']['to'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['admissions']['notification']['starters_leavers']['cc'] = array(
    '#type' => 'textarea',
    '#title' => t('The email address copied in on the email.'),
    '#default_value' => $settings['notification']['starters_leavers']['cc'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['admissions']['notification']['starters_leavers']['count'] = array(
    '#type' => 'select',
    '#title' => t('The amount of past records to display.'),
    '#options' => array('30' => '30', '60' => '60', '120' => '120', '240' => '240'),
    '#default_value' => $settings['notification']['starters_leavers']['count'],
    '#access' => user_access('administer cura settings'),
  );

  return cura_settings_form($form);
}

/** 
 * Form for managing registration settings.
 *
 * @see cura_menu()
 * @see cura_settings_form()
 * @ingroup forms
 */
function cura_admissions_registration_settings_form($form, &$form_state) {
  $settings = cura_get_settings();
    
  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['settings'] = array(
    '#tree' => TRUE,
  );
  
  // General settings.
  $form['settings']['general']['admissions'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#weight' => -50,
    '#access' => user_access('administer cura_registration entities'),
  );

  return cura_settings_form($form);
}

/**
 * Form for managing offer settings.
 *
 * @see cura_menu()
 * @see cura_settings_form()
 * @ingroup forms
 */
function cura_admissions_offer_settings_form($form, &$form_state) {
  $settings = cura_get_settings();
   
  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['settings'] = array(
    '#tree' => TRUE,
  );
 
  // General settings.
  $form['settings']['general']['admissions'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#weight' => -50,
    '#access' => user_access('administer cura_registration entities'),
  );

  return cura_settings_form($form);
}

