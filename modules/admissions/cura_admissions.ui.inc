<?php

/**
 * @file cura_admissions.ui.inc
 * User interface functions for the Admissions module of the Cura Childcare Suite.
 */

/**
 * Form for creating a new registration.
 */
function cura_registration_add_form($form, &$form_state) {
  $notice = '<div class="jumbotron">';
  $notice .= '<h1>Registration</h1>';
  $notice .= '<p>Please complete this form to register your child with ' . variable_get('cura_org_name', 'us') . '. Once the form is submitted you will not be able to change any details online so please take care when entering the information. If you are unsure about any of the areas on the form please contact our admissions co-ordinator or supervisor for assistance. Once you have completed the registration form you will need to do the following:</p>';
  $notice .= '<p>1. Pay the ' . cura_product_price_get(variable_get('cura_reg_product_id', '0')) . ' <a href="/fees" title="Polstead Preschool fees">registration fee</a>. This can be done by bank transfer or by posting or dropping off a cheque.</p>';
  $notice .= '<p>2. Present identity documents to the supervisor. We need to confirm the identity and current address of the primary parent/carer, that is, the person with which the child normally resides (current passport or UK photocard driving license AND latest bank statement or utility bill) and the child, or children if more than one child is being registered (current passport or birth certificate).</p>';
  $notice .= '<p><em>IMPORTANT!</em> The person registering a child must have parental responsibility as defined in the <a href="http://www.legislation.gov.uk/ukpga/1989/41/contents" title="UK law relating to children" target="_blank">Children Act 1989</a>.</p>';
  $notice .= '</div>';
  $form['notice'] = array(
    '#markup' => $notice,
  );

  // Form actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Create new registration'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : '<front>',
  );

  return $form;
}

/**
 * Submit handler for the cura_registration_add_form.
 */
function cura_registration_add_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] === 'Create new registration') {
    $registration = cura_registration_new();
    cura_registration_save($registration);
    watchdog('cura_admissions', 'Created registration @i.', array('@i' => $registration->registration_id), WATCHDOG_NOTICE);
    $form_state['redirect'] = 'admissions/registration/' . $registration->registration_id . '/edit';
  }
}

/**
 * Form callback wrapper: create or edit a registration.
 *
 * @param object $registration
 *   The registration object to edit through the form.
 * @param object $account
 *   The user account object.
 */
function cura_registration_form_wrapper($registration = NULL, $account = NULL) {
  if (empty($registration)) {
    $registration = cura_registration_new();
    cura_registration_save($registration);
  }

  // Set the page title and a default customer if necessary.
  if (empty($registration->registration_id)) {
    drupal_set_title(t('Create a registration'));

    if (!empty($account)) {
      $registration->uid = $account->uid;
    }
  }

  return drupal_get_form('cura_registration_form', $registration);
}

/**
 * Form for editing a registration.
 */
function cura_registration_form($form, &$form_state, $registration) {

  global $user;
  $default_email = $user->uid ? $user->mail : '';

  // Get previously submitted form data.
  $data = $registration->data;
//  if (isset($form_state['storage']['values']['data'])) $data = $form_state['storage']['values']['data'];

  $form_state['cura_registration'] = $registration;

  drupal_set_message('<div><h2>Registration data</h2><pre>' . print_r($registration->data, TRUE) . '</pre></div>');

  // Determine which page is the starting point.
  if (!isset($form_state['storage']['step'])) {
    $form_state['storage']['step'] = 1;
  }

  // Display the registration ID in the page title if there is one.
  if (!empty($registration->registration_id)) {
    drupal_set_title(t('Registration @id - Page @p of 5', array(
      '@id' => $registration->registration_id,
      '@p' => $form_state['storage']['step'],
    )));
  }

//  drupal_set_message('Step: ' . $form_state['storage']['step']);

  switch ($form_state['storage']['step']) {

    case 1:
      $form['data']['#tree'] = TRUE;
      $form['data']['child']['firstname'] = array(
        '#type' => 'textfield',
        '#title' => t('Firstname'),
        '#size' => 20,
        '#maxlength' => 40,
        '#default_value' => isset($data['child']['firstname']) ? $data['child']['firstname'] : '',
        '#required' => TRUE,
      );
      $form['data']['child']['lastname'] = array(
        '#type' => 'textfield',
        '#title' => t('Lastname'),
        '#size' => 20,
        '#maxlength' => 80,
        '#default_value' => isset($data['child']['lastname']) ? $data['child']['lastname'] : '',
        '#required' => TRUE,
      );
      $form['data']['child']['birthdate'] = array(
        '#type' => 'date_popup',
        '#title' => t('Date of birth'),
        '#date_format' => 'Y-m-d',
        '#date_label_position' => 'within',
        '#default_value' => isset($data['child']['birthdate']) ? $data['child']['birthdate'] : '',
        '#required' => TRUE,
      );
      $form['data']['child']['gender'] = array(
        '#type' => 'select',
        '#title' => t('Gender'),
        '#options' => array('M' => t('Male'), 'F' => t('Female')),
        '#default_value' => isset($data['child']['gender']) ? $data['child']['gender'] : NULL,
        '#required' => TRUE,
      );
      $info = field_info_field('cura_ethnicity');
//      echo 'Field Info:<br><pre>' . print_r($info, TRUE) . '</pre>';
      $form['data']['child']['ethnicity'] = array(
        '#type' => 'select',
        '#title' => t('Ethnicity'),
        '#options' => $info['settings']['allowed_values'],
        '#default_value' => isset($data['child']['ethnicity']) ? $data['child']['ethnicity'] : NULL,
        '#required' => TRUE,
      );
      $form['data']['child']['language'] = array(
        '#type' => 'select',
        '#title' => t('First language'),
        '#options' => cura_language_list(),
        '#default_value' => isset($data['child']['language']) ? $data['child']['language'] : NULL,
        '#required' => TRUE,
      );
      $form['data']['child']['dietary'] = array(
        '#type' => 'textarea',
        '#title' => t('Dietary requirements'),
        '#rows' => 2,
        '#default_value' => isset($data['child']['dietary']) ? $data['child']['dietary'] : '',
      );
      $form['data']['child']['sen'] = array(
        '#type' => 'textarea',
        '#title' => t('Special educational needs'),
        '#rows' => 2,
        '#default_value' => isset($data['child']['sen']) ? $data['child']['sen'] : '',
      );
      $form['data']['address']['line1'] = array(
        '#type' => 'textfield',
        '#title' => t('Address 1'),
        '#size' => 40,
        '#maxlength' => 80,
        '#default_value' => isset($data['address']['line1']) ? $data['address']['line1'] : '',
        '#required' => TRUE,
      );
      $form['data']['address']['line2'] = array(
        '#type' => 'textfield',
        '#title' => t('Address 2'),
        '#size' => 40,
        '#maxlength' => 80,
        '#default_value' => isset($data['address']['line2']) ? $data['address']['line2'] : '',
      );
      $form['data']['address']['city'] = array(
        '#type' => 'textfield',
        '#title' => t('Town/City'),
        '#size' => 30,
        '#maxlength' => 40,
        '#default_value' => isset($data['address']['city']) ? $data['address']['city'] : '',
        '#required' => TRUE,
      );
      $form['data']['address']['county'] = array(
        '#type' => 'textfield',
        '#title' => t('County'),
        '#size' => 40,
        '#maxlength' => 80,
        '#default_value' => isset($data['address']['county']) ? $data['address']['county'] : '',
      );
      $form['data']['address']['postcode'] = array(
        '#type' => 'textfield',
        '#title' => t('Postcode'),
        '#size' => 15,
        '#maxlength' => 35,
        '#default_value' => isset($data['address']['postcode']) ? $data['address']['postcode'] : '',
        '#required' => TRUE,
      );
      $form['data']['address']['country'] = array(
        '#type' => 'select',
        '#title' => t('Country'),
        '#options' => country_get_list(),
        '#default_value' => isset($data['address']['country']) ? $data['address']['country'] : variable_get('site_default_country', 'GB'),
        '#required' => TRUE,
      );
      break;

    case 2:
      $form['data']['#tree'] = TRUE;
      $form['data']['parent1']['title'] = array(
        '#type' => 'select',
        '#title' => t('Title'),
        '#options' => array('Mr' => t('Mr'), 'Mrs' => t('Mrs'), 'Ms' => t('Ms')),
        '#default_value' => isset($data['parent1']['title']) ? $data['parent1']['title'] : 'Ms',
      );
      $form['data']['parent1']['firstname'] = array(
        '#type' => 'textfield',
        '#title' => t('Firstname'),
        '#size' => 20,
        '#maxlength' => 40,
        '#default_value' => isset($data['parent1']['firstname']) ? $data['parent1']['firstname'] : '',
        '#required' => TRUE,
      );
      $form['data']['parent1']['lastname'] = array(
        '#type' => 'textfield',
        '#title' => t('Lastname'),
        '#size' => 20,
        '#maxlength' => 40,
        '#default_value' => isset($data['parent1']['lastname']) ? $data['parent1']['lastname'] : '',
        '#required' => TRUE,
      );
      $form['data']['parent1']['email'] = array(
        '#type' => 'textfield',
        '#title' => t('Email'),
        '#maxlength' => 255,
        '#default_value' => isset($data['parent1']['email']) ? $data['parent1']['email'] : $default_email,
        '#required' => TRUE,
      );
      $form['data']['parent1']['mobile'] = array(
        '#type' => 'textfield',
        '#title' => t('Mobile'),
        '#size' => 20,
        '#maxlength' => 40,
        '#default_value' => isset($data['parent1']['mobile']) ? $data['parent1']['mobile'] : '',
        '#required' => TRUE,
      );
      $form['data']['parent1']['home_phone'] = array(
        '#type' => 'textfield',
        '#title' => t('Home phone'),
        '#size' => 20,
        '#maxlength' => 40,
        '#default_value' => isset($data['parent1']['home_phone']) ? $data['parent1']['home_phone'] : '',
        '#required' => TRUE,
      );
      $form['data']['parent1']['work_phone'] = array(
        '#type' => 'textfield',
        '#title' => t('Work phone'),
        '#size' => 20,
        '#maxlength' => 40,
        '#default_value' => isset($data['parent1']['work_phone']) ? $data['parent1']['work_phone'] : '',
      );
      $form['data']['parent2']['parental_responsibility'] = array(
        '#type' => 'select',
        '#title' => t('Does this person have parental responsibility?'),
        '#options' => array('0' => t('No'), '1' => t('Yes')),
        '#default_value' => isset($data['parent2']['parental_responsibility']) ? $data['parent2']['parental_responsibility'] : 0,
        '#attributes' => array('class' => array('container-inline')),
        '#required' => TRUE,
      );
      break;

    case 3:
      $form['data']['#tree'] = TRUE;
      break;

    case 4:
      $form['data']['#tree'] = TRUE;
      break;

    case 5:
      $form['data']['#tree'] = TRUE;
      break;
  }
  $form['actions'] = array('#type' => 'actions');
  if ($form_state['storage']['step'] > 1) {
    $form['actions']['previous'] = array(
      '#type' => 'submit',
      '#value' => t('Previous'),
    );
  }
  if ($form_state['storage']['step'] < 5) {
    $form['actions']['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
    );
  }
  if ($form_state['storage']['step'] == 5) {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
  }

  // Add a cancel button at the bottom of the form.
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : '<front>',
  );

  return $form;
}

/**
 * Validation handler for the registration form.
 */
function cura_registration_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  switch ($form_state['storage']['step']) {

    case 1:
      break;

    case 2:
      if (!valid_email_address($values['data']['parent1']['email'])) {
        form_set_error('data][parent1][email', t('You must enter a valid e-mail address.'));
      }
      break;

    case 3:
      break;

    case 4:
      break;

    case 5:
      break;
  }
}

/**
 * Submission handler for the registration form.
 */
function cura_registration_form_submit($form, &$form_state) {

  // Get the registration from the form.
  $registration = $form_state['cura_registration'];

  // Save the form data.
//  if (isset($form_state['storage']['values'])) {
//    drupal_set_message('Submit form_state_storage_values:<pre>' . print_r($form_state['storage']['values'], TRUE) . '</pre>');
//    drupal_set_message('Submit form_state_values:<pre>' . print_r($form_state['values'], TRUE) . '</pre>');
//    $form_state['storage']['values'] = drupal_array_merge_deep($form_state['storage']['values'], $form_state['values']);
    $registration->data = drupal_array_merge_deep($registration->data, $form_state['values']['data']);
//    drupal_set_message('Submit array_merge:<pre>' . print_r($form_state['storage']['values'], TRUE) . '</pre>');
//  }
//  else {
//    $form_state['storage']['values'] = $form_state['values'];
//  }

  switch ($form_state['values']['op']) {

    case 'Next':
      $form_state['storage']['step']++;
      break;

    case 'Previous':
      $form_state['storage']['step']--;
      break;

    case 'Save draft':
//      $registration->data = serialize($form_state['storage']['values']['data']);
      cura_registration_save($registration);
//      $form_state['cura_registration'] = $registration;
      drupal_set_message('Saved draft registration.');
      break;

    case 'Submit':
//      $registration->data = serialize($form_state['storage']['values']['data']);
      cura_registration_submit($registration);
      break;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Handle the submission of a registration.
 */
function cura_registration_submit($registration) {
  $account = $GLOBALS['user'];
  $ip = ip_address();

  // Update the registration record.
  $registration->submitted_by = $account->uid;
  $registration->submitted_on = REQUEST_TIME;
  $registration->submitted_ip = $ip;
  $registration->status = REGISTRATION_SUBMITTED;
  cura_registration_save($registration);
}

