<?php

/**
 * @file
 * Administrative forms for the Cura Payment module.
 */

/**
 * Form builder; The settings form for the Cura cheque payment method.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function cura_payment_cheque_admin_config() {
  $form['cura_payment_cheque_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Override the default title of the Cura cheque payment method.'),
    '#default_value' => variable_get('cura_payment_cheque_title', 'Cheque'),
  );

  return system_settings_form($form);
}

/**
 * Form to confirm a payment transaction.
 */
function cura_payment_confirm_form($form, &$form_state, $order, $transaction) {
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;

  // Load and store the payment method instance for this transaction.
  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
  $form_state['payment_method'] = $payment_method;

  // Get default values.
  if (isset($transaction->payload)) {
    $default = $transaction->payload;
  }

  $form['information'] = array(
    '#markup' => t('Confirming a payment transaction means that the payment has been verified on a bank statement.') . '<br>',
  );

  // Use date field if available.
  if (module_exists('date')) {
    $form['date'] = array(
      '#type' => 'date_popup',
      '#title' =>  t('Payment date'),
      '#description' => t('Enter the date of the payment transaction from the bank statement.'),
      '#default_value' => isset($default['date']) ? $default['date'] : date('Y-m-d'),
      '#date_type' => DATE_DATETIME,
      '#date_timezone' => date_default_timezone(),
      '#date_format' => 'Y-m-d',
      '#date_increment' => 1,
      '#date_year_range' => '-3:+3',
      '#required' => TRUE,
    );
  }
  else {
    $form['date'] = array(
      '#type' => 'textfield',
      '#title' => t('Payment date'),
      '#description' => t('Enter the date of the payment transaction from the bank statement.'),
      '#default_value' => isset($default['date']) ? $default['date'] : date('Y-m-d'),
      '#size' => 48,
      '#required' => TRUE,
    );
  }

  // Amount.
  $balance = commerce_payment_order_balance($order);
  
  if ($balance['amount'] > 0 && $balance['amount'] < $transaction->amount) {
    $default_amount = $balance['amount'];
  }
  else {
    $default_amount = $transaction->amount;
  }
  
  // Convert the price amount to a user friendly decimal value.
  $default_amount = commerce_currency_amount_to_decimal($default_amount, $transaction->currency_code);
  
  $description = implode('<br />', array(
    t('Order balance: @balance', array('@balance' => commerce_currency_format($balance['amount'], $balance['currency_code']))),
  ));
  
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Confirm bank transfer amount'),
    '#description' => $description,
    '#default_value' => $default_amount,
    '#field_suffix' => check_plain($transaction->currency_code),
    '#size' => 16,
    '#required' => TRUE,
  );

  if ($transaction->payment_method == 'cura_payment_cheque') {

    // Payee name.
    $form['payee'] = array(
      '#type' => 'textfield',
      '#title' => t('Payee name'),
      '#description' => t('Enter the name of the payee.'),
      '#default_value' => isset($default['payee']) ? $default['payee'] : '',
      '#size' => 60,
      '#required' => TRUE,
    );

    // Cheque status.
    $form['status'] = array(
      '#type' => 'select',
      '#title' => t('Cheque status'),
      '#description' => t('Select the cheque deposit status.'),
      '#default_value' => $transaction->remote_status,
      '#options' => drupal_map_assoc(array('Received', 'Deposited', 'Cleared')),
      '#required' => TRUE,
    );

  }

  $form['reference'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference'),
    '#description' => t('Enter the payment reference.'),
    '#default_value' => $transaction->remote_id,
    '#size' => 32,
    '#required' => TRUE,
  );

  $form = confirm_form($form,
    t('What amount do you want to confirm?'),
    'admin/commerce/orders/' . $order->order_id . '/payment',
    '',
    t('Confirm'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Validate handler: ensure a valid amount is given.
 */
function cura_payment_confirm_form_validate($form, &$form_state) {
  $transaction = $form_state['transaction'];
  $amount = $form_state['values']['amount'];

  // Ensure a positive numeric amount has been entered for capture.
  if (!is_numeric($amount) || $amount <= 0) {
    form_set_error('amount', t('You must specify a positive numeric amount to capture.'));
  }

  // Ensure the amount is less than or equal to the transaction amount.
  if ($amount > commerce_currency_amount_to_decimal($transaction->amount, $transaction->currency_code)) {
    form_set_error('amount', t('You cannot confirm more than the transaction amount.'));
  }
}

/**
 * Submit callback for payment confirmation form.
 */
function cura_payment_confirm_form_submit($form, &$form_state) {
  $order = $form_state['order'];
  $transaction = $form_state['transaction'];

  // Date.
  $transaction->created = strtotime($form_state['values']['date']);
  $transaction->payload['date'] = $form_state['values']['date'];

  // Amount.
  $transaction->amount = commerce_currency_decimal_to_amount($form_state['values']['amount'], $transaction->currency_code);

  // Reference.
  $transaction->remote_id = check_plain($form_state['values']['reference']);
  $transaction->payload['reference'] = check_plain($form_state['values']['reference']);

  // Message and status.
  if ($transaction->payment_method == 'cura_payment_cheque') {
    $transaction->payload['payee'] = check_plain($form_state['values']['payee']);
    $transaction->remote_status = $form_state['values']['status'];
    switch($form_state['values']['status']) {
      case 'Received':
        $transaction->message .= '<br>' . t('Status set to received @date', array('@date' => date('H:i D, j M Y', REQUEST_TIME)));
        $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
        break;
      case 'Deposited':
        $transaction->message .= '<br>' . t('Status set to deposited @date', array('@date' => date('H:i D, j M Y', REQUEST_TIME)));
        $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
        break;
      case 'Cleared':
        $transaction->message .= '<br>' . t('Payment confirmed @date', array('@date' => date('H:i D, j M Y', REQUEST_TIME)));
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        break;
      default:
        $transaction->message .= '<br>' . t('Payment confirmed @date', array('@date' => date('H:i D, j M Y', REQUEST_TIME)));
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        break;
    }
  }
  else {
    $transaction->message .= '<br>' . t('Payment confirmed @date', array('@date' => date('H:i D, j M Y', REQUEST_TIME)));
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  }

  commerce_payment_transaction_save($transaction);
  drupal_set_message(t('Payment confirmed successfully.'));
  $form_state['redirect'] = 'admin/commerce/orders/' . $order->order_id . '/payment';
}

