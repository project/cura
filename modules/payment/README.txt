--------------------------------------------------------------------------------
- CURA PAYMENT MODULE
--------------------------------------------------------------------------------

CONTENTS OF THIS FILE
---------------------
   
1. Introduction
2. Requirements
3. Installation
4. Configuration
5. Troubleshooting
6. FAQ
7. Maintainers


1. INTRODUCTION
---------------

The Cura Payment module provides the payment options for the Cura
Childcare Suite.

 * To find out more about the Cura Childcare Suite visit the project page:
   https://www.drupal.org/project/cura

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/cura


2. REQUIREMENTS
---------------

This module requires the following modules:

 * Cura Childcare Suite (https://drupal.org/project/cura)


3. INSTALLATION
---------------

This module is installed with the Cura Childcare Suite.


4. CONFIGURATION
----------------

 * On enabling this module the Cura Childcare Suite payment methods will be
   available under Store » Configuration » Payment methods.

 * When a Cura Childcare Suite payment method is enabled it will appear in the
   payment method select list when adding a payment transaction to an order.


5. TROUBLESHOOTING
------------------

 * Module not appearing

   If the module is installed try uninstalling and then reinstalling.

 * Cura payment methods not appearing

   Check to see that the payment methods have been enabled.


6. FAQ
------

Q: Why is the Cura Payment module not appearing in the list of modules?

A: Ensure that the module was installed properly. If it was installed try
   installing it again.


Q: Why do I not have a Childcare voucher option in the list of payment methods?

A: Check that the Childcare voucher payment method has been enabled.


Q: The date of the payment transaction gets overridden when I save confirm
   the voucher payment. Is this normal?

A: This is as designed. It makes reporting and running entity field queries
   easier and simplifies the reconciliation of bank accounts to transaction
   data. The original payment transaction dates can always be queried from
   the revisions.


7. MAINTAINERS
--------------

Current maintainers:

* Mike Kovacevich (Canalside) - http://drupal.org/user/54136

This project has been sponsored by:

* Canalside Software
  Specialists in Drupal powered solutions offering consulting, project
  management, installation, development and hosting services.

--------------------------------------------------------------------------------

