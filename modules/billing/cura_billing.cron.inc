<?php

/**
 * @file
 * Cron functions for the Cura Billing module.
 */

/**
 * Cron daily functions.
 */
function cura_billing_cron_daily() {

  // Run these only once per week on a Thursday.
  if (date('N', time()) == 4) {

    // Place daily tasks here.

  }

  return TRUE;
}

/**
 * Cron weekly functions.
 */
function cura_billing_cron_weekly() {

  // Auto generated billing is currently disabled.
  cura_billing_generate_bills_cron();

  return TRUE;
}

/**
 * Cron monthly functions.
 */
function cura_billing_cron_monthly() {

  // Place monthly tasks here.

  return TRUE;
}

/**
 * Check to see if it is time to execute a bill run and, if so, generates bills for two school terms in advance.
 */
function cura_billing_generate_bills_cron() {

  // Auto bill run currently disabled.
  return FALSE;

  $bill_run_day = REQUEST_TIME;
  // $bill_run_day = strtotime('2014-04-27');

  if ($term_nids = cura_get_school_terms('nids', strtotime('-7 days', $bill_run_day), 2)) {
    // Check if there are two terms.
    if (count($term_nids) == 2) {
      $terms = node_load_multiple($term_nids);
      $start = strtotime($terms[$term_nids[0]]->field_term_dates[LANGUAGE_NONE][0]['value']);
      $finish = strtotime($terms[$term_nids[1]]->field_term_dates[LANGUAGE_NONE][0]['value2']);
      if (strtotime('Sunday', $start) == $bill_run_day
          && in_array($terms[$term_nids[0]]->field_school_term[LANGUAGE_NONE][0]['value'],
          array('1', '3', '5'))) {
        // If children are found create the bills.
        if ($child_nids = cura_get_children('nids', array('P'))) {
            $result = cura_billing_bills_create($start, $finish, $child_nids);
        }
        else {
          watchdog('cura_billing', 'Could not find any placed children. Cannot execute bill run.');
        }
      }
    }
    else {
      watchdog('cura_billing', 'Could not find two future school terms. Cannot execute bill run.');
    }
  }
  else {
    watchdog('cura_billing', 'Could not find any future school terms. Cannot execute bill run.');
  }
}

