<?php

/**
 * @file
 * The template for the Outstanding Orders email.
 *
 * Available variables:
 * - $orders: The outstanding orders.
 * - $payments: The most recent payments.
 * - $notes: The user-configurable notes.
 * - $footer: The user-configurable email footer.
 */
?>
<h2><?php print t('Recent payments'); ?></h2>

<?php // Bank transfer payments ?>
<h3 style="margin-left:10px;"><?php print t('Most recent bank transfer payments'); ?></h3>
<?php if ($payments['bank_transfer']) { ?>
  <table style="margin-left:10px;">
    <thead>
      <tr style="background-color:#f5f5f5;">
        <th style="text-align:left;padding:0 10px;">Date
        <th style="text-align:left;padding:0 10px;">Order
        <th style="text-align:left;padding:0 10px;">Owner
        <th style="text-align:left;padding:0 10px;">Reference
        <th style="text-align:right;padding:0 10px;">Amount
    </thead>
    <tbody>
      <?php $row = 1; ?>
      <?php foreach ($payments['bank_transfer'] as $transfer) { ?>
        <?php $tw = entity_metadata_wrapper('commerce_payment_transaction', $transfer); ?>
        <?php if (empty($tw->order_id->raw())) { ?>
        <?php   $order = commerce_order_new(); ?>
        <?php   $ow = entity_metadata_wrapper('commerce_order', $order); ?>
        <?php } else { ?>
        <?php   $ow = entity_metadata_wrapper('commerce_order', $tw->order_id->raw()); ?>
        <?php   $order = $ow->value(); ?>
        <?php } ?>
        <?php $transfer_date = date('Y-m-d', $tw->created->value()); ?>
        <?php $order_id = str_pad($tw->order_id->value(), 5, "0", STR_PAD_LEFT); ?>
        <?php $payment_ref = isset($transfer->remote_id) ? $transfer->remote_id : 'No reference'; ?>
        <?php if ($person = cura_person_load_by_uid($ow->owner->raw())) { ?>
        <?php   $pw = entity_metadata_wrapper('node', $person); ?>
        <?php   $order_owner = $pw->field_firstname->value() . ' ' . $pw->field_lastname->value(); ?>
        <?php } else { ?>
        <?php   $order_owner = $ow->owner->name->value(); ?>
        <?php } ?>
        <?php // Alternate way of displaying currency value: ?>
        <?php // $transfer_amount = $tw->currency_code->value() . number_format($tw->amount->value() / 100, 2); ?>
        <?php $transfer_amount = commerce_currency_format($tw->amount->value(), $tw->currency_code->value()); ?>
        <?php $colour = ($row % 2 == 1) ? '#fff' : '#f5f5f5'; ?>
        <tr style="background-color:<?php print $colour; ?>">
          <td style="padding:0 10px;"><?php print $transfer_date; ?>
          <td style="padding:0 10px;"><?php print $order_id; ?>
          <td style="padding:0 10px;"><?php print $order_owner; ?>
          <td style="padding:0 10px;"><?php print $payment_ref; ?>
          <td style="text-align:right;padding:0 10px;"><?php print $transfer_amount; ?>
        </tr>
        <?php $row++; ?>
      <?php } ?>
    </tbody>
  </table>
<?php } else { ?>
  <?php // No recent bank transfer payments. ?>
  <p style="margin-left:10px;"><?php print t('No bank transfer payments.'); ?></p>
<?php } ?>

<?php // Childcare voucher payments ?>
<h3 style="margin-left:10px;"><?php print t('Most recent childcare voucher payments'); ?></h3>
<?php if ($payments['childcare_voucher']) { ?>
  <table style="margin-left:10px;">
    <thead>
      <tr style="background-color:#f5f5f5;">
        <th style="text-align:left;padding:0 10px;">Date
        <th style="text-align:left;padding:0 10px;">Order
        <th style="text-align:left;padding:0 10px;">Provider
        <th style="text-align:left;padding:0 10px;">Reference
        <th style="text-align:right;padding:0 10px;">Amount
    </thead>
    <tbody>
      <?php $row = 1; ?>
      <?php foreach ($payments['childcare_voucher'] as $voucher) { ?>
        <?php $vw = entity_metadata_wrapper('commerce_payment_transaction', $voucher); ?>
        <?php $ow = entity_metadata_wrapper('commerce_order', $vw->order_id->raw()); ?>
        <?php $order = $ow->value(); ?>
        <?php $voucher_date = date('Y-m-d', $vw->created->value()); ?>
        <?php $order_id = str_pad($vw->order_id->value(), 5, "0", STR_PAD_LEFT); ?>
        <?php $payment_ref = isset($voucher->remote_id) ? $voucher->remote_id : 'No reference'; ?>
        <?php $order_owner = $ow->owner->name->value(); ?>
        <?php $voucher_amount = commerce_currency_format($vw->amount->value(), $vw->currency_code->value()); ?>
        <?php $voucher_payload = $voucher->payload; ?>
        <?php $colour = ($row % 2 == 1) ? '#fff' : '#f5f5f5'; ?>
        <tr style="background-color:<?php print $colour; ?>">
          <td style="padding:0 10px;"><?php print $voucher_date; ?>
          <td style="padding:0 10px;"><?php print $order_id; ?>
          <td style="padding:0 10px;"><?php print $voucher_payload['source']; ?>
          <td style="padding:0 10px;"><?php print $payment_ref; ?>
          <td style="text-align:right;padding:0 10px;"><?php print $voucher_amount; ?>
        </tr>
        <?php $row++; ?>
      <?php } ?>
    </tbody>
  </table>
<?php } else { ?>
  <?php // No recent voucher payments. ?>
  <p style="margin-left:10px;"><?php print t('No childcare voucher payments.'); ?></p>
<?php } ?>

<?php // Cheque payments ?>
<h3 style="margin-left:10px;"><?php print t('Most recent cheque payments'); ?></h3>
<?php if ($payments['cheque']) { ?>
  <table style="margin-left:10px;">
    <thead>
      <tr style="background-color:#f5f5f5;">
        <th style="text-align:left;padding:0 10px;">Date
        <th style="text-align:left;padding:0 10px;">Order
        <th style="text-align:left;padding:0 10px;">Owner
        <th style="text-align:left;padding:0 10px;">Reference
        <th style="text-align:right;padding:0 10px;">Amount
    </thead>
    <tbody>
      <?php $row = 1; ?>
      <?php foreach ($payments['cheque'] as $cheque) { ?>
        <?php $cw = entity_metadata_wrapper('commerce_payment_transaction', $cheque); ?>
        <?php $ow = entity_metadata_wrapper('commerce_order', $cw->order_id->raw()); ?>
        <?php $order = $ow->value(); ?>
        <?php $cheque_date = date('Y-m-d', $cw->created->value()); ?>
        <?php $order_id = str_pad($cw->order_id->raw(), 5, "0", STR_PAD_LEFT); ?>
        <?php $payment_ref = isset($cheque->remote_id) ? $cheque->remote_id : 'No reference'; ?>
        <?php $order_owner = $ow->owner->name->value(); ?>
        <?php $cheque_amount = commerce_currency_format($cw->amount->value(), $cw->currency_code->value()); ?>
        <?php $colour = ($row % 2 == 1) ? '#fff' : '#f5f5f5'; ?>
        <tr style="background-color:<?php print $colour; ?>">
          <td style="padding:0 10px;"><?php print $cheque_date; ?>
          <td style="padding:0 10px;"><?php print $order_id; ?>
          <td style="padding:0 10px;"><?php print $order_owner; ?>
          <td style="padding:0 10px;"><?php print $payment_ref; ?>
          <td style="text-align:right;padding:0 10px;"><?php print $cheque_amount; ?>
        </tr>
        <?php $row++; ?>
      <?php } ?>
    </tbody>
  </table>
<?php } else { ?>
  <?php // No recent cheque payments. ?>
  <p style="margin-left:10px;"><?php print t('No cheque payments.'); ?></p>
<?php } ?>

<?php // Cash payments ?>
<h3 style="margin-left:10px;"><?php print t('Most recent cash payments'); ?></h3>
<?php if ($payments['cash']) { ?>
  <table style="margin-left:10px;">
    <thead>
      <tr style="background-color:#f5f5f5;">
        <th style="text-align:left;padding:0 10px;">Date
        <th style="text-align:left;padding:0 10px;">Order
        <th style="text-align:left;padding:0 10px;">Owner
        <th style="text-align:left;padding:0 10px;">Reference
        <th style="text-align:right;padding:0 10px;">Amount
    </thead>
    <tbody>
      <?php $row = 1; ?>
      <?php foreach ($payments['cash'] as $cash) { ?>
        <?php $cw = entity_metadata_wrapper('commerce_payment_transaction', $cash); ?>
        <?php $ow = entity_metadata_wrapper('commerce_order', $cw->order_id->raw()); ?>
        <?php $order = $ow->value(); ?>
        <?php $cash_date = date('Y-m-d', $cw->created->value()); ?>
        <?php $order_id = str_pad($cw->order_id->raw(), 5, "0", STR_PAD_LEFT); ?>
        <?php $payment_ref = isset($cash->remote_id) ? $cash->remote_id : 'No reference'; ?>
        <?php $order_owner = $ow->owner->name->value(); ?>
        <?php $cash_amount = commerce_currency_format($cw->amount->value(), $cw->currency_code->value()); ?>
        <?php $colour = ($row % 2 == 1) ? '#fff' : '#f5f5f5'; ?>
        <tr style="background-color:<?php print $colour; ?>">
          <td style="padding:0 10px;"><?php print $cash_date; ?>
          <td style="padding:0 10px;"><?php print $order_id; ?>
          <td style="padding:0 10px;"><?php print $order_owner; ?>
          <td style="padding:0 10px;"><?php print $payment_ref; ?>
          <td style="text-align:right;padding:0 10px;"><?php print $cash_amount; ?>
        </tr>
        <?php $row++; ?>
      <?php } ?>
    </tbody>
  </table>
<?php } else { ?>
  <?php // No recent cash payments. ?>
  <p style="margin-left:10px;"><?php print t('No cash payments.'); ?></p>
<?php } ?>

<?php print $notes; ?>
<?php print $footer; ?>

