<?php

/**
 * @file
 * Administrative page callbacks for the Billing module of the Cura Childcare Suite.
 */

/**
 * Form constructor for the Billing settings page.
 *
 * @see cura_billing_menu()
 * @see cura_settings_form()
 * @ingroup forms
 */
function cura_billing_admin_settings($form, &$form_state) {
  $settings = cura_get_settings();

  $form['settings'] = array(
    '#tree' => TRUE,
  );
  $form['settings']['general']['billing']['payment']['payment_terms'] = array(
    '#type' => 'select',
    '#title' => t('Payment terms'),
    '#options' => array('30' => '30 days', '60' => '60 days', '90' => '90 days'),
    '#default_value' => $settings['general']['billing']['payment']['payment_terms'],
  );
  $form['settings']['general']['billing']['general']['bill_public_holidays'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bill public holidays'),
    '#default_value' => $settings['general']['billing']['general']['bill_public_holidays'],
  );
  $form['settings']['general']['billing']['general']['bill_school_holidays'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bill school holidays'),
    '#default_value' => $settings['general']['billing']['general']['bill_school_holidays'],
  );
  $form['settings']['general']['billing']['general']['bill_closures'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bill school closures'),
    '#default_value' => $settings['general']['billing']['general']['bill_closures'],
  );

  // Billing payment reminder notification.
  $form['settings']['notification']['email']['payment_reminder'] = array(
    '#type' => 'fieldset',
    '#title' => t('Payment reminder notifications'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer cura settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[general][notification][enabled]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['notification']['email']['payment_reminder']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable payment reminder notifications.'),
    '#default_value' => $settings['notification']['email']['payment_reminder']['enabled'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['payment_reminder']['from'] = array(
    '#type' => 'textfield',
    '#title' => t('The email address sending the notification.'),
    '#default_value' => $settings['notification']['email']['payment_reminder']['from'],
    '#access' => user_access('administer cura settings'),
  );

  // Billing outstanding orders notification.
  $form['settings']['notification']['email']['outstanding_orders'] = array(
    '#type' => 'fieldset',
    '#title' => t('Outstanding orders notifications'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer cura settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[general][notification][enabled]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['notification']['email']['outstanding_orders']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the sending of outstanding orders emails.'),
    '#default_value' => $settings['notification']['email']['outstanding_orders']['enabled'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['outstanding_orders']['from'] = array(
    '#type' => 'textfield',
    '#title' => t('The email address sending the outstanding orders email.'),
    '#default_value' => $settings['notification']['email']['outstanding_orders']['from'],
    '#maxlength' => 320,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['outstanding_orders']['to'] = array(
    '#type' => 'textarea',
    '#title' => t('Recipients (comma-separated) of the outstanding orders email.'),
    '#default_value' => $settings['notification']['email']['outstanding_orders']['to'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['outstanding_orders']['cc'] = array(
    '#type' => 'textarea',
    '#title' => t('The email addresses (comma-separated) to copy in.'),
    '#default_value' => $settings['notification']['email']['outstanding_orders']['cc'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['outstanding_orders']['payments_period'] = array(
    '#type' => 'select',
    '#title' => t('The payments period.'),
    '#options' => drupal_map_assoc(array('3 months', '6 months', '1 year', '2 years', '3 years')),
    '#default_value' => $settings['notification']['email']['outstanding_orders']['payments_period'],
    '#access' => user_access('administer cura settings'),
  );

  return cura_settings_form($form);
}

