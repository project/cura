<?php

/**
 * @file
 * Default theme implementation to display the Day of week summary.
 *
 * Available variables:
 * - $title: the (sanitized) title of the summary.
 * - $data: The summary data to display.
 *
 * @see template_preprocess()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div id="cura-hours-per-child">
  <h2><?php print $title; ?></h2>
  <?php if (isset($data['rows']) && !empty($data['rows'])) {

    // Sort the rows on lastname, firstname, child ID.
    usort($data['rows'], function($a, $b) {
      if (strcmp($a['lastname'], $b['lastname']) === 0) {
        if (strcmp($a['firstname'], $b['firstname']) === 0) {
          return $a['child_id'] - $b['child_id'];
        }
        else {
          return strcmp($a['firstname'], $b['firstname']);
        }
      }
      else {
        return strcmp($a['lastname'], $b['lastname']);
      }
    }); ?>

    <table class="table" style="margin-left:10px;">
      <thead>
        <tr style="background-color:#f5f5f5;">
          <th style="text-align:left;padding-right:10px;min-width:220px;width:280px;" rowspan="2">Child
          <th style="text-align:right;padding-right:10px;min-width:80px;" rowspan="2">Age*
          <th style="text-align:center;padding-right:10px;" colspan="7">Hours
        <tr style="background-color:#f5f5f5;">
          <th style="text-align:right;padding-right:10px;">Mon
          <th style="text-align:right;padding-right:10px;">Tue
          <th style="text-align:right;padding-right:10px;">Wed
          <th style="text-align:right;padding-right:10px;">Thu
          <th style="text-align:right;padding-right:10px;">Fri
          <th style="text-align:right;padding-right:10px;">Sat
          <th style="text-align:right;padding-right:10px;">Sun
      </thead>
      <tbody>
        <?php $num = 0; ?>
        <?php $totals = array(); ?>
        <?php $start = date('Y-m-d', $data['start']); ?>
        <?php foreach ($data['rows'] as $row) { ?>
          <?php $child = $row['firstname'] . ' ' . $row['lastname'] . ' (' . $row['child_id'] . ')'; ?>
          <?php $age = date_diff(date_create($row['birthdate']), date_create($start))->format('%yy %mm'); ?>
          <?php $colour = (($num + 1) % 2 == 1) ? '#fff' : '#f5f5f5'; $num++; ?>
          <tr style="background-color:<?php print $colour; ?>">
            <td style="padding-right:10px;"><?php print $num . '. ' . $child; ?>
            <td style="text-align:right;padding-right:10px;"><?php print $age; ?>
            <?php foreach (array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun') as $day) { ?>
              <?php if ($row[$day] == 0) { ?>
                <?php $style = 'color:gray;'; ?>
              <?php } else { ?>
                <?php $style = 'background-color:#ffffcc;'; ?>
              <?php } ?>
              <td style="text-align:right;padding-right:10px;<?php print $style; ?>">
              <?php print empty($row[$day]) ? '' : (float) $row[$day]; ?>
              <?php isset($totals[$day]) ? $totals[$day] += $row[$day] : $totals[$day] = $row[$day]; ?>
            <?php } ?>
        <?php } ?>
        <tr style="background-color:#f5f5f5;font-weight:bold;">
          <td style="padding-right:10px;">Total<td>
          <?php foreach (array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun') as $day) { ?>
            <td style="text-align:right;padding-right:10px;"><?php print $totals[$day]; ?>
          <?php } ?>
      </tbody>
    </table>
  <?php } else { ?>
    <p style="margin-left:10px;"><?php print t('No sessions for this period.'); ?></p>
  <?php } ?>
  <div>
    <p>*Age at <?php print date('l, j F Y', $data['start']); ?></p>
  </div>
</div>

