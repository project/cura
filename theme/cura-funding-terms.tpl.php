<?php

/**
 * @file
 * Template for displaying funding terms.
 *
 */

$account = $GLOBALS['user'];
$args = explode('/', drupal_strtolower(drupal_get_path_alias($_GET['q'])));
if (isset($args[1]) && $date = strtotime($args[1])) {
  $year = date('Y', $date);
}
else {
  $year = date('Y');
}
?>
<div id="funding-terms">
  <table class="table table-striped table-hover"><tbody>
    <?php for ($i = -1; $i <= 1; $i++) { ?>
      <tr><td colspan="2"><h3><?php print sprintf('%04d', $year + $i); ?></h3>
      <tr><td>Spring<td><?php print date('D, j M', strtotime(sprintf('%04d', $year + $i) . '-01-01')); ?>
      <tr><td>Summer<td><?php print date('D, j M', strtotime(sprintf('%04d', $year + $i) . '-04-01')); ?>
      <tr><td>Autumn<td><?php print date('D, j M', strtotime(sprintf('%04d', $year + $i) . '-09-01')); ?>
    <?php } ?>
  </tbody></table>
</div>

