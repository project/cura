<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 */
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php
$account = $GLOBALS['user'];
if ($account->uid) {
  if ($person = cura_person_load_by_uid($account->uid)) {
    echo '<p>Welcome ' . $person->field_firstname[LANGUAGE_NONE][0]['value'] . '</p>';
  }
  else {
    echo '<p>Welcome ' . $account->name . '</p>';
  }
}
else {
?>
<p><img alt="" src="/sites/default/files/PP_On_Slide.jpg" style="float:right; margin:5px; max-width:50%" class="img-responsive" />Polstead Preschool is a parent-run preschool established over 35 years ago. We are set in the hall at <a href="http://www.smi-oxford.org.uk" target="_blank">St Margaret's Institute</a> and serve the broader Oxford community.</p>

<p>Polstead Preschool can accommodate up to twenty-four children aged from 2 to 5 years. This mixed age group uses the same outdoor and indoor spaces, but enjoy different group time activities. We offer morning sessions from 9am to 12 noon and, where there is demand, afternoon sessions from 12 to 3pm.</p>

<p>Polstead Preschool is run by highly-qualified and skilled practitioners with the help of a very active parents' committee, which voices its needs and opinions at our monthly committee meetings.</p>

<p>The objective of our preschool is to provide a safe, exciting, happy and caring environment for your child. We recognise that each child's needs may be different; we will encourage each child's physical, intellectual, emotional and social skills through their play.</p>

<?php
}

if ($current_term = cura_get_current_school_term()) {
  echo '<p>We are currently in term ' . $current_term->field_school_term[LANGUAGE_NONE][0]['value'] . ' of school year ';
  echo date('Y', strtotime($current_term->field_school_year[LANGUAGE_NONE][0]['value'])) . ' which started on ';
  echo date('l, j F Y', strtotime($current_term->field_term_dates[LANGUAGE_NONE][0]['value'])) . ' and finishes ';
  echo date('l, j F Y', strtotime($current_term->field_term_dates[LANGUAGE_NONE][0]['value2'])) . '.</p>';
}
else {
  $terms = cura_get_school_terms('objects', REQUEST_TIME, 1);
  $next_term = reset($terms);
  echo '<p>We hope you are enjoying the holidays. The next term, ' . $next_term->title . ', starts on ';
  echo date('l, j F Y', strtotime($next_term->field_term_dates[LANGUAGE_NONE][0]['value'])) . ' and finishes ';
  echo date('l, j F Y', strtotime($next_term->field_term_dates[LANGUAGE_NONE][0]['value2'])) . '.</p>';
}

//module_load_include('inc', 'cura', 'cura.admissions'); // ** TESTING **
//echo cura_get_past_starters_leavers(60);
//cura_create_menus();
//$child = node_load(195);
//$session_plan_table = cura_display_session_plan($child);
//drupal_set_message('<pre>'.print_r($session_plan_table, TRUE).'</pre>');
//print render($session_plan_table);

//$start_date = cura_get_child_start_date(2764);
//echo '<p>Start date from cura_get_child_start_date is: '.date('Y-m-d', $start_date);

//  $current = variable_get('mail_system', array('default-system' => 'DefaultMailSystem'));
//  echo '<pre>Before:<br>'.print_r($current, TRUE);
//  $addition = array('cura' => 'CuraMailSystem', 'cura_billing' => 'CuraMailSystem');
//  variable_set('mail_system', array_merge($current, $addition));
//  echo '<pre>After:<br>'.print_r($current, TRUE);

?>

<div class="fb-like" data-href="http://www.facebook.com/polsteadoxford" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>

