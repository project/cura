<?php

/**
 * @file
 * The template for the registration confirmation email.
 *
 * Available variables:
 * - $order_id: The unique ID of the order.
 * - $parent: The parent/carer registering the child.
 * - $child: The child who was registered.
 */
?>

<?php $pw = entity_metadata_wrapper('node', $parent); ?>
<?php $cw = entity_metadata_wrapper('node', $child); ?>

<div style="text-align: center;">
  <img src="http://www.polsteadplaygroup.org.uk/sites/default/files/2013-12-12_PP_Logo3.gif" alt="Polstead Playgroup">
</div>'

<p>Dear <?php print $pw->firstname->value(); ?></p>

<p>Thank you for registering <?php print $cw->cura_firstname->value(); ?> with Polstead Playgroup. We look forward to welcoming <?php print $w['him_her']; ?> when <?php print $w['he_she']; ?> starts.</p>

<p>To pay the registration fee please follow the link below to checkout your order <?php print $order_id; ?>. We prefer that you make the payment by bank transfer but cheque and childcare voucher options are also available. Also note that you will need to be logged in before processing payment.</p>

<p>@checkout_link</p>

<p>Once you have checked out the order you can view it at any time by going to My account -> Orders.</p>

<p><?php print $cw->cura_firstname->value(); ?> will be added to our waiting list when we have confirmed receipt of your payment.</p>

