<?php

/**
 * @file
 * Default theme implementation to display the attendance summary.
 *
 * Available variables:
 * - $title: the (sanitized) title of the summary.
 * - $caption: a description of the table.
 * - $records: An array of summary records to display.
 *
 * @see template_preprocess()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div id="cura-hours-per-day">
  <?php if ($title) print '<h2>' . $title . '</h2>'; ?>
  <?php if ($records) { ?>
    <table class="table" style="margin-left:10px;">
      <?php if ($caption) print '<caption>' . $caption . '</caption>'; ?>
      <thead>
        <tr style="background-color:#f5f5f5;">
          <th style="text-align:left;padding-right:10px;min-width:100px;" rowspan="2">Date
          <th style="text-align:right;padding-right:10px;" rowspan="2">Children
          <th style="text-align:center;padding-right:10px;" colspan="3">Hours
          <th style="text-align:center;padding-right:10px;" colspan="3">Amount
        <tr style="background-color:#f5f5f5;">
          <th style="text-align:right;padding-right:10px;" width="100/3%">Total
          <th style="text-align:right;padding-right:10px;" width="100/3%">Non-funded
          <th style="text-align:right;padding-right:10px;" width="100/3%">Funded
          <th style="text-align:right;padding-right:10px;" width="100/3%">Total
          <th style="text-align:right;padding-right:10px;" width="100/3%">Non-funded
          <th style="text-align:right;padding-right:10px;" width="100/3%">Funded
      </thead>
      <tbody>
        <?php $row = 0; ?>
        <?php foreach ($records as $record) { ?>
          <?php $colour = (($row + 1) % 2 == 1) ? '#fff' : '#f5f5f5'; $row++; ?>
          <tr style="background-color:<?php print $colour; ?>">
            <td style="padding-right:10px;"><?php print date('D, j M', $record->date); ?>
            <td style="text-align:right;padding-right:10px;"><?php print $record->children; ?>
            <td style="text-align:right;padding-right:10px;"><?php print (float) $record->hours; ?>
            <td style="text-align:right;padding-right:10px;"><?php print (float) $record->hours_n; ?>
            <td style="text-align:right;padding-right:10px;"><?php print (float) $record->hours_f; ?>
            <td style="text-align:right;padding-right:10px;"><?php print $record->amount; ?>
            <td style="text-align:right;padding-right:10px;"><?php print $record->amount_n; ?>
            <td style="text-align:right;padding-right:10px;"><?php print $record->amount_f; ?>
        <?php } ?>
      </tbody>
    </table>
  <?php } else { ?>
    <p style="margin-left:10px;"><?php print t('No sessions for this period.'); ?></p>
  <?php } ?>
</div>

