
/**
 * Checks to see if a session of a specified type already exists for a child.
 *
 * @param $session_type_id
 *   The ID of the session type.
 * @param $child_nid
 *   The ID of the child.
 *
 * @return
 *   TRUE if a session already exists, FALSE if not.
 */
function cura_session_validate_unique($session_type_id, $child_nid) {
  if ($match_id = db_query('SELECT session_id FROM {cura_sessions} WHERE session_type_id = :session_type_id',
                              array(':session_type_id' => $session_type_id))->fetchField()) {
    if (empty($session_type_id) || $match_id != $session_type_id) {
      return FALSE;
    }
  }
  return TRUE;
}

