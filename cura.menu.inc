<?php

/**
 * @file
 * Creates the core menus and menu links for the Cura Childcare Suite.
 */

/**
 * Create the menus and menu links.
 */
function cura_create_menus($menus = NULL) {
  if (!$menus) $menus = _cura_get_default_menus();
  foreach ($menus as $menu_name => $menu) {
    // Create the menu if it does not already exist.
    if (!menu_load($menu_name)) {
      menu_save($menu['info']);
    }
    $plid = 0;
    _cura_create_links($menu['links'], $plid, $menu_name);
  }
}

/**
 * Create the menu links.
 */
function _cura_create_links($links, $plid = 0, $menu_name) {
  foreach ($links as $link) {
    $item = array(
      'menu_name' => $menu_name,
      'plid' => $plid,
      'link_path' => $link['link_path'],
      'link_title' => $link['link_title'],
      'expanded' => isset($link['expanded']) ? $link['expanded'] : 0,
      'weight' => $link['weight'],
    );
    // Check if the menu link already exists.
    $sql = "SELECT mlid FROM {menu_links} WHERE link_title=:link_title AND  link_path=:link_path AND menu_name=:menu_name";
    $vars = array(':link_title' => $link['link_title'], ':link_path' => $link['link_path'], ':menu_name' => $menu_name);
    $mlid = db_query($sql, $vars)->fetchField();

    // Create the link if it does not already exist.
    if (!$mlid) {
      $mlid = menu_link_save($item);
      drupal_set_message("Created link $mlid ($plid) - " . $link['link_title'] . ".");
    }

    // Create the sub-links if there are any.
    if (!empty($link['links'])) {
      _cura_create_links($link['links'], $mlid, $menu_name);
    }
  }
}

/**
 * Define default menus and menu links.
 */
function _cura_get_default_menus() {
  $menus['cura-menu'] = array(
    'info' => array(
      'menu_name' => 'cura-menu',
      'title' => 'Cura menu',
      'description' => 'The Cura Childcare Suite menu.',
    ),
    'links' => array(
      array(
        'link_path' => 'admin/config',
        'link_title' => 'Settings',
        'weight' => 0,
      ),
      array(
        'link_path' => drupal_lookup_path('source', 'admissions'),
        'link_title' => 'Admissions',
        'expanded' => 0,
        'weight' => 10,
        'links' => array(
          array(
            'link_path' => 'admissions/registrations',
            'link_title' => 'Registrations',
            'weight' => 0,
          ),
          array(
            'link_path' => 'admissions/waiting-list',
            'link_title' => 'Waiting list',
            'weight' => 10,
          ),
          array(
            'link_path' => 'admissions/offers',
            'link_title' => 'Offers',
            'weight' => 20,
          ),
        ),
      ),
      array(
        'link_path' => 'calendar',
        'link_title' => 'Calendar',
        'expanded' => 0,
        'weight' => 20,
        'links' => array(
          array(
            'link_path' => 'terms',
            'link_title' => 'School terms',
            'weight' => 0,
          ),
          array(
            'link_path' => 'holidays',
            'link_title' => 'Holidays',
            'weight' => 10,
          ),
          array(
            'link_path' => 'closures',
            'link_title' => 'Closures',
            'weight' => 20,
          ),
          array(
            'link_path' => 'session-plans',
            'link_title' => 'Session plans',
            'weight' => 30,
          ),
          array(
            'link_path' => 'sessions',
            'link_title' => 'Sessions',
            'weight' => 40,
          ),
        ),
      ),
      array(
        'link_path' => 'persons',
        'link_title' => 'Persons',
        'weight' => 30,
      ),
      array(
        'link_path' => 'children',
        'link_title' => 'Children',
        'weight' => 40,
      ),
      array(
        'link_path' => 'accounts',
        'link_title' => 'Accounts',
        'weight' => 50,
      ),
      array(
        'link_path' => 'staff',
        'link_title' => 'Staff',
        'expanded' => 0,
        'weight' => 60,
        'links' => array(
          array(
            'link_path' => 'staff/job-descriptions',
            'link_title' => 'Job descriptions',
            'weight' => 0,
          ),
          array(
            'link_path' => 'staff/security-checks',
            'link_title' => 'Security checks',
            'weight' => 10,
          ),
        ),
      ),
      array(
        'link_path' => 'meetings',
        'link_title' => 'Meetings',
        'weight' => 70,
      ),
      array(
        'link_path' => 'orders',
        'link_title' => 'Orders',
        'weight' => 80,
      ),
      array(
        'link_path' => 'fees',
        'link_title' => 'Billing',
        'expanded' => 0,
        'weight' => 90,
        'links' => array(
          array(
            'link_path' => 'billing/generate-invoices',
            'link_title' => 'Generate invoices',
            'weight' => 0,
          ),
          array(
            'link_path' => 'invoices',
            'link_title' => 'View invoices',
            'weight' => 10,
          ),
          array(
            'link_path' => 'billing/post-invoices',
            'link_title' => 'Post invoices',
            'weight' => 20,
          ),
        ),
      ),
    ),
  );
  return $menus;
}

