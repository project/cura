<?php

/**
 * @file
 * User functions for the Cura Childcare Suite.
 */

/**
 * Form for managing users.
 *
 * @param object $user
 *   The user object being managed.
 * @param string $op
 *   The operation being performed.
 */
function cura_user_form($form, &$form_state, $user, $op) {

}

