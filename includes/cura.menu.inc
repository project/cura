/**
 * Create the menus for the Cura Childcare Suite.
 */
function __cura_create_menus() {
  $menus = array(
    'cura-menu' => array(
      'menu_name' => 'cura-menu',
      'title' => 'Cura main menu',
      'description' => 'This is the main menu for the Cura Childcare Suite.',
    ),
  );

  $links = array(
    'cura-menu' => array(
      'Admissions' => array(
        'link_path' => 'admissions',
        'weight' => 0,
        'expanded' => 0,
        'links' => array(
          'Registrations' => array(
            'path' => 'admissions/registrations',
            'weight' => 0,
          ),
          'Waiting list' => array(
            'path' => 'admissions/waiting-list',
            'weight' => 10,
          ),
          'Offers' => array(
            'path' => admissions/offers',
            'weight' => 20,
        ),
      ),
      'Calendar' => array(
        'link_path' => 'http://yourdomain.com/link2',
        'weight' => 1,
      ),
    ),
  );

  // Save menu group into menu_custom table
  foreach ($menus as $menu) {
    // Create the menu if it does not already exist.
    $sql = 'SELECT title FROM {menu_custom} WHERE menu_name = :n';
    $exists = db_query($sql, array(':n' => $menu['menu_name']))->fetchField();
    if (!$exists) {
      menu_save($menu);
    }
  }

  $item = '';
  foreach ($links as $layer1) {
    foreach ($layer1 as $link) {
      // Build an array of menu link
      $item = array(
        'link_path' => $link['link_path'],
        'link_title' => $link['link_title'],
        'menu_name' => $link['menu_name'],
        'weight' => $link['weight'],
        'expanded' => $link['expanded'],
      );
      __create_menu_link($link);
    }
  }
}

/**
 * Create a single menu item.
 */
function __create_menu_link($link) {
  $sql = 'SELECT mlid FROM {menu_links}';
  $sql .= ' WHERE menu_name = :n AND link_title = :t AND link_path = :p';
  $vars = array(':n' => $link['menu_name'], ':t' => $link['link_title'], ':p' => $link['link_path']);
  $exists = db_query($sql, $vars)->fetchField();
  // Create the menu link if it does not already exist.
  if (!$exists) {
    menu_link_save($link);
  }
}


