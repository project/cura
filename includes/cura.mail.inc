<?php

/**
 * @file
 * Mail system for the Cura Childcare Suite.
 */

/**
 * Enables the sending of HTML email.
 */
class CuraMailSystem extends DefaultMailSystem {
  public function format(array $message) {
    $message['body'] = implode("\n\n", $message['body']);
    $message['body'] = drupal_wrap_mail($message['body']);
    return $message;
  }
}

