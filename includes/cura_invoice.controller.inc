<?php

/**
 * @file
 * The controller for the invoice entity containing the CRUD operations.
 */

/**
 * The controller class for invoices contains methods for the invoice CRUD
 * operations. The load method is inherited from the default controller.
 */
class CuraInvoiceEntityController extends CuraEntityController {

  /**
   * Create a default invoice.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   *
   * @return
   *   An invoice object with all default fields initialized.
   */
  public function create(array $values = array()) {

    $account = $GLOBALS['user'];

    $values += array(
      'invoice_id' => NULL,
      'label' => 'Invoice',
      'date' => REQUEST_TIME,
      'due_date' => REQUEST_TIME,
      'child_id' => 0,
      'description' => 'Invoice description',
      'notes' => 'Invoice notes',
      'tran_type' => 'I',
      'amount' => 0,
      'currency_code' => 'GBP',
      'status' => 'Pending',
      'created_by' => $account->uid,
      'created_on' => REQUEST_TIME,
      'updated_by' => $account->uid,
      'updated_on' => REQUEST_TIME,
      'active' => 1,
    );

    return parent::create($values);
  }

  /**
   * Saves an invoice.
   *
   * When saving an invoice without an invoice ID, this function will create a new
   * invoice at that time. For new invoices, it will also determine and save the
   * invoice number and then save the initial revision of the invoice. Subsequent
   * invoices that should be saved as new revisions should set $invoice->revision to
   * TRUE and include a log string in $invoice->log.
   *
   * @param $invoice
   *   The full invoice object to save.
   * @param $transaction
   *   An optional database transaction object.
   *
   * @return
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($invoice, DatabaseTransaction $transaction = NULL) {
    if (!isset($transaction)) {
      $transaction = db_transaction();
      $started_transaction = TRUE;
    }

    try {
      $account = $GLOBALS['user'];

      // Determine if we will be inserting a new invoice.
      $invoice->is_new = empty($invoice->invoice_id);

      // Set the timestamp fields.
      if ($invoice->is_new) {
        if (empty($invoice->created_by)) $invoice->created_by = $account->uid;
        if (empty($invoice->created_on)) $invoice->created_on = REQUEST_TIME;
//        if (empty($invoice->created_ip)) $invoice->created_ip = ip_address();
      } else {
        // Otherwise if the invoice is not new but comes from an entity_create()
        // or similar function call that initializes the created timestamp, uid,
        // and hostname values to empty strings, unset them to prevent
        // destroying existing data in those properties on update.
        if ($invoice->label == 'Invoice') unset($invoice->label);
        if ($invoice->child_id == 0) unset($invoice->child_id);
        if ($invoice->description == 'Invoice description') unset($invoice->description);
        if ($invoice->notes == 'Invoice notes') unset($invoice->notes);
        if (isset($invoice->created_by)) unset($invoice->created_by);
        if (isset($invoice->created_on)) unset($invoice->created_on);
        if (isset($invoice->created_ip)) unset($invoice->created_ip);
      }

      $invoice->updated_by = $account->uid;
      $invoice->updated_on = REQUEST_TIME;
//      $invoice->updated_ip = ip_address();

      $invoice->revision_timestamp = REQUEST_TIME;
      $invoice->revision_hostname = ip_address();
      $invoice->revision_uid = $account->uid;

      // Recalculate the order total using the current line item data.
      commerce_order_calculate_total($invoice);

      if ($invoice->is_new || !empty($invoice->revision)) {
        // When inserting either a new invoice or revision, $invoice->log must be set
        // because {cura_invoice_revision}.log is a text column and therefore
        // cannot have a default value. However, it might not be set at this
        // point, so we ensure that it is at least an empty string in that case.
        if (!isset($invoice->log)) {
          $invoice->log = '';
        }
      }
      elseif (empty($invoice->log)) {
        // If we are updating an existing invoice without adding a new revision,
        // we need to make sure $invoice->log is unset whenever it is empty. As
        // long as $invoice->log is unset, drupal_write_record() will not attempt
        // to update the existing database column when re-saving the revision.
        unset($invoice->log);
      }

      return parent::save($invoice, $transaction);
    }
    catch (Exception $e) {
      if (!empty($started_transaction)) {
        $transaction->rollback();
        watchdog_exception($this->entityType, $e);
      }
      throw $e;
    }
  }

  /**
   * Unserializes the data property of loaded invoices.
   */
  public function attachLoad(&$queried_invoices, $revision_id = FALSE) {
    foreach ($queried_invoices as $invoice_id => &$invoice) {
      $invoice->data = unserialize($invoice->data);
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_cura_invoice_load().
    parent::attachLoad($queried_invoices, $revision_id);
  }
}

