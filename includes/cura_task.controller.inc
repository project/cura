<?php

/**
 * @file
 * The controller for the Cura task entity containing the CRUD operations.
 */

/**
 * The controller class for tasks contains methods for the task CRUD operations.
 *
 * Mainly relies on the EntityAPIController class provided by the Entity
 * module, just overrides specific features.
 */
class CuraTaskEntityController extends CuraEntityController {

  /**
   * Creates a Cura task.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   * @return
   *   A cura_task object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $account = $GLOBALS['user'];
    if (empty($values['status'])) $values['status'] = 'New';
    if (empty($values['owned_by'])) $values['owned_by'] = $account->uid;

    return parent::create($values);
  }

  /**
   * Saves a Cura task.
   *
   * @param $task
   *   The full task object to save.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($task, DatabaseTransaction $transaction = NULL) {
    $ip = ip_address();

    // Check if this task is new.
    if (empty($task->{$this->idKey})) {
      $task->created_on = REQUEST_TIME;
      $task->created_ip = $ip;
    }

    $task->updated_on = REQUEST_TIME;
    $task->updated_ip = $ip;
    if (empty($task->active)) $task->active = 1;

    return parent::save($task, $transaction);
  }

  /**
   * Unserializes the data property of loaded tasks.
   */
  public function attachLoad(&$queried_tasks, $revision_id = FALSE) {
    foreach ($queried_tasks as $task_id => &$task) {
      $task->data = unserialize($task->data);
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_commerce_product_load().
    parent::attachLoad($queried_tasks, $revision_id);
  }

  /**
   * Deletes multiple tasks by ID.
   *
   * @param $task_ids
   *   An array of task IDs to delete.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($task_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($task_ids)) {
      $tasks = $this->load($task_ids, array());

      // Ensure the tasks can actually be deleted.
//      foreach ((array) $tasks as $task_id => $task) {
//        if (!cura_task_can_delete($task)) {
//          unset($tasks[$task_id]);
//        }
//      }

      // If none of the specified tasks can be deleted, return FALSE.
      if (empty($tasks)) {
        return FALSE;
      }

      parent::delete(array_keys($tasks), $transaction);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Builds a structured array representing the entity's content.
   *
   * The content built for the entity will vary depending on the $view_mode
   * parameter.
   *
   * @param $entity
   *   An entity object.
   * @param $view_mode
   *   View mode, e.g. 'full', 'teaser'...
   * @param $langcode
   *   (optional) A language code to use for rendering. Defaults to the global
   *   content language of the current request.
   * @return
   *   The renderable array.
   */
  public function buildContent($task, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Prepare a reusable array representing the CSS file to attach to the view.
    $attached = array(
      'css' => array(drupal_get_path('module', 'commerce_product') . '/theme/commerce_product.theme.css'),
    );

    // Add the default fields inherent to the task entity.
    $content['task_id'] = array(
      '#markup' => 'Task ID: ' . $task->task_id,
      '#attached' => $attached,
    );
    $content['label'] = array(
      '#markup' => 'Label: ' . $task->label,
      '#attached' => $attached,
    );
    $content['status'] = array(
      '#markup' => 'Status: ' . $task->status,
      '#attached' => $attached,
    );

    return parent::buildContent($task, $view_mode, $langcode, $content);
  }
}

