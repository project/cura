<?php

/**
 * @file
 * Administrative callbacks for the Cura Childcare Suite.
 */

/**
 * Form constructor for the Cura general settings page.
 *
 * @see cura_menu()
 * @see cura_settings_form()
 * @ingroup forms
 */
function cura_general_settings_form($form, &$form_state) {
  $settings = cura_get_settings();
//  drupal_set_message('<pre>Settings:<br>' . print_r($settings, TRUE) . '<br>'); // ** TESTING **

  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['settings'] = array(
    '#tree' => TRUE,
  );

  // General organisation settings.
  $form['settings']['general']['org']['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('Organisation'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#weight' => -50,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['org']['general']['org_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Organisation name'),
    '#default_value' => $settings['general']['org']['general']['org_name'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['org']['general']['org_name_short'] = array(
    '#type' => 'textfield',
    '#title' => t('Organisation short name'),
    '#default_value' => $settings['general']['org']['general']['org_name_short'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['org']['general']['org_type'] = array(
    '#type' => 'select',
    '#title' => t('Type of organisation'),
    '#options' => drupal_map_assoc(array('Charity', 'Company')),
    '#default_value' => $settings['general']['org']['general']['org_type'],
  );
  $form['settings']['general']['org']['general']['org_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Organisation number'),
    '#default_value' => $settings['general']['org']['general']['org_number'],
    '#size' => 20,
    '#maxlength' => 30,
  );
  $form['settings']['general']['org']['general']['org_number_ofsted'] = array(
    '#type' => 'textfield',
    '#title' => t('Ofsted URN'),
    '#default_value' => $settings['general']['org']['general']['org_number_ofsted'],
    '#size' => 10,
    '#maxlength' => 10,
  );
  $form['settings']['general']['org']['general']['footer'] = array(
    '#type' => 'textarea',
    '#title' => t('The email footer template'),
    '#default_value' => $settings['general']['org']['general']['footer'],
    '#access' => user_access('administer cura settings'),
  );

  // Bank settings.
  $form['settings']['general']['org']['bank'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bank details'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#tree' => TRUE,
    '#weight' => -40,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['org']['bank']['account_bank'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank/Institution'),
    '#default_value' => $settings['general']['org']['bank']['account_bank'],
  );
  $form['settings']['general']['org']['bank']['account_branch'] = array(
    '#type' => 'textfield',
    '#title' => t('Branch office'),
    '#default_value' => $settings['general']['org']['bank']['account_branch'],
  );
  $form['settings']['general']['org']['bank']['account_owner'] = array(
    '#type' => 'textfield',
    '#title' => t('Account owner'),
    '#description' => t('Name associated with bank account.'),
    '#default_value' => $settings['general']['org']['bank']['account_owner'],
  );

  $form['settings']['general']['org']['bank']['account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Account number'),
    '#default_value' => $settings['general']['org']['bank']['account_number'],
  );
 
  $form['settings']['general']['org']['bank']['account_iban'] = array(
    '#type' => 'textfield',
    '#title' => t('IBAN'),
    '#description' => t('International Bank Account Number'),
    '#default_value' => $settings['general']['org']['bank']['account_iban'],
  );
  
  $form['settings']['general']['org']['bank']['bank_code_appellation'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank code appellation'),
    '#description' => t('Appellation of bank code - depending on where your bank is located you should set this to: "BSB" (AU), "Sort code" (UK), "Bank code" (DE), "Clearing number" (CH), "Routing transit number" (US) or "Bank transit number" (CA)'),
    '#default_value' => $settings['general']['org']['bank']['bank_code_appellation'],
  ); 

  $form['settings']['general']['org']['bank']['bank_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Bank code'),
    '#description' => t('Actual bank code, will be shown with appellation set above. In the UK this will be the sort code.'),
    '#default_value' => $settings['general']['org']['bank']['bank_code'],
  );

  $form['settings']['general']['org']['bank']['account_swift'] = array(
    '#type' => 'textfield',
    '#title' => t('SWIFT'),
    '#description' => t('SWIFT-Code (aka BIC = Bank Identifier Code)'),
    '#default_value' => $settings['general']['org']['bank']['account_swift'],
  );

  // Calendar settings.
  $form['settings']['general']['calendar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Calendar'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#tree' => TRUE,
    '#weight' => -30,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['calendar']['days']['open'] = array(
    '#markup' => '<table><thead><tr><th style="width:33%"><th style="width:33%">Start<th>Finish</thead>',
  );

  foreach (array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun') as $dow) {
    $form['settings']['general']['calendar']['days'][$dow]['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => $dow,
      '#default_value' => $settings['general']['calendar']['days'][$dow]['enabled'],
      '#prefix' => '<tr><td>',
    );
    $form['settings']['general']['calendar']['days'][$dow]['start'] = array(
      '#type' => 'date_popup',
      '#date_format' => 'H:i',
      '#default_value' => $settings['general']['calendar']['days'][$dow]['start'],
      '#states' => array(
        'visible' => array(
          ':input[name="settings[general][calendar][days][' . $dow . '][enabled]"]' => array('checked' => TRUE),
        ),
      ),
      '#prefix' => '<td>',
    );
    $form['settings']['general']['calendar']['days'][$dow]['finish'] = array(
//      '#type' => 'textfield',
      '#type' => 'date_popup',
      '#date_format' => 'H:i',
      '#default_value' => $settings['general']['calendar']['days'][$dow]['finish'],
      '#states' => array(
        'visible' => array(
          ':input[name="settings[general][calendar][days][' . $dow . '][enabled]"]' => array('checked' => TRUE),
        ),
      ),
      '#prefix' => '<td>',
    );
  }

  $form['settings']['general']['calendar']['days']['close'] = array(
    '#markup' => '</table>',
  );

  $form['settings']['general']['calendar']['terms']['terms_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Terms only'),
    '#description' => t('Select this option if your setting is only open in term-time. Terms are maintained in the system as a specific content type. <a href="/terms">Manage terms</a>.'),
    '#default_value' => $settings['general']['calendar']['terms']['terms_only'],
    '#access' => user_access('administer cura settings'),
  );

  $form['settings']['general']['calendar']['weeks']['weeks_open'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of weeks open in a year'),
    '#default_value' => $settings['general']['calendar']['weeks']['weeks_open'],
    '#size' => 2,
    '#maxlength' => 2,
    '#required' => TRUE,
  );

  // Notification settings.
  $form['settings']['general']['notification'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notifications'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#weight' => -20,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['notification']['email']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable email notifications'),
    '#default_value' => $settings['general']['notification']['email']['enabled'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['general']['notification']['text']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable text notifications'),
    '#default_value' => $settings['general']['notification']['text']['enabled'],
    '#access' => user_access('administer cura settings'),
    '#disabled' => TRUE,
  );

  // Email notification settings.
  $form['settings']['notification']['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Emails'),
    '#collapsible' => TRUE,
    '#group' => 'vertical_tabs',
    '#tree' => TRUE,
    '#weight' => -10,
    '#access' => user_access('administer cura settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[general][notification][email][enabled]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['notification']['email']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable notifications'),
    '#default_value' => $settings['notification']['email']['enabled'],
    '#access' => user_access('administer cura settings'),
  );

  // Weekly plan email notification.
  $form['settings']['notification']['email']['weekly_plan'] = array(
    '#type' => 'fieldset',
    '#title' => t('Weekly plan email'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer cura settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[notification][email][enabled]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['notification']['email']['weekly_plan']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable weekly plan email notifications'),
    '#default_value' => $settings['notification']['email']['weekly_plan']['enabled'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['weekly_plan']['from'] = array(
    '#type' => 'textfield',
    '#title' => t('The email address sending the weekly plan email'),
    '#default_value' => $settings['notification']['email']['weekly_plan']['from'],
    '#maxlength' => 320,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['weekly_plan']['to'] = array(
    '#type' => 'textarea',
    '#title' => t('The email addresses to which the email is sent. Separate with commas.'),
    '#default_value' => $settings['notification']['email']['weekly_plan']['to'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['weekly_plan']['cc'] = array(
    '#type' => 'textarea',
    '#title' => t('The email address copied in on the weekly plan email.'),
    '#default_value' => $settings['notification']['email']['weekly_plan']['cc'],
    '#access' => user_access('administer cura settings'),
  );

  // Starters and leavers email notification.
  $form['settings']['notification']['email']['starters_leavers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Starters and leavers email'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer cura settings'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[notification][email][enabled]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['notification']['email']['starters_leavers']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable starters and leavers email notifications'),
    '#default_value' => $settings['notification']['email']['starters_leavers']['enabled'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['starters_leavers']['from'] = array(
    '#type' => 'textfield',
    '#title' => t('The email address sending the starters and leavers email'),
    '#default_value' => $settings['notification']['email']['starters_leavers']['from'],
    '#maxlength' => 320,
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['starters_leavers']['to'] = array(
    '#type' => 'textarea',
    '#title' => t('The email addresses to which the email is sent. Separate with commas.'),
    '#default_value' => $settings['notification']['email']['starters_leavers']['to'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['starters_leavers']['cc'] = array(
    '#type' => 'textarea',
    '#title' => t('The email address copied in on the email.'),
    '#default_value' => $settings['notification']['email']['starters_leavers']['cc'],
    '#access' => user_access('administer cura settings'),
  );
  $form['settings']['notification']['email']['starters_leavers']['count'] = array(
    '#type' => 'select',
    '#title' => t('The amount of past records to display.'),
    '#options' => array('30' => '30', '60' => '60', '120' => '120', '240' => '240'),
    '#default_value' => $settings['notification']['email']['starters_leavers']['count'],
    '#access' => user_access('administer cura settings'),
  );

  return cura_settings_form($form);
}

/**
 * Form constructor for the Cura pricing settings page.
 *
 * @see cura_menu()
 * @see cura_settings_form()
 * @ingroup forms
 */
function cura_pricing_settings_form($form, &$form_state) {
  $settings = cura_get_settings();

  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['settings'] = array(
    '#tree' => TRUE,
  );

  $form['settings']['general']['pricing']['product']['cura_reg_product_id'] = array(
    '#title' => t('Registration fee product'),
    '#type' => 'select',
    '#options' => db_query('SELECT product_id, title FROM {commerce_product} WHERE type = :type',
                           array(':type' => 'fee'))->fetchAllKeyed(),
    '#default_value' => variable_get('cura_reg_product_id', '0'),
    '#required' => TRUE,
  );
  $form['settings']['general']['pricing']['product']['cura_late_product_id'] = array(
    '#title' => t('Late payment fee product'),
    '#type' => 'select',
    '#options' => db_query('SELECT product_id, title FROM {commerce_product} WHERE type = :type',
                           array(':type' => 'fee'))->fetchAllKeyed(),
    '#default_value' => variable_get('cura_late_product_id', '0'),
    '#required' => TRUE,
  );
  $form['settings']['general']['pricing']['product']['cura_dep_product_id'] = array(
    '#title' => t('Deposit product'),
    '#type' => 'select',
    '#options' => db_query('SELECT product_id, title FROM {commerce_product} WHERE type = :type',
                           array(':type' => 'deposit'))->fetchAllKeyed(),
    '#default_value' => variable_get('cura_dep_product_id', '0'),
    '#required' => TRUE,
  );

  return cura_settings_form($form);
}

/**
 * Generates the session type editing form.
 *
 * Implements ENTITY_TYPE_form().
 */
function cura_session_type_form($form, &$form_state, $session_type, $op = 'edit', $entity_type = NULL) {
//  drupal_set_message('<pre>session_type beginning of form:<br>' . print_r($session_type, TRUE) . '<br>'); // ** TESTING **
  $dow = array(1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat', 7 => 'Sun');
  if ($op == 'clone') {
    // Only label is provided for cloned entities.
    $session_type->label .= ' (cloned)';
    $session_type->type = $entity_type . '_clone';
  }
  $form['overview'] = array(
    '#markup' => t('A session type is key to the workings of the system. It is used to break up a day into one or more sessions.'),
  );
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => isset($session_type->name) ? $session_type->name : '',
    '#required' => TRUE,
  );
  $form['start'] = array(
    '#title' => t('Start (HH:MM)'),
    '#type' => 'textfield',
    '#default_value' => isset($session_type->start) ? $session_type->start : '',
    '#description' => t('This is the description tag for the Start field.'),
    '#required' => TRUE,
  );
  $form['finish'] = array(
    '#title' => t('Finish (HH:MM)'),
    '#type' => 'textfield',
    '#default_value' => isset($session_type->finish) ? $session_type->finish : '',
    '#description' => t('This is the description tag for the Finish field.'),
    '#required' => TRUE,
  );
  $form['duration'] = array(
    '#title' => t('Duration (Hours)'),
    '#type' => 'textfield',
    '#default_value' => isset($session_type->duration) ? $session_type->duration : 0,
    '#description' => t('The duration of the session is calculated when the form is submitted.'),
    '#disabled' => TRUE,
  );
  $form['days_fieldset'] = array(
    '#title' => t('Days'),
    '#type' => 'fieldset',
    '#required' => TRUE,
  );
  $form['days_fieldset']['days'] = array(
    '#type' => 'checkboxes',
    '#options' => $dow,
    '#default_value' => isset($session_type->days) ? unserialize($session_type->days) :
                        array(1, 2, 3, 4, 5),
    '#required' => TRUE,
  );
  if (isset($session_type->limits)) $limits = unserialize($session_type->limits);
  $form['limits'] = array(
    '#markup' => t('Session limits'),
    '#type' => 'container',
    '#tree' => TRUE,
    '#prefix' => '<table><tr>',
    '#suffix' => '</table>',
  );
  for ($i = 1; $i <= 7; $i++) {
    $form['limits'][$i] = array(
      '#title' => t($dow[$i]),
      '#type' => 'textfield',
      '#size' => 5,
      '#default_value' => isset($limits[$i]) ? $limits[$i] : 0,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
  }
  $form['billable'] = array(
    '#title' => t('Is this session billable?'),
    '#type' => 'checkbox',
    '#default_value' => isset($session_type->billable) ? $session_type->billable : 1,
  );
  $form['product_id'] = array(
    '#title' => t('Product'),
    '#type' => 'select',
    '#options' => db_query('SELECT product_id, title FROM {commerce_product} WHERE type = :type',
                           array(':type' => 'session'))->fetchAllKeyed(),
    '#default_value' => isset($session_type->product_id) ? $session_type->product_id : 1,
    '#required' => TRUE,
  );
  $form['sup_product_id'] = array(
    '#title' => t('Product for supplementary fee'),
    '#type' => 'select',
    '#options' => db_query('SELECT product_id, title FROM {commerce_product} WHERE type = :type',
                           array(':type' => 'session'))->fetchAllKeyed(),
    '#default_value' => isset($session_type->sup_product_id) ? $session_type->sup_product_id : 1,
    '#required' => TRUE,
  );
  $form['active'] = array(
    '#title' => t('Is this session active?'),
    '#type' => 'checkbox',
    '#default_value' => isset($session_type->active) ? $session_type->active : 1,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save session type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the session type form.
 */
function cura_session_type_form_submit(&$form, &$form_state) {
  $account = $GLOBALS['user'];
  $ip = ip_address();
  $session_type = entity_ui_form_submit_build_entity($form, $form_state);
  $session_type->label = $session_type->name . ' ' . $session_type->start . ' - ' . $session_type->finish;
//  drupal_set_message('<pre>session_type:<br>' . print_r($session_type, TRUE) . '<br>'); // ** TESTING **
  $session_type->days = serialize($session_type->days);
  $session_type->limits = serialize($session_type->limits);
  // Check if it is a new session type.
  if (empty($session_type->session_type_id)) {
    $session_type->is_new = TRUE;
    $session_type->created_by = $account->uid;
    $session_type->created_on = REQUEST_TIME;
    $session_type->created_ip = $ip;
  }
  $session_type->updated_by = $account->uid;
  $session_type->updated_on = REQUEST_TIME;
  $session_type->updated_ip = $ip;

  // Save and go back.
  $session_type->save();
  $form_state['redirect'] = 'admin/structure/session-types';
//  drupal_set_message('<pre>form_state[values]:<br>' . print_r($form_state['values'], TRUE) . '<br>'); // ** TESTING **
}

/**
 * Form API validate callback for the session type form.
 */
function cura_session_type_form_validate($form, &$form_state) {
  $today = date('Y-m-d');
  $start = strtotime($today . ' ' . $form_state['values']['start']);
  if ($start == FALSE) {
    form_error($form, t('Start time must be entered in HH:MM format.'));
  }
  $finish = strtotime($today . ' ' . $form_state['values']['finish']);
  if ($finish == FALSE) {
    form_error($form, t('Finish time must be entered in HH:MM format.'));
  }

  if ($start >= $finish) {
    form_error($form, t('Finish time must be later than start time.'));
  }
  $duration = round(($finish - $start) / 3600, 2);
  $form_state['values']['start'] = date('H:i', $start);
  $form_state['values']['finish'] = date('H:i', $finish);
  $form_state['values']['duration'] = $duration;
//  drupal_set_message('Validation function working!'); // ** TESTING **
}

/**
 * Generates the session plan editing form.
 *
 * Implements ENTITY_TYPE_form().
 */
function cura_session_plan_form($form, &$form_state, $session_plan, $op = 'edit', $entity_type = NULL) {
  if (isset($session_plan->session_type_id) && isset($session_plan->child_id)) {
    if ($session = cura_session_last_load($session_plan->session_type_id, $session_plan->child_id)) {
      if (isset($session_plan->start) && $session_plan->start <= $session->date) {
        $form['overview'] = array(
          '#markup' => t('<p>This plan cannot be changed as it has active sessions.</p><p>The date of the last active session on this plan is ' . date('Y-m-d', $session->date) . '.</p>'),
        );
        return $form;
      }
    }
  }
//  drupal_set_message('<pre>session_plan beginning of form:<br>' . print_r($session_plan, TRUE) . '<br>'); // ** TESTING **
  $dow = array(1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat', 7 => 'Sun');
  if ($op == 'clone') {
    // Only label is provided for cloned entities.
    $session_plan->label .= ' (cloned)';
    $session_plan->type = $entity_type . '_clone';
  }
  $form['overview'] = array(
    '#markup' => t('<p>A session plan is a weekly schedule of sessions for the specified child.</p>'),
  );
  $form['child_id'] = array(
    '#title' => t('Child'),
    '#type' => 'hidden',
    '#default_value' => isset($session_plan->child_id) ? $session_plan->child_id : 0,
  );
  if (!isset($session_plan->child_id)) {
    $form['child_id'] = array(
      '#title' => t('Child'),
      '#type' => 'select',
      '#options' => cura_get_children('select', 'All'),
      '#required' => TRUE,
    );
  }
  $session_types = entity_load('cura_session_type');
  foreach ($session_types as $session_type) {
    $options[$session_type->session_type_id] = $session_type->label;
  }
  $form['session_type_id'] = array(
    '#title' => t('Session type'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => isset($session_plan->session_type_id) ? $session_plan->session_type_id : 1,
    '#required' => TRUE,
  );
  if (isset($session_plan->days)) $days = explode(',', $session_plan->days);
//  if (isset($days)) drupal_set_message('<pre>days:<br>' . print_r($days, TRUE) . '<br>'); // ** TESTING **
  $form['days'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#prefix' => '<p><table><tr>',
    '#suffix' => '</table></p>',
    'content' => array(
      '#markup' => t('<label>Days</label>'),
    ),
  );
  for ($i = 1; $i <= 7; $i++) {
    $form['days'][$i] = array(
      '#title' => t($dow[$i]),
      '#type' => 'checkbox',
      '#return_value' => $dow[$i],
      '#default_value' => isset($days[$i-1]) ? $days[$i-1] : 0,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
  }
  if (isset($session_plan->funding)) $funding = explode(',', $session_plan->funding);
  $form['funding'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#prefix' => '<label>Funding hours per day</label><table><tr>',
    '#suffix' => '</table>',
  );
  for ($i = 1; $i <= 7; $i++) {
    $form['funding'][$i] = array(
      '#title' => t($dow[$i]),
      '#type' => 'textfield',
      '#size' => 5,
      '#default_value' => isset($funding[$i-1]) ? $funding[$i-1] : 0,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    );
  }
  $form['start'] = array(
    '#title' => t('The date when this session plan starts'),
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#date_label_position' => 'within',
    '#default_value' => isset($session_plan->start) ?
                        date('Y-m-d', $session_plan->start) : date('Y-m-d', REQUEST_TIME),
    '#required' => TRUE,
  );
  $form['active'] = array(
    '#title' => t('Is this session plan active?'),
    '#type' => 'checkbox',
    '#default_value' => isset($session_plan->active) ? $session_plan->active : 1,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save session plan'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the session plan form.
 */
function cura_session_plan_form_submit(&$form, &$form_state) {
  $account = $GLOBALS['user'];
  $ip = ip_address();
//  drupal_set_message('<pre>form_state_values (before):<br>' . print_r($form_state['values'], TRUE) . '<br>'); // ** TESTING **
  $form_state['values']['days'] = implode(',', $form_state['values']['days']);
//  drupal_set_message('<pre>form_state_days ( after):<br>' . print_r($form_state['values']['days'], TRUE) . '<br>'); // ** TESTING **
  $session_plan = entity_ui_form_submit_build_entity($form, $form_state);
  $cw = entity_metadata_wrapper('node', $session_plan->child_id);
  $session_type = current(entity_load('cura_session_type', (array) $session_plan->session_type_id));
//  drupal_set_message('<pre>session_type:<br>' . print_r($session_type, TRUE) . '<br>'); // ** TESTING **
  $session_plan->label = $session_type->name . ' for ' . $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
//  $session_plan->days = implode(',', $session_plan->days);
  $session_plan->funding = implode(',', $session_plan->funding);
  $session_plan->start = strtotime($session_plan->start);
  // Check if it is a new session plan.
  if (empty($session_plan->session_plan_id)) {
    $session_plan->is_new = TRUE;
    $session_plan->created_by = $account->uid;
    $session_plan->created_on = REQUEST_TIME;
    $session_plan->created_ip = $ip;
  }
  $session_plan->updated_by = $account->uid;
  $session_plan->updated_on = REQUEST_TIME;
  $session_plan->updated_ip = $ip;
  $session_plan->save();

  // Update the start date on the child record.
  if ($start_date = cura_get_child_start_date($cw->getIdentifier())) {
    $cw->field_child_start_date = $start_date;
    $cw->save();
  }

  // Delete all the inactive session records for the child.
  module_load_include('inc', 'cura', 'cura.sessions');
  cura_session_delete($cw->getIdentifier());

  // Regenerate the session records based on the new session plans.
  if ($cw->field_child_status->value() <> 'C') {
    cura_sessions_create($start_date, NULL, array($cw->getIdentifier()));
  }

  $form_state['redirect'] = 'attendance/plans';
//  drupal_set_message('<pre>form_state[values]:<br>' . print_r($form_state['values'], TRUE) . '<br>'); // ** TESTING **
}

/**
 * Form API validate callback for the session plan form.
 */
function cura_session_plan_form_validate($form, &$form_state) {
  if ($child = node_load($form_state['values']['child_id'])) {
    $fn = $child->cura_firstname[LANGUAGE_NONE][0]['value'];
    $ln = $child->cura_lastname[LANGUAGE_NONE][0]['value'];
    $child_start = date('Y-m-d', strtotime($child->field_child_start_date[LANGUAGE_NONE][0]['value']));
    $plan_start = date('Y-m-d', strtotime($form_state['values']['start']));
    if ($plan_start < $child_start) {
      $error = $plan_start . ' is earlier than the start of ' . $fn . ' ' . $ln . ' (' . $child_start . ')';
      form_error($form, $error);
    }
  }
}

/**
 * Generates the session editing form.
 *
 * Implements ENTITY_TYPE_form().
 */
function cura_session_form($form, &$form_state, $session, $op = 'edit', $entity_type = NULL) {
  if ($op == 'clone') {
    // Only label is provided for cloned entities.
    $session->label .= ' (cloned)';
//    $session->type = $entity_type . '_clone';
  }
  $session_types = entity_load('cura_session_type');
  foreach ($session_types as $session_type) {
    $options[$session_type->session_type_id] = $session_type->label;
  }
  $form['overview'] = array(
    '#markup' => t('A session is key to the workings of the system. It is used to break up a day into one or more sessions.'),
  );
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($session->label) ? $session->label : 'Auto generated',
    '#disabled' => TRUE,
  );
  $form['session_type_id'] = array(
    '#title' => t('Session type ID'),
//    '#type' => 'cura_session_type',
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => isset($session->session_type_id) ? $session->session_type_id : 1,
    '#required' => TRUE,
  );
  $form['date'] = array(
    '#title' => t('Date'),
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#date_label_position' => 'within',
    '#default_value' => isset($session->date) ?
                        date('Y-m-d', $session->date) : date('Y-m-d', REQUEST_TIME),
    '#required' => TRUE,
  );
  $header = array(
    'name' => array('data' => t('Name'), 'field' => 'n.cura_lastname', 'sort' => 'asc'),
    'birthdate' => array('data' => t('Birthdate'), 'field' => 'n.field_birth_date'),
  );
  $form['children'] = array(
    '#type' => 'tableselect',
    '#title' => t('Children'),
    '#header' => $header,
    '#options' => cura_get_children('tableselect', array('P')),
    '#tree' => TRUE,
    '#multiple' => TRUE,
//    '#size' => 50,
    '#default_value' => isset($session->children) ? unserialize($session->children) : NULL,
    '#description' => t('Select the children attending this session.'),
  );
  drupal_set_message('<pre>session->children:<br>' . print_r($session->children, TRUE) . '<br>'); // ** TESTING **
  drupal_set_message('<pre>unserialize:<br>' . print_r(unserialize($session->children), TRUE) . '<br>'); // ** TESTING **
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save session'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the session form.
 */
function cura_session_form_submit(&$form, &$form_state) {
  $account = $GLOBALS['user'];
  $ip = ip_address();
  $session = entity_ui_form_submit_build_entity($form, $form_state);
  $session_type_id = $session->session_type_id;
  $session_type = entity_load('cura_session_type', (array) $session_type_id);
  $session_type = $session_type[$session_type_id];
//  drupal_set_message('<pre>session type:<br>' . print_r($session_type, TRUE) . '<br>'); // ** TESTING **
  $session->label = $session->date . ' ' . $session_type->name;
  $session->date = strtotime($session->date);
  $session->children = serialize($session->children);
  // Check if it is a new session.
  if (empty($session->session_id)) {
    $session->is_new = TRUE;
    $session->created_by = $account->uid;
    $session->created_on = REQUEST_TIME;
    $session->created_ip = $ip;
  }
  $session->session_type_label = $session_type->label;
  $session->start = $session_type->start;
  $session->finish = $session_type->finish;
  $session->duration = $session_type->duration;
  $session->billable = $session_type->billable;
  $session->updated_by = $account->uid;
  $session->updated_on = REQUEST_TIME;
  $session->updated_ip = $ip;

  // Save and go back.
  $session->save();
  $form_state['redirect'] = 'admin/structure/sessions';
//  drupal_set_message('<pre>session:<br>' . print_r($session, TRUE) . '<br>'); // ** TESTING **
}

