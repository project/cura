<?php

/**
 * @file
 * Provides a central controller for the Cura Childcare Suite.
 */

class CuraEntityController extends EntityAPIController {

  /**
   * Create a new Cura entity.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   * @return
   *   A new instance of the entity type.
   */
  public function create(array $values = array()) {

    $account = $GLOBALS['user'];
    $ip = ip_address();

    // Default to the current user if no user provided.
    if (empty($values['created_by'])) $values['created_by'] = $account->uid;
    if (empty($values['uid'])) $values['uid'] = $account->uid;

    // Add other default values.
    $values += array(
      'created_on' => REQUEST_TIME,
      'created_ip' => $ip,
      'updated_by' => $account->uid,
      'updated_on' => REQUEST_TIME,
      'updated_ip' => $ip,
      'active' => 1,
    );

    // If there is a class for this entity type, instantiate it now.
    if (isset($this->entityInfo['entity class']) && $class = $this->entityInfo['entity class']) {
      $entity = new $class($values, $this->entityType);
    }
    else {
      // Otherwise create a stdClass object.
      $entity = (object) $values;
    }

    // Allow other modules to alter the created entity.
    drupal_alter('cura_entity_create', $this->entityType, $entity);

    return $entity;
  }

}

