<?php

/**
 * Views customisations for the Cura Childcare Suite.
 */

/**
 * Implements hook_views_data_alter().
 */
function cura_views_data_alter(&$data) {
//  drupal_set_message('<pre>' . print_r($data, TRUE) . '</pre>');
  $data['node']['child_age'] = array(
    'title' => t('Age of child'),
    'help' => t('Shows the current age of the child'),
    'real field' => 'field_birth_date',
    'field' => array(
      'handler' => 'cura_handler_field_node_child_age',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name_field' => 'title',
      'string' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'help' => t('Filter results to a particular result set'),
    ),
    'sort' => array('handler' => 'views_handler_sort'),
  );
}

