<?php

/**
 * @file
 * Contains the field handler for child_age.
 */

/**
 * Field handler to calculate current age from birthdate.
 */
class cura_handler_field_node_child_age extends views_handler_field {

  /**
   * Get the value that's supposed to be rendered.
   *
   * This api exists so that other modules can easy set the values of the field
   * without having the need to change the render method as well.
   *
   * @param $values
   *   An object containing all retrieved values.
   * @param $field
   *   Optional name of the field where the value is stored.
   */
  function get_value($values, $field = NULL) {
//    drupal_set_message('Field: ' . $field);
    $field = 'field_' . $this->real_field;
    $birth_date = new DateTime($values->{$field}[0]['raw']['value']);
    $time = new DateTime();
    $diff = $time->diff($birth_date);
    $age = '';
    if (1 === $diff->invert) {
      $age = sprintf('%dy %dm', $diff->y, $diff->m);
    }
//    drupal_set_message('Values:<br><pre>' . print_r($values->{$field}, TRUE) . '</pre>');

    return $age;
  }

//  function render($values) {
//    $birth_date = $this->options['birth_date'];
//    $birth_date = DateTime::createFromFormat('U', $birth_date);
//    $birth_date = new DateTime($values->field_birth_date[LANGUAGE_NONE][0]['value']);
//    $birth_date = new DateTime('2016-01-01');
//    $time = new DateTime();
//    $diff = $time->diff($birth_date);
//    $age = printf('%d years, %d month, %d days', $diff->y, $diff->m, $diff->d);
//    $age = $diff->y;
//    $tokens = $this->get_render_tokens('');
//    if (!empty($tokens)) {
//      $birth_date = DateTime::createFromFormat('U', $tokens[$birth_date]);
//      $time = new DateTime();
//      $diff = $time->diff($birth_date);
//      $age = printf('%d years, %d month, %d days', $diff->y, $diff->m, $diff->d);
//      return $age;
//      return $this->sanitize_value($age);
//    }
//
//    return 0;
//  }

  function option_definition() {
    $options = parent::option_definition();
    // Add our custom settings fields.
    $options['birth_date'] = array();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $options = array();
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $options[t('Fields')]["[$field]"] = $handler->ui_name();
    }
    $form['birth_date'] = array(
      '#type' => 'select',
      '#title' => t('Birth date'),
      '#options' => $options,
      '#default_value' => isset($this->options['birth_date']) ? $this->options['birth_date'] : '',
      '#required' => TRUE,
    );
    parent::options_form($form, $form_state);
  }

  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
    // Update our custom values.
    $this->options['birth_date'] = $form_state['values']['options']['birth_date'];
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
}

