<?php

/**
 * @file
 * Creates the core fields for the Cura Childcare Suite.
 */

/**
 * Create the fields.
 */
function cura_create_fields($fields = NULL) {
  if (!$fields) $fields = _cura_get_default_fields();
  foreach ($fields as $field_name => $field) {
    // Create the field if it does not already exist.
    if (!field_info_field($field_name)) {
      $field['module'] = 'cura';
      $field['locked'] = 1;
      field_create_field($field);
      watchdog('cura', 'Created field @f.', array('@f' => $field_name));
    }
  }
}

/**
 * Define default fields.
 */
function _cura_get_default_fields() {
  $fields['cura_child_image'] = array(
    'field_name' => 'cura_child_image',
    'type' => 'image',
    'settings' => array(
      'uri_scheme' => 'public',
      'default_image' => '16',
    ),
  );
  $fields['cura_ethnicity'] = array(
    'field_name' => 'cura_ethnicity',
    'type' => 'list_text',
    'settings' => array(
      'allowed_values' => array(
        'AAO' => 'Any other Asian background',
        'ABA' => 'Bangladeshi',
        'AIN' => 'Indian',
        'APK' => 'Pakistani',
        'BLB' => 'Black Caribbean',
        'BLF' => 'African',
        'BLG' => 'Any other black background',
        'CHE' => 'Chinese',
        'MBA' => 'White/Black African',
        'MOT' => 'Any other mixed background',
        'MWA' => 'White Asian',
        'MWB' => 'White/Black Caribbean',
        'NOT' => 'Information not obtained',
        'OEO' => 'Any other ethnic group',
        'REF' => 'Refused to give',
        'WHA' => 'Any other white background',
        'WHB' => 'British',
        'WHR' => 'Irish',
        'WHT' => 'Traveller/Irish Heritage',
        'WRO' => 'Roma/Roma Gypsy',
      ),
    ),
  );
  $fields['cura_firstname'] = array(
    'field_name' => 'cura_firstname',
    'type' => 'text',
    'settings' => array(
      'max_length' => '35',
    ),
  );
  $fields['cura_gender'] = array(
    'field_name' => 'cura_gender',
    'type' => 'list_text',
    'settings' => array(
      'allowed_values' => array(
        'M' => 'Male',
        'F' => 'Female',
      ),
    ),
  );
  $fields['cura_key_person'] = array(
    'field_name' => 'cura_key_person',
    'type' => 'entityreference',
    'settings' => array(
      'target_type' => 'node',
      'handler' => 'views',
      'handler_settings' => array(
        'view' => array(
          'view_name' => 'our_staff',
          'display_name' => 'entityreference_1',
          'args' => array(
          ),
        ),
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
      ),
    ),
  );
  $fields['cura_lastname'] = array(
    'field_name' => 'cura_lastname',
    'type' => 'text',
    'settings' => array(
      'max_length' => '60',
    ),
  );

  return $fields;
}

/**
 * Create the field instances.
 */
function cura_create_field_instances($instances = NULL) {
  if (!$instances) $instances = _cura_get_default_field_instances();
  foreach ($instances as $entity_type => $bundles) {
    foreach ($bundles as $bundle_name => $fields) {
      foreach ($fields as $field_name => $instance) {
        if (!field_info_instance($entity_type, $field_name, $bundle_name)) {
          $instance['field_name'] = $field_name;
          $instance['entity_type'] = $entity_type;
          $instance['bundle'] = $bundle_name;
          field_create_instance($instance);
          watchdog('cura', 'Created instance of @f for bundle @b.', array(
            '@f' => $field_name, '@b' => $bundle_name));
        }
        else {
          watchdog('cura', 'Instance of @f for bundle @b already exists!', array(
            '@f' => $field_name, '@b' => $bundle_name));
        }
      }
    }
  }
}

/**
 * Define default field instances.
 */
function _cura_get_default_field_instances() {

  // Child node.
  $instances['node']['child'] = array(
    'cura_firstname' => array(
      'label' => 'Firstname',
      'description' => 'The first name, or given name.',
      'required' => TRUE,
      'widget' => array(
        'weight' => '1',
        'type' => 'text_textfield',
        'module' => 'text',
        'active' => 1,
        'settings' => array(
          'size' => '35',
        ),
      ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'text_default',
          'weight' => '1',
          'module' => 'text',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'weight' => 0,
        ),
      ),
    ),
    'cura_lastname' => array(
      'label' => 'Lastname',
      'description' => 'The last name, or family name.',
      'required' => TRUE,
      'widget' => array(
        'weight' => '2',
        'type' => 'text_textfield',
        'module' => 'text',
        'active' => 1,
        'settings' => array(
          'size' => '60',
        ),
      ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'text_default',
          'weight' => '2',
          'module' => 'text',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'weight' => 0,
        ),
      ),
    ),
    'cura_child_image' => array(
      'label' => 'Image',
      'description' => 'Passport-style 200 x 200 photo of the child.',
      'widget' => array(
        'weight' => '5',
        'type' => 'image_image',
        'module' => 'image',
        'active' => 1,
        'settings' => array(
          'progress_indicator' => 'throbber',
          'preview_image_style' => 'thumbnail',
        ),
      ),
      'settings' => array(
        'file_directory' => 'child',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '2 MB',
        'max_resolution' => '400x400',
        'min_resolution' => '200x200',
        'alt_field' => 1,
        'title_field' => 1,
        'default_image' => '17',
        'user_register_form' => false,
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'image',
          'weight' => '0',
          'settings' => array(
            'image_style' => '',
            'image_link' => '',
          ),
          'module' => 'image',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'weight' => 0,
        ),
      ),
    ),
    'cura_gender' => array(
      'label' => 'Gender',
      'description' => 'Specify the gender of the child.',
      'required' => TRUE,
      'widget' => array(
        'weight' => '6',
        'type' => 'options_buttons',
        'module' => 'options',
        'active' => 1,
      ),
      'settings' => array(
        'user_register_form' => false,
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'hidden',
          'weight' => '4',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'weight' => 0,
        ),
      ),
    ),
    'cura_key_person' => array(
      'label' => 'Key person',
      'description' => 'The key person assigned to a child.',
      'widget' => array(
        'type' => 'options_select',
        'weight' => '9',
        'settings' => array(
        ),
        'module' => 'options',
      ),
      'settings' => array(
        'user_register_form' => false,
      ),
      'display' => array(
        'default' => array(
          'label' => 'inline',
          'type' => 'entityreference_label',
          'weight' => '7',
          'settings' => array(
            'link' => false,
          ),
          'module' => 'entityreference',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(
          ),
          'weight' => 0,
        ),
      ),
    ),
  );

  // Person node.
  $instances['node']['person'] = array(

  );

  return $instances;
}

