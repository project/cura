<?php

/**
 * @file
 * Cron functions for the Cura Childcare Suite.
 */

/**
 * Cron daily functions.
 */
function cura_cron_daily() {
  cura_update_children_left();
  module_load_include('inc', 'cura', 'cura.sessions');
  cura_sessions_create();

  // Run these only once per week on a Thursday.
  if (date('N', time()) == 4) {
    module_load_include('inc', 'cura', 'cura.admissions');
    cura_admissions_send_starters_leavers_email();
  }

  return TRUE;
}

/**
 * Cron weekly functions run every Sunday.
 */
function cura_cron_weekly() {
  cura_send_weekly_plan_email();

  return TRUE;
}

/**
 * Cron monthly functions.
 */
function cura_cron_monthly() {

  return TRUE;
}

/**
 * Changes status to L for children who have left.
 */
function cura_update_children_left() {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'child')
    ->fieldCondition('field_child_status', 'value', 'P', '=')
    ->fieldCondition('field_child_finish_date', 'value', date('Ymd'), '<')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (isset($result['node'])) {
    $child_nids = array_keys($result['node']);
    $children = entity_load('node', $child_nids);
    foreach ($children as $nid => $child) {
      $cw = entity_metadata_wrapper('node', $child);
      $cw->field_child_status->set('L');
      $cw->save();
      watchdog('cura', 'Updated status to Left (L) for child %c.', array('%c' => $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value() . ' (' . $child->nid . ').'));
    }
  }
  else {
    watchdog('cura', 'No children to update to Left (L) status!');
  }
}

