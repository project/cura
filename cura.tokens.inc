<?php

/**
 * @file
 * Builds placeholder replacement tokens for cura data.
 */

/**
 * Implements hook_token_info().
 */
function cura_token_info() {

  // Define token types.
  $types['org'] = array(
    'name' => t('Organisation'),
    'description' => t('Tokens for organisation.'),
  );
  $types['gender'] = array(
    'name' => t('Gender'),
    'description' => t('Tokens for gender words.'),
    'needs-data' => array('gender'),
  );
  $types['person'] = array(
    'name' => t('Person'),
    'description' => t('Tokens for person.'),
    'needs-data' => array('person'),
  );
  $types['account'] = array(
    'name' => t('Account'),
    'description' => t('Tokens for account.'),
    'needs-data' => array('account'),
  );
  $types['child'] = array(
    'name' => t('Child'),
    'description' => t('Tokens for child.'),
    'needs-data' => array('child'),
  );

  // Define organisation-related tokens.
  $org['name'] = array(
    'name' => t("Organisation name"),
    'description' => t("The name of the organisation."),
  );
  $org['name-short'] = array(
    'name' => t("Short organisation name"),
    'description' => t("The short name of the organisation."),
  );
  $org['type'] = array(
    'name' => t("Organisation type"),
    'description' => t("The type of the organisation."),
  );
  $org['number'] = array(
    'name' => t("Organisation number"),
    'description' => t("The number of the organisation."),
  );
  $org['number-ofsted'] = array(
    'name' => t("Organisation Ofsted number"),
    'description' => t("The Ofsted number for the organisation."),
  );

  // Define gender-related tokens.
  $gender['type'] = array(
    'name' => t("Gender type"),
    'description' => t("The type of gender."),
  );
  $gender['he-she'] = array(
    'name' => t("He or she"),
    'description' => t("He or she."),
  );
  $gender['his-her'] = array(
    'name' => t("His or her"),
    'description' => t("His or her."),
  );
  $gender['him-her'] = array(
    'name' => t("Him or her"),
    'description' => t("Him or her."),
  );
  $gender['boy-girl'] = array(
    'name' => t("Boy or girl"),
    'description' => t("Boy or girl."),
  );
  $gender['son-daughter'] = array(
    'name' => t("Son or daughter"),
    'description' => t("Son or daughter."),
  );

  // Define person-related tokens.
  $person['person-id'] = array(
    'name' => t("Person ID"),
    'description' => t("The ID of the person."),
  );
  $person['firstname'] = array(
    'name' => t("Person firstname"),
    'description' => t("The firstname of the person."),
  );
  $person['lastname'] = array(
    'name' => t("Person lastname"),
    'description' => t("The lastname of the person."),
  );
  $person['name'] = array(
    'name' => t("Person name"),
    'description' => t("The name of the person."),
  );

  // Define account-related tokens.
  $account['account-id'] = array(
    'name' => t("Account ID"),
    'description' => t("The ID of the account."),
  );
  $account['number'] = array(
    'name' => t("Account number"),
    'description' => t("The number of the account."),
  );
  $account['owner'] = array(
    'name' => t("Account owner"),
    'description' => t("The owner of the account."),
  );
  $account['balance'] = array(
    'name' => t("Account balance"),
    'description' => t("The balance of the account."),
  );
  $account['status'] = array(
    'name' => t("Account status"),
    'description' => t("The status of the account."),
  );

  // Define child-related tokens.
  $child['child-id'] = array(
    'name' => t("Child ID"),
    'description' => t("The unique ID of the child."),
  );
  $child['firstname'] = array(
    'name' => t("Child firstname"),
    'description' => t("The firstname of the child."),
  );
  $child['lastname'] = array(
    'name' => t("Child lastname"),
    'description' => t("The lastname of the child."),
  );
  $child['birthdate'] = array(
    'name' => t("Child birthdate"),
    'description' => t("The birthdate of the child."),
  );
  $child['startdate'] = array(
    'name' => t("Child start date"),
    'description' => t("The start date of the child."),
  );
  $child['finishdate'] = array(
    'name' => t("Child finish date"),
    'description' => t("The finish date of the child."),
  );
  $child['plan'] = array(
    'name' => t("Child attendance plan"),
    'description' => t("The attendance plan of the child."),
  );
  $child['position'] = array(
    'name' => t("Child waiting list position"),
    'description' => t("The waiting list position of the child."),
  );

  return array(
    'types' => $types,
    'tokens' => array(
      'org' => $org,
      'gender' => $gender,
      'person' => $person,
      'account' => $account,
      'child' => $child,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function cura_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

//  drupal_set_message('type:<br><pre>' . print_r($type, TRUE) . '</pre>');
//  drupal_set_message('data:<br><pre>' . print_r($data, TRUE) . '</pre>');
  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);
  $settings = cura_get_settings('general');

  if ($type == 'org') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'name':
          $org_name = $settings['general']['org']['general']['org_name'];
          $replacements[$original] = $sanitize ? check_plain($org_name) : $org_name;
          break;

        case 'name-short':
          $org_name_short = $settings['general']['org']['general']['org_name_short'];
          $replacements[$original] = $sanitize ? check_plain($org_name_short) : $org_name_short;
          break;

        case 'type':
          $org_type = $settings['general']['org']['general']['org_type'];
          $replacements[$original] = $sanitize ? check_plain($org_type) : $org_type;
          break;

        case 'number':
          $org_number = $settings['general']['org']['general']['org_number'];
          $replacements[$original] = $sanitize ? check_plain($org_number) : $org_number;
          break;

        case 'number-ofsted':
          $org_number_ofsted = $settings['general']['org']['general']['org_number_ofsted'];
          $replacements[$original] = $sanitize ? check_plain($org_number_ofsted) : $org_number_ofsted;
          break;
      }
    }
  }

  elseif ($type == 'gender' && !empty($data['gender'])) {
    $is_male = (in_array(strtolower($data['gender']->gender_id), array('1', 'm', 'male', 'he', 'him', 'boy', 'son')));
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'type':
          $replacements[$original] = $is_male ? 'Male' : 'Female';
          break;

        case 'he-she':
          $replacements[$original] = $is_male ? 'he' : 'she';
          break;

        case 'his-her':
          $replacements[$original] = $is_male ? 'his' : 'her';
          break;

        case 'him-her':
          $replacements[$original] = $is_male ? 'him' : 'her';
          break;

        case 'boy-girl':
          $replacements[$original] = $is_male ? 'boy' : 'girl';
          break;

        case 'son-daughter':
          $replacements[$original] = $is_male ? 'son' : 'daughter';
          break;
      }
    }
  }

  elseif ($type == 'person' && !empty($data['person'])) {
    $pw = entity_metadata_wrapper('node', $data['person']);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'person-id':
          $replacements[$original] = $pw->getIdentifier();
          break;

        case 'firstname':
          $replacements[$original] = $pw->field_firstname->value();
          break;

        case 'lastname':
          $replacements[$original] = $pw->field_lastname->value();
          break;

        case 'name':
          $replacements[$original] = $pw->field_firstname->value() . ' ' . $pw->field_lastname->value();
          break;
      }
    }
  }

  elseif ($type == 'account' && !empty($data['account'])) {
    $aw = entity_metadata_wrapper('node', $data['account']);
    $pw = entity_metadata_wrapper('node', $aw->field_account_owner->value());
    $user = $pw->field_user_id->value();
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'account-id':
          $replacements[$original] = $aw->getIdentifier();
          break;

        case 'number':
          $replacements[$original] = $aw->field_account_number->value();
          break;

        case 'owner':
          $replacements[$original] = $pw->field_firstname->value() . ' ' . $pw->field_lastname->value();
          break;

        case 'balance':
          $sum = 0;
          // If the account owner is not linked to a user account return 0.
          if ($user) {
            $sql = 'SELECT ROUND(SUM(cot.commerce_order_total_amount)/100, 2) FROM {commerce_order} co';
            $sql .= ' LEFT JOIN {field_data_commerce_order_total} cot ON co.order_id = cot.entity_id';
            $sql .= ' WHERE co.uid = :uid';
            $sum = db_query($sql, array(':uid' => $user->uid))->fetchField();
            $sql = 'SELECT ROUND(SUM(cpt.amount)/100, 2) FROM {commerce_order} co';
            $sql .= ' JOIN {commerce_payment_transaction} cpt ON co.order_id = cpt.order_id';
            $sql .= ' WHERE co.uid = :uid AND cpt.status = :status';
            $sum -= db_query($sql, array(':uid' => $user->uid, ':status' => 'success'))->fetchField();
          }
          $replacements[$original] = $sum;
          break;

        case 'status':
          $replacements[$original] = $aw->field_account_status->value();
          break;
      }
    }

    // Handle chained owner token.
    if ($owner_tokens = token_find_with_prefix($tokens, 'owner')) {
      $replacements += token_generate('person', $owner_tokens, array('person' => $pw->value()), $options);
    }
  }

  elseif ($type == 'child' && !empty($data['child'])) {
    $cw = entity_metadata_wrapper('node', $data['child']);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'child-id':
          $replacements[$original] = $cw->getIdentifier();
          break;

        case 'firstname':
          $replacements[$original] = $cw->cura_firstname->value();
          break;

        case 'lastname':
          $replacements[$original] = $cw->cura_lastname->value();
          break;

        case 'birthdate':
          $replacements[$original] = format_date($cw->field_birth_date[0]->value(), 'medium', '', NULL, $language_code);
          break;

        case 'startdate':
          $replacements[$original] = format_date($cw->field_child_start_date->value(), 'medium', '', NULL, $language_code);
          break;

        case 'finishdate':
          $replacements[$original] = format_date($cw->field_child_finish_date->value(), 'medium', '', NULL, $language_code);
          break;

        case 'plan':
          $date = $cw->field_child_start_date->value();
          if (REQUEST_TIME > $date) $date = REQUEST_TIME;
          module_load_include('inc', 'cura', 'cura.sessions');
          $plan = cura_display_session_plan($data['child'], $date);
          $replacements[$original] = drupal_render($plan);
          break;

        case 'position':
          $replacements[$original] = $cw->field_child_wl_position->raw();
          break;
      }
    }

    // Handle chained gender token.
    $gender = new stdClass();
    $gender->gender_id = $cw->field_child_gender->value();
    if ($gender_tokens = token_find_with_prefix($tokens, 'gender')) {
      $replacements += token_generate('gender', $gender_tokens, array('gender' => $gender), $options);
    }

    // Handle chained date tokens.
    $list = array(
      'birthdate' => 'field_birth_date[0]',
      'startdate' => 'field_child_start_date',
      'finishdate' => 'field_child_finish_date',
    );
    foreach ($list as $prefix => $source) {
      if ($date_tokens = token_find_with_prefix($tokens, $prefix)) {
        $replacements += token_generate('date', $date_tokens, array('date' => $cw->{$source}->value()), $options);
      }
    }
  }

  return $replacements;
}

