<?php

/**
 * @file
 * Utility functions for the Cura Childcare Suite.
 */

/**
 * Create some settings to test.
 *
 * WARNING! Executing this function will potentially overwrite some production settings.
 */
function _cura_create_settings() {
  $category = 'notification';
  $data['enabled'] = 1;
  $data['waiting_list'] = array(
    'enabled' => 1,
    'from' => 'admissions_email',
    'cc' => 'admissions_email',
  );
  $data['weekly_plan'] = array(
    'enabled' => 1,
    'from' => 'from_email',
    'to' => 'vip <vip@gmail.com>',
    'cc' => 'admin_email',
  );
  $data['test'] = array(
    'test' => 0,
    'test2' => 'test2@pp.org',
  );
  $settings = cura_settings_save($category, $data);
}

/**
 * Create blank order for the specified person.
 */
function _cura_create_blank_order($person, $child = NULL) {
  $account = $GLOBALS['user'];
  if ($account->uid >= 0) {
    if (!empty($person)) {
      if (($pw = entity_metadata_wrapper('node', $person)) && ($pw->type->value() === 'person')) {
        drupal_set_message('<pre>person:<br>' . print_r($pw->value(), TRUE));
        // Check that the person has a uid.
        if ($uid = $pw->field_user_id->value()) {
          drupal_set_message('uid: ' . $uid);
          $profile = cura_billing_customer_profile_new('billing', $pw->value());
          drupal_set_message('<pre>profile:<br>' . print_r($profile, TRUE));
          $order = commerce_order_new($uid, 'checkout_review', 'commerce_order');
          if (!empty($profile->profile_id)) {
            $order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id'] = $profile->profile_id;
          }
          commerce_order_save($order);
          drupal_set_message('<pre>order:<br>' . print_r($order, TRUE));
        }
        else {
          drupal_set_message('Person does not have a user account!');
        }
      }
    }
  }
  else {
    drupal_set_message('Only the administrator can create blank orders!');
  }
}

/**
 * Creates registration notifications for all children.
 */
function _cura_create_reg_notifications() {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'child')
//    ->propertyCondition('status', 1)
    ->fieldOrderBy('cura_lastname', 'value', 'ASC')
    ->fieldOrderBy('cura_firstname', 'value', 'ASC')
    ->addMetaData('account', user_load(1)); // Run the query as user 1.
  $result = $query->execute();
  // Check that child records were found.
  if (isset($result['node'])) {
    $child_nids = array_keys($result['node']);
    $children = node_load_multiple($child_nids);
    foreach ($children as $child_id => $child) {
      $cw = entity_metadata_wrapper('node', $child);
      $child_name = $cw->cura_firstname->value() . ' ' . $cw->cura_lastname->value();
      $child_dob = date('j F Y', $cw->field_birth_date[0]->value());
      drupal_set_message(print_r($cw->field_birth_date->value(), TRUE));
      $notification = new stdClass();
      $notification->label = 'Registration for ' . $child_name;
      $notification->type = 'Registration';
      $notification->notes = 'Registration for ' . $child_name . ' born ' . $child_dob . ' submitted ' . date('l, j F Y', $cw->field_child_reg_date->value()) . '.';
      $notification->date = $cw->field_child_reg_date->value();
      $notification->child_id = $cw->getIdentifier();
      $notification->uid = $cw->field_account_id->field_account_owner->field_user_id->raw();
      cura_notification_save($notification);
      drupal_set_message('<pre>Notification:<br>' . print_r($notification, TRUE) . '</pre>');
    }
  }
}

/**
 * Creates user accounts for those account owners that do not already have one.
 */
function _cura_create_user_accounts() {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'account')
//    ->propertyCondition('status', 1)
    ->propertyOrderBy('created', 'ASC')
    ->addMetaData('account', user_load(1)); // Run the query as user 1.
  $result = $query->execute();
  if (isset($result['node'])) {
    $account_nids = array_keys($result['node']);
    $accounts = node_load_multiple($account_nids);
    foreach ($accounts as $account_id => $account) {
      $aw = entity_metadata_wrapper('node', $account);
      $text = 'Account ID (nid): ' . $aw->getIdentifier() . '<br>';
      $person = node_load($aw->field_account_owner->raw());
      if (!empty($person->field_firstname[LANGUAGE_NONE][0]['value'])) {
        $username = $aw->field_account_owner->field_firstname->value() . ' ' . $aw->field_account_owner->field_lastname->value();
        $email = $aw->field_account_owner->field_email->value();
        $text .= $aw->field_account_number->value();
        $uid = $aw->field_account_owner->field_user_id->raw();
        if (empty($uid)) {
          $text .= '->No user';
          if (!$user = user_load_by_mail($email)) {
            $text .= '->No userbyemail';
            if (!$user = user_load_by_name($username)) {
              $text .= '->No userbyname->Create';
              $user = entity_create('user', array(
                'name' => $username,
                'mail' => $email,
                'status' => 1,
              ));
              entity_save('user', $user);
            }
          }
        }
        $text .= ' ' . $username . ' (' . $email . ')<br>';
      }
      else {
        $text .= '->No account owner!!!';
      }
      drupal_set_message($text);
    }
  }
}

/**
 * Tests the registration confirmation email.
 */
function _cura_send_test_registration_confirmation_email() {
  $child = node_load(3939);
  $order = commerce_order_load(720);
  cura_admissions_send_registration_confirmation_email($child, $order);
}

